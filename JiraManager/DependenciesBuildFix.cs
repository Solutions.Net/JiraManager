using System;


namespace JiraManager
{
    // Make Visual studio to copies dependencies (DLL)
    // see http://stackoverflow.com/questions/15816769/dependent-dll-is-not-getting-copied-to-the-build-output-folder-in-visual-studio
    static class DependenciesBuildFix
    {
        #pragma warning disable CS0414

        //static AnyTypeFromAnyLibrary dummy = null;

        #pragma warning restore CS0414
    }
}

