﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.Diagram.Core;
using DevExpress.Utils;
using DevExpress.XtraDiagram;

using TechnicalTools;

using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.ObjectExtensions;


namespace JiraManager.UI.ViewObjects
{
    public class HeaderShape : DiagramShape
    {
        internal IssueView Parent;
        Issue Issue { get { return Parent.Issue; } }
        public override string Content { get { return Parent.Issue.GetLocalData(false)?.Summary ?? ""; }
                                         set { Parent.Issue.GetLocalData().Summary = value ?? ""; RaiseContentChanged(null, null); } }

        public HeaderShape(IssueView parent)
        {
            Parent = parent;
            Parent.Items.Add(this);
            Bounds = new RectangleF(0, 0, Parent.Width, Parent.MinHeight);
            Anchors = Sides.Left | Sides.Top | Sides.Right;

            CanMove = false;
            CanRotate = false;
            CanResize = false;
            CanSelect = false;
            CanEdit = Issue.Id < 0;
            // Clear connectionPoint to forbid creating connection
            ConnectionPoints = new PointCollection(new System.Collections.Generic.List<PointFloat>());

            Content = Issue.Summary.IfBlankUse(Issue.GetLocalData(false)?.Summary ?? "");
            Appearance.TextOptions.WordWrap = WordWrap.Wrap;
                
            if (Issue.Id < 0)
            {
                Appearance.BackColor = Color.Transparent;
                Appearance.ForeColor = Color.Black;
                Appearance.BorderColor = GraphicalChart.IssueTypeToBackColor(Issue.IssueType);
            }
            else
            {
                Appearance.BackColor = GraphicalChart.IssueTypeToBackColor(Issue.IssueType);
                Appearance.ForeColor = GraphicalChart.IssueTypeToForeColor(Issue.IssueType);
            }
        }
    }
}
