﻿using System;
using System.Diagnostics;

using DevExpress.Diagram.Core;
using DevExpress.XtraDiagram;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.UI.ViewObjects
{
    public class IssueLinkView : DiagramConnector, IViewObject
    {
        public IssueLink Link { get; set; } // can be null

        public IssueLinkView(IssueLink link, IssueView from, IssueView to)
            : base(DefaultConnectorType ?? ConnectorType.Curved, from, to)
        {
            Debug.Assert(link != null);
            Debug.Assert(link.InwardIssue == from.Issue);
            Debug.Assert(link.OutwardIssue == to.Issue);
            Link = link;
            BeginItem = from;
            EndItem = to;
        }

        public static ConnectorType DefaultConnectorType = null;
    }

}
