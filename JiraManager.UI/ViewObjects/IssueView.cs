﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Diagram.Core;
using DevExpress.Utils;
using DevExpress.XtraDiagram;

using TechnicalTools;

using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.ObjectExtensions;
using System.Collections.Generic;

namespace JiraManager.UI.ViewObjects
{
    public class IssueView : DiagramContainer, IViewObject
    {
        public Issue Issue { get; set; } // can be null

        public IssueView(Issue issue)
        {
            Debug.Assert(issue != null);
            Issue = issue;
            Height = MinHeight;
            Width = 100;
            Height = 50;
            CanRotate = false;
            CanEdit = Issue.Id < 0;
            DragMode = ContainerDragMode.ByAnyPoint;

            // Clear connectionPoint to forbid creating connection
            //ConnectionPoints = new PointCollection(new System.Collections.Generic.List<PointFloat>());

            Header = Issue.Summary.IfBlankUse(Issue.GetLocalData(false)?.Summary ?? "");
            ShowHeader = true;
            Appearance.TextOptions.WordWrap = WordWrap.Wrap;

            if (Issue.Id < 0)
            {
                Appearance.BorderSize = 3;
                Appearance.BackColor = Color.Transparent;
                Appearance.ForeColor = Color.Black;
                Appearance.BorderColor = GraphicalChart.IssueTypeToBackColor(Issue.IssueType);
            }
            else
            {
                Appearance.BackColor = GraphicalChart.IssueTypeToBackColor(Issue.IssueType);
                Appearance.ForeColor = GraphicalChart.IssueTypeToForeColor(Issue.IssueType);
            }

            Shape = StandardContainers.Classic;
            
            ResetFromDb();
        }

        public void MoveToParent(IssueView parent)
        {
            // https://supportcenter.devexpress.com/ticket/details/t689550/how-to-programically-move-diagram-shapes-inside-a-diagram-container
            var box = GetBoundingBox(parent.Items);
            GetDiagram().Items.Remove(this);
            parent.Items.Add(this);
            parent.CanCollapse = true;
            Position = new PointFloat(box.Left, box.Bottom + 5);
        }

        public static RectangleF GetBoundingBox(IEnumerable<DiagramItem> items)
        {
            var points = items.OfType<DiagramItem>()
                              .SelectMany(item => new[] { item.Position, new PointF(item.Bounds.Right, item.Bounds.Bottom) })
                              .ToList();
            var xs = points.Select(p => p.X).DefaultIfEmpty(0);
            float xmin = xs.Min();
            float xmax = xs.Max();

            var ys = points.Select(p => p.Y).DefaultIfEmpty(0);
            float ymin = ys.Min();
            float ymax = ys.Max();
            var box = new RectangleF(xmin, ymin, xmax - xmin, ymax - ymin);
            return box;
        }

        public void ListenChangeAndAssignModel()
        {
            BoundsChanged += IssueShape_BoundsChanged;
        }

        


        private void Title_ContentChanged(object sender, EventArgs e)
        {
            RaiseHeaderChanged(null, null);
        }


        public override string Header { get { return base.Header; } set { base.Header = value; } }

        private void IssueShape_BoundsChanged(object sender, EventArgs e)
        {
            var data = Issue.GetLocalData();
            data.Width = Width;
            data.Height = Height;
            data.Left = X;
            data.Top = Y;
        }

        // from https://docs.devexpress.com/WindowsForms/116877/controls-and-libraries/diagrams/diagram-control/properties-panel
        internal static void GetCustomGetEditableItemProperties(object sender, DiagramCustomGetEditableItemPropertiesEventArgs e)
        {
            // To add property existing in IssueShape (need to implement INotifyPropertyChanged after)
            //if (e.Item is IssueShape)
            //{
            //    PropertyDescriptor infoPropertyDescriptor = TypeDescriptor.GetProperties(typeof(IssueShape))["Info"];
            //    e.Properties.Add(infoPropertyDescriptor);
            //}

            // To add unbound property 
            //if (e.Item is IssueShape)
            //{
            //    PropertyDescriptor namePropertyDescriptor = e.CreateProxyProperty("Name", item => ((Customer)item.DataContext).Name, (item, value) => ((Customer)item.DataContext).Name = value);
            //    e.Properties.Add(namePropertyDescriptor);
            //}

            // To remove a property
            if (e.Item is IssueView)
            {
                e.Properties.Remove(e.Properties[nameof(IDiagramItem.StrokeDashArray)]);
                e.Properties.Remove(e.Properties[nameof(IDiagramItem.StrokeThickness)]);
                e.Properties.Remove(e.Properties[nameof(IDiagramItem.Angle)]);
                e.Properties.Remove(e.Properties[nameof(IDiagramContainer.ShowHeader)]);
                //e.Properties.Remove(e.Properties[nameof(IDiagramContainer.)]);

                // Simply rename Content to Issue Key
                //e.Properties.Remove(e.Properties[nameof(Header)]);
                //e.Properties.Add(e.CreateProxyProperty(TypeDescriptor.GetProperties(typeof(DiagramShape))[nameof(Header)],
                //    x => x, new Attribute[] { new BrowsableAttribute(true), new DisplayNameAttribute("Issue Key") }));
                // https://supportcenter.devexpress.com/ticket/details/t865684/propertygridcontrol-how-to-generate-rows-for-a-business-object-at-design-time
                // To add expandable properties https://supportcenter.devexpress.com/ticket/details/t854591/propertygridcontrol-how-to-make-an-embedded-list-of-class-expandable
                //                              https://supportcenter.devexpress.com/ticket/details/t555027/property-grid-how-to-display-expandable-properties
            }
        }

        // from https://supportcenter.devexpress.com/ticket/details/t473958/how-to-clone-a-diagram-item
        //public IssueShape Clone()
        //{
        //    //Issue issue = ... ???
        //    return (IssueShape)Controller.GetCloneInfo(GetDiagram())();
        //}

        public void FitToContent()
        {
            IDiagramItem s = this;
            var testFont = new Font(s.FontFamily.Name, (float)s.FontSize, FontStyle.Regular);
            var size = TextRenderer.MeasureText(Header, testFont);
            var buttonWidth = CanCollapse
                            ? 50
                            : 0;
            var buttonHeight = CanCollapse && (Items.Count > 0 || CollapseButtonVisibilityMode == CollapseButtonVisibilityMode.Always)
                            ? 17
                            : 0;
            var sizeF = new SizeF(size.Width + 5 + buttonWidth, size.Height + buttonHeight);

            var box = GetBoundingBox(Items);
            sizeF.Width = Math.Max(sizeF.Width, box.Right);
            sizeF.Height += box.Bottom;
            Size = sizeF;
        }

        public void ResetFromDb()
        {
            var iv = Issue.GetLocalData(false);
            if (iv == null)
                return;
            Bounds = new RectangleF(iv.Left, iv.Top, Math.Max(iv.Width, MinWidth), Math.Max(iv.Height, MinHeight));
        }
    }

}
