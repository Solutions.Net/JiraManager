namespace JiraManager.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbon = new TechnicalTools.UI.DX.Controls.EnhancedRibbonControl();
            this.mnuJiraOverview = new DevExpress.XtraBars.BarButtonItem();
            this.mnuUserSettings = new DevExpress.XtraBars.BarSubItem();
            this.mnuSkins = new DevExpress.XtraBars.BarSubItem();
            this.btnReleaseNoteHistory = new DevExpress.XtraBars.BarButtonItem();
            this.mnuUsersProfilesRights = new DevExpress.XtraBars.BarButtonItem();
            this.mnuLogs = new DevExpress.XtraBars.BarButtonItem();
            this.mnuDeploy = new DevExpress.XtraBars.BarButtonItem();
            this.barlblVersion = new DevExpress.XtraBars.BarStaticItem();
            this.barlblDomain = new DevExpress.XtraBars.BarStaticItem();
            this.barlblEnv = new DevExpress.XtraBars.BarStaticItem();
            this.barlblConnectedTo = new DevExpress.XtraBars.BarStaticItem();
            this.barlblHostIp = new DevExpress.XtraBars.BarStaticItem();
            this.barlblDatabase = new DevExpress.XtraBars.BarStaticItem();
            this.barblbNewRelease = new DevExpress.XtraBars.BarStaticItem();
            this.bbtnShowInstancesInMemory = new DevExpress.XtraBars.BarStaticItem();
            this.bbtnShowTrace = new DevExpress.XtraBars.BarStaticItem();
            this.barlblLogin = new DevExpress.XtraBars.BarStaticItem();
            this.mnuEnvironments = new DevExpress.XtraBars.BarButtonItem();
            this.mnuAppDialoguing = new DevExpress.XtraBars.BarButtonItem();
            this.mnuSaveAllLocally = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRefreshFromAtlassian = new DevExpress.XtraBars.BarButtonItem();
            this.mnuSeeAllIssues = new DevExpress.XtraBars.BarButtonItem();
            this.btnAnalyseAnyDataFile = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageCategoryMain = new DevExpress.XtraBars.Ribbon.RibbonPageCategory();
            this.ribbonPageMain = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupMainScreens = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUserSettings = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupDevelopperTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.compositeControl = new TechnicalTools.UI.DX.XtraUserControlComposite();
            this.colorBar = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.mnuUserPersonalSettings = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ribbon.ApplicationButtonImageOptions.Image")));
            this.ribbon.ApplicationCaption = "JIRA Manager";
            this.ribbon.DrawGroupCaptions = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.mnuJiraOverview,
            this.mnuUserSettings,
            this.mnuSkins,
            this.mnuUsersProfilesRights,
            this.mnuLogs,
            this.mnuDeploy,
            this.barlblVersion,
            this.barlblDomain,
            this.barlblEnv,
            this.barlblConnectedTo,
            this.barlblHostIp,
            this.barlblDatabase,
            this.barblbNewRelease,
            this.bbtnShowInstancesInMemory,
            this.bbtnShowTrace,
            this.barlblLogin,
            this.mnuEnvironments,
            this.mnuAppDialoguing,
            this.btnReleaseNoteHistory,
            this.mnuSaveAllLocally,
            this.mnuRefreshFromAtlassian,
            this.mnuSeeAllIssues,
            this.btnAnalyseAnyDataFile,
            this.mnuUserPersonalSettings});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 121;
            this.ribbon.Name = "ribbon";
            this.ribbon.PageCategories.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageCategory[] {
            this.ribbonPageCategoryMain});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbon.Size = new System.Drawing.Size(1270, 98);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            this.ribbon.ShowCustomizationMenu += new DevExpress.XtraBars.Ribbon.RibbonCustomizationMenuEventHandler(this.ribbon_ShowCustomizationMenu);
            // 
            // mnuJiraOverview
            // 
            this.mnuJiraOverview.Caption = "JIRA Overview";
            this.mnuJiraOverview.Id = 1;
            this.mnuJiraOverview.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuJiraOverview.ImageOptions.Image")));
            this.mnuJiraOverview.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuJiraOverview.ImageOptions.LargeImage")));
            this.mnuJiraOverview.Name = "mnuJiraOverview";
            this.mnuJiraOverview.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.mnuJiraOverview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuJiraOverview_ItemClick);
            // 
            // mnuUserSettings
            // 
            this.mnuUserSettings.Caption = "Settings";
            this.mnuUserSettings.Id = 21;
            this.mnuUserSettings.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuUserSettings.ImageOptions.Image")));
            this.mnuUserSettings.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuUserSettings.ImageOptions.LargeImage")));
            this.mnuUserSettings.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuSkins),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnReleaseNoteHistory),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuUserPersonalSettings)});
            this.mnuUserSettings.Name = "mnuUserSettings";
            // 
            // mnuSkins
            // 
            this.mnuSkins.Caption = "Skins";
            this.mnuSkins.Id = 59;
            this.mnuSkins.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuSkins.ImageOptions.Image")));
            this.mnuSkins.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuSkins.ImageOptions.LargeImage")));
            this.mnuSkins.Name = "mnuSkins";
            // 
            // btnReleaseNoteHistory
            // 
            this.btnReleaseNoteHistory.Caption = "Release Notes history";
            this.btnReleaseNoteHistory.Id = 96;
            this.btnReleaseNoteHistory.Name = "btnReleaseNoteHistory";
            this.btnReleaseNoteHistory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReleaseNoteHistory_ItemClick);
            // 
            // mnuUsersProfilesRights
            // 
            this.mnuUsersProfilesRights.Caption = "Users && Profiles && Rights";
            this.mnuUsersProfilesRights.Id = 64;
            this.mnuUsersProfilesRights.Name = "mnuUsersProfilesRights";
            this.mnuUsersProfilesRights.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuUsersProfilesRights.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuUsersProfilesRights_ItemClick);
            // 
            // mnuLogs
            // 
            this.mnuLogs.Caption = "Logs";
            this.mnuLogs.Id = 65;
            this.mnuLogs.Name = "mnuLogs";
            this.mnuLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuLogs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuLogs_ItemClick);
            // 
            // mnuDeploy
            // 
            this.mnuDeploy.Caption = "Deploy JiraManager";
            this.mnuDeploy.Id = 66;
            this.mnuDeploy.Name = "mnuDeploy";
            this.mnuDeploy.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuDeploy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuDeploy_ItemClick);
            // 
            // barlblVersion
            // 
            this.barlblVersion.Caption = "Version : YY.MM.DD.HH";
            this.barlblVersion.Id = 69;
            this.barlblVersion.Name = "barlblVersion";
            this.barlblVersion.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblDomain
            // 
            this.barlblDomain.Caption = "Domain: ";
            this.barlblDomain.Id = 70;
            this.barlblDomain.Name = "barlblDomain";
            this.barlblDomain.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblEnv
            // 
            this.barlblEnv.Caption = "Env:";
            this.barlblEnv.Id = 71;
            this.barlblEnv.Name = "barlblEnv";
            this.barlblEnv.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblConnectedTo
            // 
            this.barlblConnectedTo.Caption = "Connected to:";
            this.barlblConnectedTo.Id = 72;
            this.barlblConnectedTo.Name = "barlblConnectedTo";
            this.barlblConnectedTo.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblHostIp
            // 
            this.barlblHostIp.Caption = "192.168.255.XX";
            this.barlblHostIp.Id = 73;
            this.barlblHostIp.Name = "barlblHostIp";
            this.barlblHostIp.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblDatabase
            // 
            this.barlblDatabase.Caption = "database name";
            this.barlblDatabase.Id = 74;
            this.barlblDatabase.Name = "barlblDatabase";
            this.barlblDatabase.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barblbNewRelease
            // 
            this.barblbNewRelease.Caption = "*** New Version Available ! ***";
            this.barblbNewRelease.Id = 75;
            this.barblbNewRelease.Name = "barblbNewRelease";
            this.barblbNewRelease.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barblbNewRelease.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barblbNewRelease_ItemClick);
            // 
            // bbtnShowInstancesInMemory
            // 
            this.bbtnShowInstancesInMemory.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbtnShowInstancesInMemory.Caption = "Show object instances in memory";
            this.bbtnShowInstancesInMemory.Id = 77;
            this.bbtnShowInstancesInMemory.Name = "bbtnShowInstancesInMemory";
            this.bbtnShowInstancesInMemory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbtnShowInstancesInMemory_ItemClick);
            // 
            // bbtnShowTrace
            // 
            this.bbtnShowTrace.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbtnShowTrace.Caption = "Show Database Trace";
            this.bbtnShowTrace.Id = 78;
            this.bbtnShowTrace.Name = "bbtnShowTrace";
            this.bbtnShowTrace.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbtnShowTrace_ItemClick);
            // 
            // barlblLogin
            // 
            this.barlblLogin.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barlblLogin.Caption = "Login";
            this.barlblLogin.Id = 79;
            this.barlblLogin.Name = "barlblLogin";
            // 
            // mnuEnvironments
            // 
            this.mnuEnvironments.Caption = "Environment Settings";
            this.mnuEnvironments.Id = 89;
            this.mnuEnvironments.Name = "mnuEnvironments";
            this.mnuEnvironments.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuEnvironments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEnvironments_ItemClick);
            // 
            // mnuAppDialoguing
            // 
            this.mnuAppDialoguing.Caption = "App Dialoguing";
            this.mnuAppDialoguing.Id = 94;
            this.mnuAppDialoguing.Name = "mnuAppDialoguing";
            this.mnuAppDialoguing.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuAppDialoguing.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuAppDialoguing_ItemClick);
            // 
            // mnuSaveAllLocally
            // 
            this.mnuSaveAllLocally.Caption = "Save All (locally)";
            this.mnuSaveAllLocally.Id = 116;
            this.mnuSaveAllLocally.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuSaveAllLocally.ImageOptions.Image")));
            this.mnuSaveAllLocally.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuSaveAllLocally.ImageOptions.LargeImage")));
            this.mnuSaveAllLocally.Name = "mnuSaveAllLocally";
            this.mnuSaveAllLocally.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuSaveAllLocally_ItemClick);
            // 
            // mnuRefreshFromAtlassian
            // 
            this.mnuRefreshFromAtlassian.Caption = "Refresh From Atlassian";
            this.mnuRefreshFromAtlassian.Id = 117;
            this.mnuRefreshFromAtlassian.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRefreshFromAtlassian.ImageOptions.Image")));
            this.mnuRefreshFromAtlassian.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuRefreshFromAtlassian.ImageOptions.LargeImage")));
            this.mnuRefreshFromAtlassian.Name = "mnuRefreshFromAtlassian";
            this.mnuRefreshFromAtlassian.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuRefreshFromAtlassian_ItemClick);
            // 
            // mnuSeeAllIssues
            // 
            this.mnuSeeAllIssues.Caption = "See All Issues";
            this.mnuSeeAllIssues.Id = 118;
            this.mnuSeeAllIssues.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuSeeAllIssues.ImageOptions.Image")));
            this.mnuSeeAllIssues.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuSeeAllIssues.ImageOptions.LargeImage")));
            this.mnuSeeAllIssues.Name = "mnuSeeAllIssues";
            this.mnuSeeAllIssues.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuSeeAllIssues_ItemClick);
            // 
            // btnAnalyseAnyDataFile
            // 
            this.btnAnalyseAnyDataFile.Caption = "Analyse file";
            this.btnAnalyseAnyDataFile.Id = 119;
            this.btnAnalyseAnyDataFile.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAnalyseAnyDataFile.ImageOptions.Image")));
            this.btnAnalyseAnyDataFile.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnAnalyseAnyDataFile.ImageOptions.LargeImage")));
            this.btnAnalyseAnyDataFile.Name = "btnAnalyseAnyDataFile";
            this.btnAnalyseAnyDataFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAnalyseAnyDataFile_ItemClick);
            // 
            // ribbonPageCategoryMain
            // 
            this.ribbonPageCategoryMain.Name = "ribbonPageCategoryMain";
            this.ribbonPageCategoryMain.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageMain});
            this.ribbonPageCategoryMain.Text = "Main";
            // 
            // ribbonPageMain
            // 
            this.ribbonPageMain.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupMainScreens,
            this.ribbonPageGroupUserSettings,
            this.ribbonPageGroupDevelopperTools});
            this.ribbonPageMain.Name = "ribbonPageMain";
            this.ribbonPageMain.Text = "Main Screens";
            // 
            // ribbonPageGroupMainScreens
            // 
            this.ribbonPageGroupMainScreens.ItemLinks.Add(this.mnuJiraOverview);
            this.ribbonPageGroupMainScreens.ItemLinks.Add(this.mnuSeeAllIssues);
            this.ribbonPageGroupMainScreens.ItemLinks.Add(this.mnuSaveAllLocally);
            this.ribbonPageGroupMainScreens.ItemLinks.Add(this.mnuRefreshFromAtlassian);
            this.ribbonPageGroupMainScreens.ItemLinks.Add(this.btnAnalyseAnyDataFile);
            this.ribbonPageGroupMainScreens.Name = "ribbonPageGroupMainScreens";
            this.ribbonPageGroupMainScreens.Text = "Main Screens";
            // 
            // ribbonPageGroupUserSettings
            // 
            this.ribbonPageGroupUserSettings.ItemLinks.Add(this.mnuUserSettings);
            this.ribbonPageGroupUserSettings.Name = "ribbonPageGroupUserSettings";
            this.ribbonPageGroupUserSettings.Tag = "AlignRight";
            this.ribbonPageGroupUserSettings.Text = "Your personnal settings";
            // 
            // ribbonPageGroupDevelopperTools
            // 
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuUsersProfilesRights);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuEnvironments);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuLogs);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuDeploy);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuAppDialoguing);
            this.ribbonPageGroupDevelopperTools.Name = "ribbonPageGroupDevelopperTools";
            this.ribbonPageGroupDevelopperTools.Tag = "AlignRight";
            this.ribbonPageGroupDevelopperTools.Text = "Developpers tools";
            this.ribbonPageGroupDevelopperTools.Visible = false;
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barlblVersion);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblDomain);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblEnv);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblConnectedTo);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblHostIp);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblDatabase);
            this.ribbonStatusBar.ItemLinks.Add(this.barblbNewRelease);
            this.ribbonStatusBar.ItemLinks.Add(this.bbtnShowInstancesInMemory);
            this.ribbonStatusBar.ItemLinks.Add(this.bbtnShowTrace);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblLogin);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 775);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1270, 24);
            // 
            // compositeControl
            // 
            this.compositeControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.compositeControl.Location = new System.Drawing.Point(0, 108);
            this.compositeControl.Name = "compositeControl";
            this.compositeControl.Size = new System.Drawing.Size(1270, 667);
            this.compositeControl.TabIndex = 2;
            // 
            // colorBar
            // 
            this.colorBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.colorBar.EditValue = 0;
            this.colorBar.Enabled = false;
            this.colorBar.Location = new System.Drawing.Point(0, 98);
            this.colorBar.Name = "colorBar";
            this.colorBar.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(237)))), ((int)(((byte)(125)))));
            this.colorBar.Properties.EndColor = System.Drawing.Color.White;
            this.colorBar.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.colorBar.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.colorBar.Properties.MarqueeAnimationSpeed = 1000;
            this.colorBar.Properties.MarqueeWidth = 1;
            this.colorBar.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.colorBar.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.colorBar.Properties.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(237)))), ((int)(((byte)(125)))));
            this.colorBar.Properties.Stopped = true;
            this.colorBar.Size = new System.Drawing.Size(1270, 10);
            this.colorBar.TabIndex = 24;
            // 
            // mnuUserPersonalSettings
            // 
            this.mnuUserPersonalSettings.Caption = "Your Personal Settings";
            this.mnuUserPersonalSettings.Id = 120;
            this.mnuUserPersonalSettings.Name = "mnuUserPersonalSettings";
            this.mnuUserPersonalSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuUserPersonalSettings_ItemClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1270, 799);
            this.Controls.Add(this.compositeControl);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.colorBar);
            this.Controls.Add(this.ribbon);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("MainForm.IconOptions.Icon")));
            this.Name = "MainForm";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "JIRA Manager";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBar.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TechnicalTools.UI.DX.Controls.EnhancedRibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageMain;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupMainScreens;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUserSettings;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem mnuJiraOverview;
        private TechnicalTools.UI.DX.XtraUserControlComposite compositeControl;
        private DevExpress.XtraBars.BarSubItem mnuUserSettings;
        private DevExpress.XtraBars.BarSubItem mnuSkins;
        private DevExpress.XtraBars.Ribbon.RibbonPageCategory ribbonPageCategoryMain;
        private DevExpress.XtraBars.BarButtonItem mnuUsersProfilesRights;
        private DevExpress.XtraBars.BarButtonItem mnuLogs;
        private DevExpress.XtraBars.BarButtonItem mnuDeploy;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupDevelopperTools;
        private DevExpress.XtraBars.BarStaticItem barlblVersion;
        private DevExpress.XtraBars.BarStaticItem barlblDomain;
        private DevExpress.XtraBars.BarStaticItem barlblEnv;
        private DevExpress.XtraBars.BarStaticItem barlblConnectedTo;
        private DevExpress.XtraBars.BarStaticItem barlblHostIp;
        private DevExpress.XtraBars.BarStaticItem barlblDatabase;
        private DevExpress.XtraBars.BarStaticItem barblbNewRelease;
        private DevExpress.XtraBars.BarStaticItem bbtnShowInstancesInMemory;
        private DevExpress.XtraBars.BarStaticItem bbtnShowTrace;
        private DevExpress.XtraBars.BarStaticItem barlblLogin;
        private DevExpress.XtraEditors.MarqueeProgressBarControl colorBar;
        private DevExpress.XtraBars.BarButtonItem mnuEnvironments;
        private DevExpress.XtraBars.BarButtonItem mnuAppDialoguing;
        private DevExpress.XtraBars.BarButtonItem btnReleaseNoteHistory;
        private DevExpress.XtraBars.BarButtonItem mnuSaveAllLocally;
        private DevExpress.XtraBars.BarButtonItem mnuRefreshFromAtlassian;
        private DevExpress.XtraBars.BarButtonItem mnuSeeAllIssues;
        private DevExpress.XtraBars.BarButtonItem btnAnalyseAnyDataFile;
        private DevExpress.XtraBars.BarButtonItem mnuUserPersonalSettings;
    }
}