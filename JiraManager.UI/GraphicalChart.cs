﻿using System;
using System.Collections.Generic;
using System.Drawing;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Tools;

using ApplicationBase.Deployment.Data;

using JiraManager.Common;
using JiraManager.Business.API.DtoObjects;


namespace JiraManager.UI
{
    public static class GraphicalChart
    {
        public static readonly Color BeautifulIndigo = Color.FromArgb(75, 19, 214); // #4B13D6
        public static readonly Color StealPurple = Color.FromArgb(75, 64, 136); // #4b4088
        public static readonly Color WarmOrange = Color.FromArgb(200, 176, 151); // #C8B097

        public static readonly Color ReportsForegroundSelected = Color.White;
        public static readonly Color ReportsForeground = Color.Black;
        public static readonly Color ReportsBackgroundSelected = Color.FromKnownColor(KnownColor.Highlight);
        public static readonly Color WhiteBackground = Color.White;
        public static readonly Color LightGrayBackground = Color.WhiteSmoke;
        public static readonly Color EditableFieldBackground = Color.MintCream;


        public static readonly SolidBrush reportsForegroundBrushSelected = new SolidBrush(ReportsForegroundSelected);
        public static readonly SolidBrush reportsForegroundBrush = new SolidBrush(ReportsForeground);
        public static readonly SolidBrush reportsBackgroundBrushSelected = new SolidBrush(ReportsBackgroundSelected);
        public static readonly SolidBrush whiteBackgroundBrush = new SolidBrush(WhiteBackground);
        public static readonly SolidBrush lightGrayBackgroundBrush = new SolidBrush(LightGrayBackground);


        public static Color GetColor(eEnvironment env)
        {
            return DB.Config.Environment.Domain == eEnvironment.Prod ? Color.Red
                 : DB.Config.Environment.Domain == eEnvironment.Sandbox ? Color.Goldenrod
                 : DB.Config.Environment.Domain == eEnvironment.PreProd ? Color.DodgerBlue
                 : DB.Config.Environment.Domain == eEnvironment.Dev ? Color.FromArgb(84, 87, 118)
                 : DB.Config.Environment.Domain == eEnvironment.LocalNoDB ? Color.LightSkyBlue
                 : ((Color?)null).ThrowIfNull($"Unknown Domain {DB.Config.Environment.Domain}!");
        }

        public static Color ColorFulBlinkingForUpdateAvailable
        {
            get
            {
                return BeautifulIndigo;
            }
        }

        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(GraphicalChart));

        public static Color IssueTypeToBackColor(IssueType it)
        {
            switch (it.Name)
            {
                case "Epic": return Color.MediumPurple;
                case "Story": return Color.LimeGreen;
                case "Bug": return Color.Red;
                case "Task": return Color.DeepSkyBlue;
                case "Sub-task": return Color.LightSkyBlue;

                case "Support": return Color.Pink;
                case "Incident": return Color.Yellow;
                case "New Feature": return Color.Purple;
                case "Improvement": return Color.Blue;
                case "User Acceptance Tests": return Color.Lime;
                case "INTERNAL REVIEW": return Color.LimeGreen;
                case "Habilitation": return Color.Black;
            }
            if (!_unknownIssueType.Contains(it.Name))
            {
                _unknownIssueType.Add(it.Name);
                BusEvents.Instance.RaiseBusinessMessage($"Issue type {it.Name} has no associated background color!", _log);
            }
            return Color.LightSteelBlue;
        }
        static readonly HashSet<string> _unknownIssueType = new HashSet<string>();

        public static Color IssueTypeToForeColor(IssueType it)
        {
            return Color.Black;
        }
    }
}
