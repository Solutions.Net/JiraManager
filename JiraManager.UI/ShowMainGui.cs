﻿using System;
using System.IO;
using System.Windows.Forms;

using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraDiagram;

using TechnicalTools;
using TechnicalTools.UI.DX;

using ApplicationBase.UI.Helpers;

using JiraManager.Business;
using JiraManager.Business.DataRelation;
using JiraManager.UI.ViewObjects;


namespace JiraManager.UI
{
    public class ShowMainGui : ApplicationBase.UI.ShowMainGuiBase
    {
       
        public ShowMainGui(ApplicationBase.Business.Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            var layoutfile = Path.Combine(BusinessSingletons.Instance.GetRecommendedLocalDataFolder(), "GridViewLayouts.xml");
            EnhancedGridView_LayoutManager.DefaultLayoutRepository.DefaultRepositoryFileName = layoutfile;

            EnhancedGridView_BrowsableNestedProperties.DefaultMenuBuildingMaxRecursion = 3;
            var defaultShowInstanceOfType = EnhancedGridView_BrowsableNestedProperties.ShowInstanceOfType;
            EnhancedGridView_BrowsableNestedProperties.ShowInstanceOfType = t =>
            {
                var res = defaultShowInstanceOfType(t);
                if (res.HasValue)
                    return res;
                if (t.RemoveNullability().Assembly.FullName.StartsWith(nameof(JiraManager) + "."))
                    return true;
                return null;
            };
            var defaultDetailViewConfiguration = GridView_DetailViewManager.DetailView.SetupDefaultLayoutOnDetailViewExpandedForBusinessObject;
            GridView_DetailViewManager.DetailView.SetupDefaultLayoutOnDetailViewExpandedForBusinessObject = (masterView, detailView, parentObject) =>
            {
                defaultDetailViewConfiguration?.Invoke(masterView, detailView, parentObject);
                if (detailView is BandedGridView bgv)
                    GridView_ConfiguratorProxy.Instance.ConfigureFor(bgv, null, HierarchyProvider.Instance, parentObject);
                else if (detailView is GridView gv)
                    GridView_ConfiguratorProxy.Instance.ConfigureFor(gv, HierarchyProvider.Instance, parentObject);
            };

            // For Undo / Redo, we need to register ui element 
            // https://docs.devexpress.com/WindowsForms/118077/controls-and-libraries/diagrams/examples/how-to-create-a-diagramshape-descendant-and-serialize-its-properties
            DiagramControl.ItemTypeRegistrator.Register(typeof(IssueView));
        }

        protected override Form CreateMainForm()
        {
            return new MainForm();
        }
    }
}
        