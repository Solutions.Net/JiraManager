﻿namespace JiraManager.UI.Views
{
    partial class ProjectComponentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectComponentView));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gcIssues = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvIssues = new TechnicalTools.UI.DX.EnhancedGridView();
            this.lytctlMain = new DevExpress.XtraLayout.LayoutControl();
            this.btnEditDependencies = new DevExpress.XtraEditors.ButtonEdit();
            this.btnRefresh = new DevExpress.XtraEditors.ButtonEdit();
            this.diagramControl = new TechnicalTools.UI.DX.EnhancedDiagramControl();
            this.pgcElementProperties = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lytctlgrpIssues = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerBottom = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dckpnlGrid = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.hideContainerRight = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.pnlElementProperties = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.propertyDescriptionControl1 = new DevExpress.XtraVerticalGrid.PropertyDescriptionControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnViewStyle = new DevExpress.XtraEditors.ButtonEdit();
            this.btnSortPropertiesByCategory = new DevExpress.XtraEditors.ButtonEdit();
            this.btnSortPropertiesAlphabetically = new DevExpress.XtraEditors.ButtonEdit();
            this.btnSnapToItems = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gcIssues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIssues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlMain)).BeginInit();
            this.lytctlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditDependencies.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagramControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgcElementProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlgrpIssues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerBottom.SuspendLayout();
            this.dckpnlGrid.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            this.hideContainerRight.SuspendLayout();
            this.pnlElementProperties.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnViewStyle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortPropertiesByCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortPropertiesAlphabetically.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSnapToItems.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // gcIssues
            // 
            this.gcIssues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcIssues.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcIssues.EmbeddedNavigator.TextStringFormat = "Account {0} of {1} (visible) of {2} (total)";
            this.gcIssues.Location = new System.Drawing.Point(0, 0);
            this.gcIssues.MainView = this.gvIssues;
            this.gcIssues.Name = "gcIssues";
            this.gcIssues.Size = new System.Drawing.Size(1002, 106);
            this.gcIssues.TabIndex = 1;
            this.gcIssues.UseEmbeddedNavigator = true;
            this.gcIssues.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvIssues});
            // 
            // gvIssues
            // 
            this.gvIssues.GridControl = this.gcIssues;
            this.gvIssues.GroupFormat = "[#image]{1} {2}";
            this.gvIssues.Name = "gvIssues";
            this.gvIssues.OptionsView.ShowAutoFilterRow = true;
            // 
            // lytctlMain
            // 
            this.lytctlMain.Controls.Add(this.btnSnapToItems);
            this.lytctlMain.Controls.Add(this.btnEditDependencies);
            this.lytctlMain.Controls.Add(this.btnRefresh);
            this.lytctlMain.Controls.Add(this.diagramControl);
            this.lytctlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytctlMain.Location = new System.Drawing.Point(0, 0);
            this.lytctlMain.Name = "lytctlMain";
            this.lytctlMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(849, 285, 461, 350);
            this.lytctlMain.Root = this.layoutControlGroup1;
            this.lytctlMain.Size = new System.Drawing.Size(987, 479);
            this.lytctlMain.TabIndex = 1;
            this.lytctlMain.Text = "layoutControl1";
            // 
            // btnEditDependencies
            // 
            this.btnEditDependencies.Location = new System.Drawing.Point(59, 26);
            this.btnEditDependencies.Name = "btnEditDependencies";
            this.btnEditDependencies.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnEditDependencies.Properties.Appearance.Options.UseBackColor = true;
            this.btnEditDependencies.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.btnEditDependencies.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnEditDependencies.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnEditDependencies.Size = new System.Drawing.Size(50, 38);
            this.btnEditDependencies.StyleController = this.lytctlMain;
            this.btnEditDependencies.TabIndex = 6;
            this.btnEditDependencies.ToolTip = "Link Issues";
            this.btnEditDependencies.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnEditDependencies_ButtonClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(5, 26);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnRefresh.Properties.Appearance.Options.UseBackColor = true;
            this.btnRefresh.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.btnRefresh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnRefresh.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnRefresh.Size = new System.Drawing.Size(50, 38);
            this.btnRefresh.StyleController = this.lytctlMain;
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnRefresh_ButtonClick);
            // 
            // diagramControl
            // 
            this.diagramControl.Location = new System.Drawing.Point(5, 68);
            this.diagramControl.MoreBeautifulConnectorByDefault = true;
            this.diagramControl.Name = "diagramControl";
            this.diagramControl.OptionsBehavior.AllowPropertiesPanel = true;
            this.diagramControl.OptionsBehavior.SelectedStencils = new DevExpress.Diagram.Core.StencilCollection(new string[] {
            "BasicShapes",
            "BasicFlowchartShapes"});
            this.diagramControl.OptionsView.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.diagramControl.OptionsView.PropertiesPanelVisibility = DevExpress.Diagram.Core.PropertiesPanelVisibility.Closed;
            this.diagramControl.PreventOrphanConnector = true;
            this.diagramControl.PropertyGrid = this.pgcElementProperties;
            this.diagramControl.Size = new System.Drawing.Size(977, 406);
            this.diagramControl.StyleController = this.lytctlMain;
            this.diagramControl.TabIndex = 0;
            this.diagramControl.Text = "diagramControl1";
            // 
            // pgcElementProperties
            // 
            this.pgcElementProperties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pgcElementProperties.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pgcElementProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgcElementProperties.Location = new System.Drawing.Point(0, 26);
            this.pgcElementProperties.Name = "pgcElementProperties";
            this.pgcElementProperties.Size = new System.Drawing.Size(193, 314);
            this.pgcElementProperties.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lytctlgrpIssues});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(987, 479);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lytctlgrpIssues
            // 
            this.lytctlgrpIssues.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem2});
            this.lytctlgrpIssues.Location = new System.Drawing.Point(0, 0);
            this.lytctlgrpIssues.Name = "lytctlgrpIssues";
            this.lytctlgrpIssues.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lytctlgrpIssues.Size = new System.Drawing.Size(987, 479);
            this.lytctlgrpIssues.Text = "Issues";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.diagramControl;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(981, 410);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnRefresh;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(54, 42);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(54, 42);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(54, 42);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnEditDependencies;
            this.layoutControlItem4.Location = new System.Drawing.Point(54, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(54, 42);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(54, 42);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(54, 42);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(162, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(819, 42);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerBottom,
            this.hideContainerRight});
            this.dockManager1.Form = this;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl",
            "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl",
            "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"});
            // 
            // hideContainerBottom
            // 
            this.hideContainerBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.hideContainerBottom.Controls.Add(this.dckpnlGrid);
            this.hideContainerBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.hideContainerBottom.Location = new System.Drawing.Point(0, 479);
            this.hideContainerBottom.Name = "hideContainerBottom";
            this.hideContainerBottom.Size = new System.Drawing.Size(987, 21);
            // 
            // dckpnlGrid
            // 
            this.dckpnlGrid.Controls.Add(this.controlContainer1);
            this.dckpnlGrid.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dckpnlGrid.FloatVertical = true;
            this.dckpnlGrid.ID = new System.Guid("0f3dab01-67b2-47f2-9d8a-ac81e838599d");
            this.dckpnlGrid.Location = new System.Drawing.Point(0, 0);
            this.dckpnlGrid.Name = "dckpnlGrid";
            this.dckpnlGrid.OriginalSize = new System.Drawing.Size(200, 136);
            this.dckpnlGrid.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dckpnlGrid.SavedIndex = 0;
            this.dckpnlGrid.Size = new System.Drawing.Size(1008, 136);
            this.dckpnlGrid.Text = "Grid view";
            this.dckpnlGrid.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.gcIssues);
            this.controlContainer1.Location = new System.Drawing.Point(3, 27);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(1002, 106);
            this.controlContainer1.TabIndex = 0;
            // 
            // hideContainerRight
            // 
            this.hideContainerRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.hideContainerRight.Controls.Add(this.pnlElementProperties);
            this.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.hideContainerRight.Location = new System.Drawing.Point(987, 0);
            this.hideContainerRight.Name = "hideContainerRight";
            this.hideContainerRight.Size = new System.Drawing.Size(21, 500);
            // 
            // pnlElementProperties
            // 
            this.pnlElementProperties.Controls.Add(this.dockPanel1_Container);
            this.pnlElementProperties.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.pnlElementProperties.ID = new System.Guid("45f80ee3-121b-427b-b4fe-76777e8c71fd");
            this.pnlElementProperties.Location = new System.Drawing.Point(0, 0);
            this.pnlElementProperties.Name = "pnlElementProperties";
            this.pnlElementProperties.OriginalSize = new System.Drawing.Size(200, 200);
            this.pnlElementProperties.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.pnlElementProperties.SavedIndex = 0;
            this.pnlElementProperties.Size = new System.Drawing.Size(200, 479);
            this.pnlElementProperties.Text = "Element Properties";
            this.pnlElementProperties.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.pgcElementProperties);
            this.dockPanel1_Container.Controls.Add(this.splitterControl1);
            this.dockPanel1_Container.Controls.Add(this.propertyDescriptionControl1);
            this.dockPanel1_Container.Controls.Add(this.panelControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 26);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(193, 450);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // splitterControl1
            // 
            this.splitterControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitterControl1.Location = new System.Drawing.Point(0, 340);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(193, 10);
            this.splitterControl1.TabIndex = 0;
            this.splitterControl1.TabStop = false;
            // 
            // propertyDescriptionControl1
            // 
            this.propertyDescriptionControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.propertyDescriptionControl1.Location = new System.Drawing.Point(0, 350);
            this.propertyDescriptionControl1.Name = "propertyDescriptionControl1";
            this.propertyDescriptionControl1.PropertyGrid = this.pgcElementProperties;
            this.propertyDescriptionControl1.Size = new System.Drawing.Size(193, 100);
            this.propertyDescriptionControl1.TabIndex = 4;
            this.propertyDescriptionControl1.TabStop = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnViewStyle);
            this.panelControl1.Controls.Add(this.btnSortPropertiesByCategory);
            this.panelControl1.Controls.Add(this.btnSortPropertiesAlphabetically);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(193, 26);
            this.panelControl1.TabIndex = 3;
            // 
            // btnViewStyle
            // 
            this.btnViewStyle.Location = new System.Drawing.Point(58, 2);
            this.btnViewStyle.Name = "btnViewStyle";
            this.btnViewStyle.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnViewStyle.Properties.Appearance.Options.UseBackColor = true;
            this.btnViewStyle.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions4.Image")));
            this.btnViewStyle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnViewStyle.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnViewStyle.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnViewStyle.Size = new System.Drawing.Size(26, 22);
            this.btnViewStyle.TabIndex = 5;
            this.btnViewStyle.ToolTip = "Change style (Office View)";
            this.btnViewStyle.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnViewStyle_ButtonClick);
            // 
            // btnSortPropertiesByCategory
            // 
            this.btnSortPropertiesByCategory.Location = new System.Drawing.Point(26, 2);
            this.btnSortPropertiesByCategory.Name = "btnSortPropertiesByCategory";
            this.btnSortPropertiesByCategory.Properties.Appearance.BackColor = System.Drawing.Color.LightBlue;
            this.btnSortPropertiesByCategory.Properties.Appearance.Options.UseBackColor = true;
            this.btnSortPropertiesByCategory.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions5.Image = global::JiraManager.UI.Properties.Resources.Categorized;
            this.btnSortPropertiesByCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnSortPropertiesByCategory.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSortPropertiesByCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnSortPropertiesByCategory.Size = new System.Drawing.Size(26, 22);
            this.btnSortPropertiesByCategory.TabIndex = 4;
            this.btnSortPropertiesByCategory.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSortPropertiesByCategory_ButtonClick);
            // 
            // btnSortPropertiesAlphabetically
            // 
            this.btnSortPropertiesAlphabetically.Location = new System.Drawing.Point(0, 2);
            this.btnSortPropertiesAlphabetically.Name = "btnSortPropertiesAlphabetically";
            this.btnSortPropertiesAlphabetically.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnSortPropertiesAlphabetically.Properties.Appearance.Options.UseBackColor = true;
            this.btnSortPropertiesAlphabetically.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions6.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions6.Image")));
            this.btnSortPropertiesAlphabetically.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnSortPropertiesAlphabetically.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnSortPropertiesAlphabetically.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnSortPropertiesAlphabetically.Size = new System.Drawing.Size(26, 22);
            this.btnSortPropertiesAlphabetically.TabIndex = 3;
            this.btnSortPropertiesAlphabetically.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSortPropertiesAlphabetically_ButtonClick);
            // 
            // btnSnapToItems
            // 
            this.btnSnapToItems.Location = new System.Drawing.Point(113, 26);
            this.btnSnapToItems.Name = "btnSnapToItems";
            this.btnSnapToItems.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnSnapToItems.Properties.Appearance.Options.UseBackColor = true;
            this.btnSnapToItems.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.btnSnapToItems.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnSnapToItems.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnSnapToItems.Size = new System.Drawing.Size(50, 38);
            this.btnSnapToItems.StyleController = this.lytctlMain;
            this.btnSnapToItems.TabIndex = 8;
            this.btnSnapToItems.ToolTip = "Add Dependencies";
            this.btnSnapToItems.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSnapToItems_ButtonClick);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnSnapToItems;
            this.layoutControlItem2.Location = new System.Drawing.Point(108, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(54, 42);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(54, 42);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(54, 42);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ProjectComponentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytctlMain);
            this.Controls.Add(this.hideContainerBottom);
            this.Controls.Add(this.hideContainerRight);
            this.Name = "ProjectComponentView";
            this.Size = new System.Drawing.Size(1008, 500);
            this.Load += new System.EventHandler(this.ProjectComponentView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcIssues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIssues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlMain)).EndInit();
            this.lytctlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnEditDependencies.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagramControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pgcElementProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlgrpIssues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerBottom.ResumeLayout(false);
            this.dckpnlGrid.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            this.hideContainerRight.ResumeLayout(false);
            this.pnlElementProperties.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnViewStyle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortPropertiesByCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortPropertiesAlphabetically.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSnapToItems.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcIssues;
        private TechnicalTools.UI.DX.EnhancedGridView gvIssues;
        private DevExpress.XtraLayout.LayoutControl lytctlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup lytctlgrpIssues;
        private TechnicalTools.UI.DX.EnhancedDiagramControl diagramControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel pnlElementProperties;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraVerticalGrid.PropertyGridControl pgcElementProperties;
        private DevExpress.XtraVerticalGrid.PropertyDescriptionControl propertyDescriptionControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.ButtonEdit btnSortPropertiesByCategory;
        private DevExpress.XtraEditors.ButtonEdit btnSortPropertiesAlphabetically;
        private DevExpress.XtraEditors.ButtonEdit btnViewStyle;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerBottom;
        private DevExpress.XtraBars.Docking.DockPanel dckpnlGrid;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerRight;
        private DevExpress.XtraEditors.ButtonEdit btnRefresh;
        private DevExpress.XtraEditors.ButtonEdit btnEditDependencies;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.ButtonEdit btnSnapToItems;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}