﻿using System;

using TechnicalTools.Tools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using ApplicationBase.DAL.Users;
using ApplicationBase.UI;

using JiraManager.Business;
using JiraManager.Business.API.DtoObjects;
using JiraManager.UI.Helpers;
using JiraManager.Business.DataRelation;
using JiraManager.Business.Controllers;


namespace JiraManager.UI.Views
{
    public partial class OverviewManagerView : ApplicationBaseView
    {
        RepositoryController Controller { get { return (RepositoryController)BusinessObject; } }

        [Obsolete("For designer only")]
        public OverviewManagerView() : this(null) { }

        public OverviewManagerView(RepositoryController repository)
            : base(repository, Feature.AllowEverybody)
        {
            InitializeComponent();
            Text = "JIRA Overview";
            _refreshPostponer = new PostponingExecuter(RefreshProjectComponents);
            var repo = BusinessSingletons.Instance.GetRepository();
            repo.Refreshed += _refreshPostponer.SchedulePostponedExecutionAsEventHandler;
            Disposed += (_, __) => repo.Refreshed -= _refreshPostponer.SchedulePostponedExecutionAsEventHandler;
        }

        
        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
            // Application des droits utilisateurs
            gvProjectComponents.OptionsBehavior.ReadOnly = true;
            gvProjectComponents.OptionsBehavior.Editable = false;
        }

        void OverviewManagerView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            gvProjectComponents.RowItemDoubleClick += gvProjectComponents_RowItemDoubleClick;
            // Invoke to catch exception instead of letting .Net framework to ignore them
            this.BeginInvokeSafely(() =>
            {
                GridView_ConfiguratorProxy.Instance.ConfigureFor<ProjectComponent>(gvProjectComponents, HierarchyProvider.Instance);
            });
            this.BeginInvokeSafely(RefreshProjectComponents);
        }
        readonly PostponingExecuter _refreshPostponer;

        
        private void lytctlgrpProjectComponents_CustomButtonClick(object sender, DevExpress.XtraBars.Docking2010.BaseButtonEventArgs e)
        {
            RefreshProjectComponents();
        }
        void RefreshProjectComponents()
        {
            WaitControlGeneric.ShowOnControl(gcProjectComponents, "Refreshing",
                            () => Controller.GetProjectComponents(),
                            components =>
                            {
                                bool firstTime = gcProjectComponents.DataSource == null;
                                gcProjectComponents.DataSource = components;
                                if (firstTime)
                                    gvProjectComponents.BestFitColumns();
                            });
        }
       
        private void gvProjectComponents_RowItemDoubleClick(object sender, RowItemDoubleClickEventArgs e)
        {
            var component = (ProjectComponent)e.Item;
            UIControler.Instance.ShowObject(component);
        }
    }
}

