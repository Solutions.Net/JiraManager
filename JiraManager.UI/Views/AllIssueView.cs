﻿using System;

using DevExpress.XtraEditors.ButtonsPanelControl;

using TechnicalTools;
using TechnicalTools.Tools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Helpers;

using ApplicationBase.DAL.Users;
using ApplicationBase.UI;

using JiraManager.Business;
using JiraManager.Business.API.DtoObjects;
using JiraManager.UI.Helpers;
using JiraManager.Business.DataRelation;

namespace JiraManager.UI.Views
{
    public partial class AllIssuesView : ApplicationBaseView
    {
        SetSnapshot<Issue> IssuesSetSnapshot { get { return (SetSnapshot<Issue>)BusinessObject; } }

        [Obsolete("For designer only")]
        public AllIssuesView() : this(null) { }

        public AllIssuesView(SetSnapshot<Issue> manager)
            : base(manager, Feature.AllowEverybody)
        {
            InitializeComponent();
            Text = "All Issues";
            _refreshPostponer = new PostponingExecuter(RefreshGridView);
            var repo = BusinessSingletons.Instance.GetRepository();
            repo.Refreshed -= _refreshPostponer.SchedulePostponedExecutionAsEventHandler;
            repo.Refreshed += _refreshPostponer.SchedulePostponedExecutionAsEventHandler;
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
            // Application des droits utilisateurs
            gvIssues.OptionsBehavior.ReadOnly = true;
        }

        void AllIssuesView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            // Invoke to catch exception instead of letting .Net framework to ignore them
            this.BeginInvokeSafely(() =>
            {
                gcIssues.DataSource = IssuesSetSnapshot;
                GridView_ConfiguratorProxy.Instance.ConfigureFor<Issue>(gvIssues, HierarchyProvider.Instance);
            });
        }
        readonly PostponingExecuter                 _refreshPostponer;

        private void lytctlgrpIssues_CustomButtonClick(object sender, DevExpress.XtraBars.Docking2010.BaseButtonEventArgs e)
        {
            RefreshGridView();
        }
        void RefreshGridView()
        {
            var btn = (GroupBoxButton)lytctlgrpIssues.CustomHeaderButtons[0];
            btn.Enabled = false;
            WaitControlGeneric.ShowOnControl(gcIssues, "Refreshing",
                () => IssuesSetSnapshot.Refresh(new Progress<string>()),
                gcIssues.RefreshDataSource, // everything is done by GridView_ConfiguratorProxy
                ex => { btn.Enabled = true; ex?.Rethrow(); });
        }
       
    }
}

