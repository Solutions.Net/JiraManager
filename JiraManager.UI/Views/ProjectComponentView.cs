﻿using ApplicationBase.DAL.Users;
using ApplicationBase.UI;

using DevExpress.Diagram.Core;
using DevExpress.Diagram.Core.Layout;
using DevExpress.Diagram.Core.Layout.Native;
using DevExpress.Diagram.Core.Native;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraDiagram;
using DevExpress.XtraEditors;
using JiraManager.Business;
using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.ObjectExtensions;
using JiraManager.UI.ViewObjects;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using TechnicalTools;
using TechnicalTools.Algorithm.Graph;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;


namespace JiraManager.UI.Views
{
    public partial class ProjectComponentView : ApplicationBaseView
    {
        ProjectComponent Component { get { return (ProjectComponent)BusinessObject; } }

        [Obsolete("For designer only")]
        public ProjectComponentView() : this(null) { }

        public ProjectComponentView(ProjectComponent component)
            : base(component, Feature.AllowEverybody)
        {
            InitializeComponent();
            Text = "Project: " + component.Project.Key + " - Component: " + component.Name;

            if (DesignTimeHelper.IsInDesignMode)
                return;

            diagramControl.SelectOnRightClick = false;
            diagramControl.OptionsView.ShowGrid = false;
            diagramControl.OptionsView.ShowPageBreaks = false;
            diagramControl.OptionsView.ShowRulers = false;
            
            
            diagramControl.OptionsBehavior.EnableProportionalResizing = false;
            //diagramControl1.OptionsBehavior.AllowEmptySelection = true;
            diagramControl.OptionsBehavior.MinDragDistance = 1;
            //diagramControl.OptionsView.PaperKind = System.Drawing.Printing.PaperKind.A2;
            diagramControl.OptionsView.CanvasSizeMode = CanvasSizeMode.Fill;
            diagramControl.MouseHover += DiagramControl_MouseHover;
            diagramControl.PopupMenuShowing += DiagramControl_PopupMenuShowing;
            //diagramControl.Commands.RegisterHandlers(x => x.RegisterHandlerCore(DiagramCommandsBase.DeleteCommand, (param, diagram, getArgs, baseHandler) => { }, (param, diagram, baseCanExecute) => false));
            //diagramControl.Commands.RegisterHandlers(x => x.RegisterHandlerCore(DiagramCommandsBase.StartDragToolCommand, (param, diagram, getArgs, baseHandler) => { }, (param, diagram, baseCanExecute) => false));
            //diagramControl.Commands.RegisterHandlers(x => x.RegisterHandlerCore(DiagramCommandsBase.StartDragToolAlternateCommand, (param, diagram, getArgs, baseHandler) => { }, (param, diagram, baseCanExecute) => false));
            diagramControl.ItemsChanged += DiagramControl_ItemsChanged;
            diagramControl.ItemsDeleting += DiagramControl_ItemsDeleting;
            diagramControl.ItemsMoving += DiagramControl_ItemsMoving;

            // From https://supportcenter.devexpress.com/ticket/details/t490970/diagram-control-large-hierarchy-performance
            // Should make performance better, but this is still slow :/
            IssueLinkView.DefaultConnectorType = ConnectorType.Straight;
            diagramControl.OptionsConnector.LineJumpPlacement = LineJumpPlacement.None;


            diagramControl.ConnectionChanged += DiagramControl_ConnectionChanged;
            InitSummaryinToolTip();

            diagramControl.OptionsView.PropertiesPanelVisibility = PropertiesPanelVisibility.Visible;
            diagramControl.OptionsBehavior.AllowPropertiesPanel = true;
            // For the property grid 
            diagramControl.CustomGetEditableItemProperties += IssueView.GetCustomGetEditableItemProperties;
            // When content is edited from the diagram or property grid

            // sync  UI with default behavior
            btnSortPropertiesByCategory.PerformClick(btnSortPropertiesByCategory.Properties.Buttons.Single());
            btnSnapToItems.PerformClick(btnSortPropertiesByCategory.Properties.Buttons.Single());
            if (((ConfigOfUser)BusinessSingletons.Instance.GetConfigOfUser()).ShowPropertiesAsOffice)
                btnViewStyle.PerformClick(btnViewStyle.Properties.Buttons.Single());
        }

        private void DiagramControl_ItemsChanged(object sender, DiagramItemsChangedEventArgs e)
        {
            
        }

        private void DiagramControl_ItemsMoving(object sender, DiagramItemsMovingEventArgs e)
        {
            
        }

        private void DiagramControl_ItemsDeleting(object sender, DiagramItemsDeletingEventArgs e)
        {
            e.Cancel = true;
        }

        private void DiagramControl_ConnectionChanged(object sender, DiagramConnectionChangedEventArgs e)
        {

        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            base.RefreshEnabilitiesAndVisibilities();
            // Application des droits utilisateurs
        }

        void ProjectComponentView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
        }

        #region Feature Display Issue summary as tooltip

        void InitSummaryinToolTip()
        {
            components = components ?? new System.ComponentModel.Container();
            var toolTipController = new ToolTipController(components);
            diagramControl.ToolTipController = toolTipController;
            toolTipController.GetActiveObjectInfo += toolTipController_GetActiveObjectInfo;
        }

        void toolTipController_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl != diagramControl)
                return;

            ToolTipControlInfo toolTipInfo = null;
            var issueShape = diagramControl.CalcHitItem(e.ControlMousePosition) as IssueView;
            if (issueShape != null)
                toolTipInfo = new ToolTipControlInfo(issueShape, issueShape.Issue.Summary.IfBlankUse(issueShape.Issue.GetLocalData(false)?.Summary));

            if (toolTipInfo != null)
                e.Info = toolTipInfo;
        }

        #endregion Feature Display Issue summary as tooltip


        private void btnRefresh_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var repo = BusinessSingletons.Instance.GetRepository();
            // I prefer this because WaitControlGeneric does not handle Progress (yet)
            this.ShowBusyWhileDoing("Refreshing", pr => Component.GetIssues(pr).ToHashSet()
                                                        //repo.Issues.Where(issue => issue.StatusCategory != StatusCategory.Done).ToHashSet()
                                                        )
                .ContinueWithUIWorkOnSuccess(issues => this.BeginInvokeSafely(() => this.ShowBusyWhileDoingUIWorkInPlace(pr => DisplayIssues(issues, pr))));
            //WaitControlGeneric.ShowOnControl(diagramControl, "Refreshing",
            //    () => Project.GetIssues(new Progress<string>()), DisplayIssues);
        }

        void DisplayIssues(HashSet<Issue> issues, IProgress<string> pr)
        {
            // To Test with a tiny graph
            //issues = CompleteSetOfAccessibleIssues(issues.OrderByDescending(issue => issue.BlockLinks.Count()).Take(1));

            gcIssues.DataSource = issues;

            diagramControl.BeginUpdate();
            diagramControl.Items.BeginUpdate();
            diagramControl.Items.Clear();
            _views.Clear();

            #region Just add all "views" of issue & links to diagram with mapping to model and restore known position
            var accessible = CompleteSetOfAccessibleIssues(issues);

            foreach (var issue in accessible)
            {
                var view = new IssueView(issue);
                _views.Add(issue, view);
                diagramControl.Items.Add(view);
            }

            // Nest issue in their parents
            pr.Report("Creating issue views");
            var viewsToFit = new List<IssueView>();
            foreach (var view in _views.Values)
            {
                viewsToFit.Add(view);
                if (view.Issue.ParentTask != null)
                {
                    var parentView = _views.TryGetValueClass(view.Issue.ParentTask);
                    if (parentView != null)
                    {
                        view.MoveToParent(parentView);
                        viewsToFit.Add(parentView); // reschedule 
                    }
                }
            }

            // Fit all view from the most nested to the most parent
            foreach (var view in viewsToFit)
            {
                view.FitToContent();
               //view.Height = 150;
            }
                

            // Show links between issue
            pr.Report("Creating links between issues");
      
            var ignoredLinks = new Dictionary<Issue, List<Issue>>();
            var orderedIssues = TopologicalOrderSimple.DoTopologicalSort(accessible.OrderBy(issue => issue.Created), issue => issue.BlockLinks.Select(lnk => lnk.InwardIssue), (from, to, _) =>
            {
                var lst = ignoredLinks.GetValueOrCreateDefault(from);
                lst.Add(to);
            });//.Where(issue => issues.Contains(issue)).ToList(); // remove back the issue done

            string cycles = "";
            foreach (var issue in orderedIssues)
            {
                var view = _views[issue];
                var lst = ignoredLinks.TryGetValueClass(issue);
                foreach (var lnk in issue.BlockLinks)
                {
                    if (lst != null && lst.Contains(lnk.InwardIssue))
                    {
                        cycles += "- " + lnk.OutwardIssue.Key + " and " + lnk.InwardIssue.Key + Environment.NewLine;
                        // TODO : Warn user about this ignored link (causing a cycle)
                        continue;
                    }
                    var blockedIssueView = _views[lnk.InwardIssue];
                    var linkView = new IssueLinkView(lnk, view, blockedIssueView);
                    diagramControl.Items.Add(linkView);
                }
            }

            if (cycles.Length > 0)
            {
                cycles = "Some issues auto reference each other (maybe with indirect path), which seems invalid:" + Environment.NewLine
                       + cycles;
                this.BeginInvokeSafely(() => XtraMessageBox.Show(this, cycles));
            }


            #endregion

            #region Heuristic to place new issue views

            // Capture issue views never moved by user
            var untouchedViews = _views.Values
                                       .Where(iv => iv.Issue.GetLocalData(false) == null)
                                       .OrderBy(iv => iv.Issue?.Created ?? DateTime.MaxValue)
                                       .ToList();
            if (untouchedViews.Count > 0)
            {
                //var layoutSettings = new SugiyamaLayoutSettings(40.0, 40.0, LayoutDirection.LeftToRight);
                //diagramControl.ApplySugiyamaLayout(layoutSettings, untouchedViews);
                pr.Report("Laying out views... (can take times)");
                var layoutSettings = new MindMapTreeLayoutSettings(40.0, 40.0, OrientationKind.Horizontal);
                
                diagramControl.ApplyMindMapTreeLayoutForSubordinates(untouchedViews);
                //https://stackoverflow.com/questions/16775844/force-graphviz-to-preserve-node-positions
            }

            #endregion

            foreach (var view in _views.Values)
                view.ListenChangeAndAssignModel();


            //var viewsToFit = new List<IssueView>();
            //foreach (var issueView in issueViews)
            //{
            //    viewsToFit.Add(issueView);
            //    if (issueView.Issue.ParentTask != null)
            //    {
            //        var parentIssueView = _views.TryGetValueClass(issueView.Issue.ParentTask);
            //        issueView.MoveToParent(parentIssueView);
            //        viewsToFit.Add(parentIssueView);
            //    }
            //}
            //foreach (var issueView in viewsToFit)
            //    issueView.FitToContent();



            //// Fit diagram windows to let user see all issues
            diagramControl.FitToItems(_views.Values);
            pr.Report("Finalizing...");
            diagramControl.Items.EndUpdate();
            diagramControl.EndUpdate();
        }
        readonly Dictionary<Issue, IssueView> _views = new Dictionary<Issue, IssueView>();

        HashSet<Issue> CompleteSetOfAccessibleIssues(IEnumerable<Issue> issues, bool bothDirection = false)
        {
            var set = new HashSet<Issue>(issues);
            var queue = new Queue<Issue>(issues);
            while (queue.Count > 0)
            {
                var i = queue.Dequeue();
                foreach (var other in i.BlockLinks)
                    if (!set.Contains(other.InwardIssue))
                    {
                        set.Add(other.InwardIssue);
                        queue.Enqueue(other.InwardIssue);
                    }
                if (bothDirection)
                    foreach (var other in i.BlockedByLinks)
                        if (!set.Contains(other.OutwardIssue))
                        {
                            set.Add(other.OutwardIssue);
                            queue.Enqueue(other.OutwardIssue);
                        }
            }
            return set;
        }

        private void btnEditDependencies_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var toolIsConnectorNow = diagramControl.OptionsBehavior.ActiveTool == diagramControl.OptionsBehavior.PointerTool;
            diagramControl.OptionsBehavior.ActiveTool = toolIsConnectorNow
                                                      ? diagramControl.OptionsBehavior.ConnectorTool
                                                      : diagramControl.OptionsBehavior.PointerTool;
            btnEditDependencies.BackColor = toolIsConnectorNow
                                          ? Color.LightBlue
                                          : Color.Transparent;
        }

        private void btnSnapToItems_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            diagramControl.OptionsBehavior.SnapToItems = !diagramControl.OptionsBehavior.SnapToItems;
            btnEditDependencies.BackColor = diagramControl.OptionsBehavior.SnapToItems
                                          ? Color.LightBlue
                                          : Color.Transparent;
        }

        private void DiagramControl_MouseHover(object sender, EventArgs e)
        {
            //point = diagramControl.PointToDocument(new PointFloat(e.X, e.Y));
        }
        // https://supportcenter.devexpress.com/ticket/details/t618829/example-for-diagram-containers-from-code-in-winforms
        // https://supportcenter.devexpress.com/ticket/details/t455083/how-to-modify-the-position-of-turn-points-of-diagramconnector
        // https://supportcenter.devexpress.com/ticket/details/t655046/moving-connectors-along-with-moving-shapes-when-dragging-shape
        // https://supportcenter.devexpress.com/ticket/details/t586483/how-to-add-a-connection-point-to-diagram-shape
        // https://supportcenter.devexpress.com/ticket/details/t741675/constant-stroke-width-at-different-zoom-levels
        // https://supportcenter.devexpress.com/ticket/details/t832842/change-appearance-of-diagramconnector-linejoin-linecap-curved-edges
        // https://supportcenter.devexpress.com/ticket/details/t571461/how-to-change-diagramconnector-s-appearance-when-it-s-focused
        // https://supportcenter.devexpress.com/ticket/details/t631983/show-text-to-left-of-diagramconnector-s-content-in-diagram-control
        // https://supportcenter.devexpress.com/ticket/details/t581666/prevent-user-from-deleting-incoming-connections-when-deleting-a-shape
        // https://supportcenter.devexpress.com/ticket/details/t349919/how-to-keep-intermediate-points-in-a-diagramconnector-when-a-shape-is-moved
        // https://supportcenter.devexpress.com/Ticket/Details/T335254/is-possibility-to-add-connection-points-to-shape
        // https://supportcenter.devexpress.com/ticket/details/t550056/how-to-adjust-the-snaptogrid-distance-so-that-it-s-impossible-to-move-a-shape-to-a-non
        // https://docs.devexpress.com/WindowsForms/116904/controls-and-libraries/diagrams/automatic-layout
        // https://supportcenter.devexpress.com/ticket/details/t723295/a-long-delay-occurs-in-diagramcontrol-when-the-applylayout-method-is-called-in-a-large
        private void DiagramControl_PopupMenuShowing(object sender, EnhancedDiagramControl.MouseContextMenuClick e)
        {
            if (e.Menu == null)
                return;
            DiagramItem item = diagramControl.CalcHitItem(e.DocumentLocation);
            var mnu = new DXMenuItem() { Caption = "Create new task" };
            mnu.Click += (_, __) =>
            {
                var repo = BusinessSingletons.Instance.GetRepository();
                var issue = repo.CreateIssue(Component);
                var view = CreateIssueShape(issue);
                view.X = e.DocumentLocation.X;
                view.Y = e.DocumentLocation.Y;
                //var container = new DiagramContainer();
                //container.X = e.DocumentLocation.X;
                //container.Y = e.DocumentLocation.Y;
                //container.Appearance.BorderColor = System.Drawing.Color.Red;
                //container.Items.Add(view);
                //diagramControl.Items.Add(container);
                diagramControl.Items.Add(view);
                this.BeginInvokeSafely(view.FitToContent); // postponed because font is set later :/
                //container.Width = 100;
                //container.Height = 25;
            };
            e.Menu.Items.Add(mnu);

            mnu = new DXMenuItem() { Caption = "Fit view to see all issues" };
            mnu.Click += (_, __) =>
            {
                diagramControl.FitToItems(diagramControl.Items);
            };
            e.Menu.Items.Add(mnu);

            var itemClicked = diagramControl.CalcHitItem(e.Location);
            itemClicked = (itemClicked as HeaderShape)?.Parent ?? itemClicked;
            var issueView = itemClicked as IssueView;
            if (issueView != null)
            {
                mnu = new DXMenuItem() { Caption = "Fit to content" };
                mnu.Click += (_, __) =>
                {
                    issueView.FitToContent();
                };
                e.Menu.Items.Add(mnu);
                mnu = new DXMenuItem() { Caption = "Delete task" };
                mnu.Click += (_, __) =>
                {
                    diagramControl.Items.Remove(issueView);
                };
                e.Menu.Items.Add(mnu);
                e.Menu.Items.Add(new DXMenuItem() { Caption = "-" });
                mnu = new DXMenuItem() { Caption = "Bring to front" };
                mnu.Click += (_, __) =>
                {
                    diagramControl.BringItemsToFront(issueView.Yield());
                };
                e.Menu.Items.Add(mnu);
                mnu = new DXMenuItem() { Caption = "Send to back" };
                mnu.Click += (_, __) =>
                {
                    diagramControl.SendItemsToBack(issueView.Yield());
                };
                e.Menu.Items.Add(mnu);

                mnu = new DXMenuItem() { Caption = "Lay out children" };
                mnu.Click += (_, __) =>
                {
                    // Code from https://supportcenter.devexpress.com/ticket/details/t717489/performing-automatic-layout-on-shapes-inside-a-container
                    var viewsToLayOut = issueView.Items.OfType<IssueView>().ToHashSet();
                    var issuesToLayOut = viewsToLayOut.Select(iv => iv.Issue).ToList();
                    var connnectors = viewsToLayOut.SelectMany(iv => iv.GetAttachedConnectors()).ToList(); // We don't want to take connector that cross issueView boundaries (ie: in-to-out connector or reverse))
                    var edges = connnectors.Select(con => new Edge<IDiagramItem>(con.BeginItem, con.EndItem)).ToList();
                    var graph = new Graph<IDiagramItem>(viewsToLayOut, edges);

                    Rect containerRect = issueView.RotatedClientDiagramBounds().BoundedRect();
                    PageInfo pageInfo = new PageInfo(containerRect.Size, containerRect);

                    diagramControl.RelayoutDiagramItems(TreeLayout.LayoutGraph(graph, t => t.Size, new GraphTreeLayoutSettings(new TreeLayoutSettings(diagramControl.OptionsTreeLayout.HorizontalSpacing, diagramControl.OptionsTreeLayout.VerticalSpacing, diagramControl.OptionsTreeLayout.Direction), pageInfo, SplitToConnectedComponentsMode.AllComponents)));
                };
                mnu.Enabled = issueView.Items.Skip(1).Any();
                e.Menu.Items.Add(mnu);


            }
            if (diagramControl.SelectedItems.Count > 0)
            {
                mnu = new DXMenuItem() { Caption = "Layout selected items" };
                mnu.Click += (_, __) =>
                {
                    var layoutSettings = new SugiyamaLayoutSettings(40.0, 40.0, LayoutDirection.LeftToRight);
                    //diagramControl.ApplySugiyamaLayout(layoutSettings, diagramControl.SelectedItems);
                    //var circularSettings = new CircularLayoutSettings(40.0, CircularLayoutOrder.Optimal);
                    //diagramControl.ApplyCircularLayout(diagramControl.SelectedItems);
                    var settings = new MindMapTreeLayoutSettings(40, 40, OrientationKind.Vertical);
                    diagramControl.ApplyMindMapTreeLayoutForSubordinates(diagramControl.SelectedItems, settings);
                    diagramControl.ApplyOrgChartLayout(diagramControl.SelectedItems);
                };
                e.Menu.Items.Add(mnu);
            }
        }
        IssueView CreateIssueShape(Issue issue, bool bindEvents = true)
        {
            var issueView = new IssueView(issue);
            _views.Add(issueView.Issue, issueView);
            if (bindEvents)
                issueView.ListenChangeAndAssignModel();
            return issueView;
        }

        private void btnSortPropertiesAlphabetically_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            pgcElementProperties.OptionsView.ShowRootCategories = false;
            btnSortPropertiesAlphabetically.BackColor = Color.LightBlue;
            btnSortPropertiesByCategory.BackColor = Color.Transparent;
        }
        private void btnSortPropertiesByCategory_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            pgcElementProperties.OptionsView.ShowRootCategories = true;
            btnSortPropertiesAlphabetically.BackColor = Color.Transparent;
            btnSortPropertiesByCategory.BackColor = Color.LightBlue;
        }
        private void btnViewStyle_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var isOfficeNow = pgcElementProperties.ActiveViewType == DevExpress.XtraVerticalGrid.PropertyGridView.Classic;
            pgcElementProperties.ActiveViewType = isOfficeNow
                                                ? DevExpress.XtraVerticalGrid.PropertyGridView.Office
                                                : DevExpress.XtraVerticalGrid.PropertyGridView.Classic;
            btnViewStyle.BackColor = isOfficeNow ? Color.LightBlue : Color.Transparent;
            var cou = (ConfigOfUser)BusinessSingletons.Instance.GetConfigOfUser();
            cou.ShowPropertiesAsOffice = isOfficeNow;
        }


    }
}

