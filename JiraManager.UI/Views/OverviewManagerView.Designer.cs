﻿namespace JiraManager.UI.Views
{
    partial class OverviewManagerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions1 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OverviewManagerView));
            this.gcProjectComponents = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvProjectComponents = new TechnicalTools.UI.DX.EnhancedGridView();
            this.lytctlMain = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.lytctlgrpProjectComponents = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gcIssues_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gcProjectComponents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvProjectComponents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlMain)).BeginInit();
            this.lytctlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlgrpProjectComponents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcIssues_LayoutItem)).BeginInit();
            this.SuspendLayout();
            // 
            // gcProjectComponents
            // 
            this.gcProjectComponents.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcProjectComponents.EmbeddedNavigator.TextStringFormat = "Project {0} of {1} (visible) of {2} (total)";
            this.gcProjectComponents.Location = new System.Drawing.Point(24, 59);
            this.gcProjectComponents.MainView = this.gvProjectComponents;
            this.gcProjectComponents.Name = "gcProjectComponents";
            this.gcProjectComponents.Size = new System.Drawing.Size(960, 626);
            this.gcProjectComponents.TabIndex = 0;
            this.gcProjectComponents.UseEmbeddedNavigator = true;
            this.gcProjectComponents.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvProjectComponents});
            // 
            // gvProjectComponents
            // 
            this.gvProjectComponents.GridControl = this.gcProjectComponents;
            this.gvProjectComponents.GroupFormat = "[#image]{1} {2}";
            this.gvProjectComponents.Name = "gvProjectComponents";
            this.gvProjectComponents.OptionsView.ColumnAutoWidth = false;
            this.gvProjectComponents.OptionsView.ShowAutoFilterRow = true;
            // 
            // lytctlMain
            // 
            this.lytctlMain.Controls.Add(this.gcProjectComponents);
            this.lytctlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytctlMain.Location = new System.Drawing.Point(0, 0);
            this.lytctlMain.Name = "lytctlMain";
            this.lytctlMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(834, 147, 658, 350);
            this.lytctlMain.Root = this.layoutControlGroup1;
            this.lytctlMain.Size = new System.Drawing.Size(1008, 729);
            this.lytctlMain.TabIndex = 1;
            this.lytctlMain.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.splitterItem1,
            this.splitterItem2,
            this.lytctlgrpProjectComponents});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1008, 729);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(0, 689);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(988, 10);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.Location = new System.Drawing.Point(0, 699);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(988, 10);
            // 
            // lytctlgrpProjectComponents
            // 
            buttonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("buttonImageOptions1.Image")));
            this.lytctlgrpProjectComponents.CustomHeaderButtons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("Button", false, buttonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1)});
            this.lytctlgrpProjectComponents.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gcIssues_LayoutItem});
            this.lytctlgrpProjectComponents.Location = new System.Drawing.Point(0, 0);
            this.lytctlgrpProjectComponents.Name = "lytctlgrpProjectComponents";
            this.lytctlgrpProjectComponents.Size = new System.Drawing.Size(988, 689);
            this.lytctlgrpProjectComponents.Text = "Projects && Components";
            this.lytctlgrpProjectComponents.CustomButtonClick += new DevExpress.XtraBars.Docking2010.BaseButtonEventHandler(this.lytctlgrpProjectComponents_CustomButtonClick);
            // 
            // gcIssues_LayoutItem
            // 
            this.gcIssues_LayoutItem.Control = this.gcProjectComponents;
            this.gcIssues_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.gcIssues_LayoutItem.Name = "gcIssues_LayoutItem";
            this.gcIssues_LayoutItem.Size = new System.Drawing.Size(964, 630);
            this.gcIssues_LayoutItem.Text = "Projects:";
            this.gcIssues_LayoutItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.gcIssues_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.gcIssues_LayoutItem.TextVisible = false;
            // 
            // OverviewManagerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytctlMain);
            this.Name = "OverviewManagerView";
            this.Size = new System.Drawing.Size(1008, 729);
            this.Load += new System.EventHandler(this.OverviewManagerView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcProjectComponents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvProjectComponents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlMain)).EndInit();
            this.lytctlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlgrpProjectComponents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcIssues_LayoutItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcProjectComponents;
        private TechnicalTools.UI.DX.EnhancedGridView gvProjectComponents;
        private DevExpress.XtraLayout.LayoutControl lytctlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem gcIssues_LayoutItem;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.LayoutControlGroup lytctlgrpProjectComponents;
    }
}