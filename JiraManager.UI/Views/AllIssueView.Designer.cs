﻿namespace JiraManager.UI.Views
{
    partial class AllIssuesView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions buttonImageOptions1 = new DevExpress.XtraEditors.ButtonsPanelControl.ButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AllIssuesView));
            this.lytctlMain = new DevExpress.XtraLayout.LayoutControl();
            this.gcIssues = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lytctlgrpIssues = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gcIssues_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.gvIssues = new TechnicalTools.UI.DX.EnhancedGridView();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlMain)).BeginInit();
            this.lytctlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcIssues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlgrpIssues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcIssues_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIssues)).BeginInit();
            this.SuspendLayout();
            // 
            // lytctlMain
            // 
            this.lytctlMain.Controls.Add(this.gcIssues);
            this.lytctlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytctlMain.Location = new System.Drawing.Point(0, 0);
            this.lytctlMain.Name = "lytctlMain";
            this.lytctlMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(834, 147, 658, 350);
            this.lytctlMain.Root = this.layoutControlGroup1;
            this.lytctlMain.Size = new System.Drawing.Size(1008, 729);
            this.lytctlMain.TabIndex = 1;
            this.lytctlMain.Text = "layoutControl1";
            // 
            // gcIssues
            // 
            this.gcIssues.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcIssues.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcIssues.EmbeddedNavigator.TextStringFormat = "Issue {0} of {1} (visible) of {2} (total)";
            this.gcIssues.Location = new System.Drawing.Point(14, 49);
            this.gcIssues.MainView = this.gvIssues;
            this.gcIssues.Name = "gcIssues";
            this.gcIssues.Size = new System.Drawing.Size(980, 666);
            this.gcIssues.TabIndex = 6;
            this.gcIssues.UseEmbeddedNavigator = true;
            this.gcIssues.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvIssues});
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lytctlgrpIssues});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1008, 729);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lytctlgrpIssues
            // 
            buttonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("buttonImageOptions1.Image")));
            this.lytctlgrpIssues.CustomHeaderButtons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("Button", false, buttonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1)});
            this.lytctlgrpIssues.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gcIssues_LayoutItem});
            this.lytctlgrpIssues.Location = new System.Drawing.Point(0, 0);
            this.lytctlgrpIssues.Name = "lytctlgrpIssues";
            this.lytctlgrpIssues.Size = new System.Drawing.Size(1008, 729);
            this.lytctlgrpIssues.Text = "All issues for all projects";
            this.lytctlgrpIssues.CustomButtonClick += new DevExpress.XtraBars.Docking2010.BaseButtonEventHandler(this.lytctlgrpIssues_CustomButtonClick);
            // 
            // gcIssues_LayoutItem
            // 
            this.gcIssues_LayoutItem.Control = this.gcIssues;
            this.gcIssues_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.gcIssues_LayoutItem.Name = "gcIssues_LayoutItem";
            this.gcIssues_LayoutItem.Size = new System.Drawing.Size(984, 670);
            this.gcIssues_LayoutItem.Text = "Issues:";
            this.gcIssues_LayoutItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.gcIssues_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.gcIssues_LayoutItem.TextVisible = false;
            // 
            // gvIssues
            // 
            this.gvIssues.GridControl = this.gcIssues;
            this.gvIssues.GroupFormat = "[#image]{1} {2}";
            this.gvIssues.Name = "gvIssues";
            this.gvIssues.OptionsView.ColumnAutoWidth = false;
            this.gvIssues.OptionsView.ShowAutoFilterRow = true;
            // 
            // AllIssuesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytctlMain);
            this.Name = "AllIssuesView";
            this.Size = new System.Drawing.Size(1008, 729);
            this.Load += new System.EventHandler(this.AllIssuesView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lytctlMain)).EndInit();
            this.lytctlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcIssues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytctlgrpIssues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcIssues_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIssues)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControl lytctlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private TechnicalTools.UI.DX.EnhancedGridControl gcIssues;
        private DevExpress.XtraLayout.LayoutControlItem gcIssues_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlGroup lytctlgrpIssues;
        private TechnicalTools.UI.DX.EnhancedGridView gvIssues;
    }
}