using System;
using System.Collections.Generic;
using System.Linq;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.UI
{
    public class UIControler : ApplicationBase.UI.UIControler
    {
        // Please See "Note 01" in file "ApplicationBase.Common/Code Design Notes.txt"
        public new static UIControler Instance
        {
            get { return _Instance; }
            set
            {
                _Instance = value;
                if (ApplicationBase.UI.UIControler.Instance == null ||
                    value != null && ApplicationBase.UI.UIControler.Instance.GetType().IsAssignableFrom(value.GetType()))
                    ApplicationBase.UI.UIControler.Instance = value;
            }
        }
        static UIControler _Instance;

        static UIControler()
        {
            Instance = new UIControler();
        }

        protected override IReadOnlyCollection<Mapping> AllMappings
        {
            get
            {
                return base.AllMappings.Concat(new List<Mapping>()
                {
                     Mapping.Create((Business.Controllers.RepositoryController controller) => new Views.OverviewManagerView(controller)),
                     Mapping.Create((ProjectComponent component) => new Views.ProjectComponentView(component)),
                     Mapping.Create((Business.SetSnapshot<Issue> issues) => new Views.AllIssuesView(issues)),

                }).ToList();
            }
        }
    }
}

