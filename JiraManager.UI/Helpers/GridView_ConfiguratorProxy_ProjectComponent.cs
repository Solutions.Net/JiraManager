﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

using TechnicalTools;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Helpers;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.UI.Helpers
{
    public class GridView_ConfiguratorProxy_ForProjectComponent
    {
        Control _owner;
        GridView _view;
        GridBand _band;
        string _nestedProperty;
        // Get an project component when gridview row is not directly of type project component
        // This is useful for composition when in grid wewant to display a type "A", which is composed of a property of type ProjectComponent 
        // and we want to display in the same grid  the object ProjectComponent (relation 1-0 or 1-1 between A and ProjectComponent)
        Func<object, ProjectComponent> _getProjectComponent = x => (ProjectComponent)x;

        public GridViewDataSourceFlattenizer Flattenizer { get; private set; }

        // Generate column on first band
        public void Install(Control owner, EnhancedGridView view, Func<object, ProjectComponent> getProjectComponent = null)
        {
            _getProjectComponent = getProjectComponent ?? _getProjectComponent;
            _Install(owner, view);
        }
        public void Install(Control owner, EnhancedBandedGridView view, string nestedProperty, GridBand band, Func<object, ProjectComponent> getProjectComponent = null)
        {
            _nestedProperty = nestedProperty.IfNotBlankAddSuffix(".") ?? "";
            _band = band;
            _getProjectComponent = getProjectComponent ?? _getProjectComponent;
            _Install(owner, view);
        }
        void _Install(Control owner, GridView view)
        {
            if (_view != null)
                Uninstall();
            _owner = owner;
            _view = view;
            _view.BeginUpdate();

            // we use Editable = false instead of "readonly = true" because user wants to :
            // select part of text in a cell and copy it 
            // with readonly we have to copy all cell or nothing which is annoying for user when he has to copy ticket number etc
            _view.OptionsBehavior.Editable = false;
            _view.OptionsView.ColumnAutoWidth = false;
            _view.OptionsView.ShowAutoFilterRow = true;
            _view.OptionsView.RowAutoHeight = true;
            _view.OptionsDetail.AllowExpandEmptyDetails = true; // Evite que MasterRowGetChildList soit appelé deux fois
            _view.LevelIndent = -1;

            ConfigureProjectComponentColumns(_view);

            _view.EndUpdate();
        }

        public void Uninstall()
        {
            _view.RowCellStyle -= GridView_RowCellStyle;
            _view.DataSourceChanged -= View_DataSourceChanged;
            if (_view.GridControl is EnhancedGridControl egc)
                egc.DataSourceRefreshed -= Grid_DataSourceChanged;
            _view = null;
        }

        public object GetRow(int rowHandle)
        {
            if (rowHandle < 0)
                return null;
            var obj = Flattenizer != null
                    ? Flattenizer.GetRowBusinessObject(rowHandle)
                    : _view.GetRow(rowHandle);
            return _getProjectComponent(obj);
        }
        public object GetRow(object row)
        {
            if (row == null)
                return row;
            return _getProjectComponent(row);
        }
        public string FPrefix
        {
            get
            {
                return Flattenizer == null
                     ? ""
                     : GridViewDataSourceFlattenizer.LvlPropertyName(0) + ".";
            }
        }

        void ConfigureProjectComponentColumns(GridView view)
        {
            var bview = view as BandedGridView;

            var newColumns = view.PopulateColumnsFromDataAnnotations(typeof(ProjectComponent), _band);

            view.Columns.Remove(view.Columns[nameof(ProjectComponent.TypedId)]);

            foreach (GridColumn col in newColumns)
                col.FieldName = _nestedProperty + col.FieldName;

            var colProject = view.Columns[nameof(ProjectComponent.Project)];
            colProject.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            colProject.InsertAfter(null);
            colProject.FilterMode = ColumnFilterMode.DisplayText;
            GridViewJiraHelper.MakeColumnDisplayProject<ProjectComponent>(colProject, view, () => false);

            var colSelf = view.Columns[nameof(ProjectComponent.Self)];
            colSelf.Caption = "Component Name";
            colSelf.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            colSelf.InsertAfter(colProject);
            colSelf.FilterMode = ColumnFilterMode.DisplayText;
            GridViewJiraHelper.MakeColumnDisplayProjectComponent<ProjectComponent>(colSelf, view);

            view.Columns[nameof(ProjectComponent.Id)].Visible = false;
            view.Columns.Remove(view.Columns[nameof(ProjectComponent.Name)]);
            view.Columns.Remove(view.Columns[nameof(ProjectComponent.ProjectKey)]);
            view.Columns.Remove(view.Columns[nameof(ProjectComponent.ProjectId)]);

            colProject.SortOrder = ColumnSortOrder.Ascending;
            colProject.SortIndex = 0;
            colSelf.SortOrder = ColumnSortOrder.Ascending;
            colSelf.SortIndex = 1;

            view.DataSourceChanged -= View_DataSourceChanged;
            view.DataSourceChanged += View_DataSourceChanged;
            if (view.GridControl is EnhancedGridControl egc)
            {
                egc.DataSourceRefreshed -= Grid_DataSourceChanged;
                egc.DataSourceRefreshed += Grid_DataSourceChanged;
            }
            if (view.IsDetailView && view.DataSource != null)
                View_DataSourceChanged(view, EventArgs.Empty);

            view.RowCellStyle -= GridView_RowCellStyle;
            view.RowCellStyle += GridView_RowCellStyle;
        }


        void Grid_DataSourceChanged(object sender, EventArgs e)
        {
            var view = (GridView)((GridControl)sender).MainView;
            View_DataSourceChanged(view, e);
        }

        void View_DataSourceChanged(object sender, EventArgs e)
        {
            var view = sender is GridView gv ? gv : (GridView)((GridControl)sender).MainView;
            GridViewHelper.SetDefaultColumnFormatting(view);
            _owner.BeginInvoke((Action)(() =>
            {
                var value = _view.BestFitMaxRowCount;
                _view.BestFitMaxRowCount = 10;
                _view.BestFitColumns();
                _view.BestFitMaxRowCount = value;
            }));

            if (!view.IsDetailView)
            {
                view.DataSourceChanged -= View_DataSourceChanged;
                if (view.GridControl is EnhancedGridControl egc)
                    egc.DataSourceRefreshed -= Grid_DataSourceChanged;
            }
        }

        void GridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            var view = sender as GridView;
            if (e.RowHandle == view.FocusedRowHandle)
            {
                if (e.CellValue != null)
                {
                    // Do not change background color of business object. 
                    // Usually some helpers take care of all display in gridview.
                    var name = e.CellValue.GetType().Assembly.FullName;
                    if (name.Contains(nameof(JiraManager)) && name.Contains(nameof(Business)))
                        e.Appearance.BackColor = e.Appearance.BackColor.InterpolateTo(Color.White, 0.5);
                }
            }
        }
    }
}
