﻿using System;

using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.UI.DX;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.UI.Helpers
{
    public class GridView_ConfiguratorProxy : ApplicationBase.UI.Helpers.GridView_ConfiguratorProxy
    {
        public static new GridView_ConfiguratorProxy Instance
        {
            get { return (GridView_ConfiguratorProxy)ApplicationBase.UI.Helpers.GridView_ConfiguratorProxy.Instance; }
            set { ApplicationBase.UI.Helpers.GridView_ConfiguratorProxy.Instance = value; }
        }
        static GridView_ConfiguratorProxy()
        {
            Instance = new GridView_ConfiguratorProxy();
        }


        protected override Action ConfigureAndReturnCancelAction(Type rowTypeToDisplay, GridView view, GridBand band)
        {
            var eview = view as EnhancedGridView;
            var ebview = view as EnhancedBandedGridView;
            view.BeginUpdate();
            ClearColumnAndBands(view, band);

            if (rowTypeToDisplay == typeof(Issue))
            {
                var config = new GridView_ConfiguratorProxy_ForIssue();

                if (eview != null)
                    config.Install(view.GridControl, eview);
                else if (ebview != null)
                    config.Install(view.GridControl, ebview, null, band);
                view.EndUpdate();
                return config.Uninstall;
            }
            else if (rowTypeToDisplay == typeof(ProjectComponent))
            {
                var config = new GridView_ConfiguratorProxy_ForProjectComponent();

                if (eview != null)
                    config.Install(view.GridControl, eview);
                else if (ebview != null)
                    config.Install(view.GridControl, ebview, null, band);
                view.EndUpdate();
                return config.Uninstall;
            }
            view.EndUpdate();
            return null;
        }
    }
}
