﻿using System;
using System.Diagnostics;
using System.Drawing;

using DevExpress.Data;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX.RepositoryItems;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Forms;

using JiraManager.Business.API.DtoObjects;
using JiraManager.Business;


namespace JiraManager.UI.Helpers
{
    public static partial class GridViewJiraHelper
    {
        public static void MakeColumnDisplayProject<TRow>(GridColumn col, GridView gridView, Func<bool> userCanEdit)
        {
            // Default settings for business objects
            gridView.RowCellStyle += (sender, e) =>
            {
                if (e.Column == col && e.RowHandle != GridControl.AutoFilterRowHandle)
                {
                    e.Appearance.Options.UseForeColor = true;
                    e.Appearance.ForeColor = GraphicalChart.StealPurple;
                    e.Appearance.Options.UseBackColor = true;
                    e.Appearance.BackColor = GraphicalChart.WarmOrange;
                }
            };
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            col.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            col.AppearanceCell.Options.UseTextOptions = true;

            gridView.CustomRowCellEdit += (sender, e) =>
            {
                var gv = (GridView)sender;
                if (!gv.IsDataRow(e.RowHandle))
                    return;
                if (e.Column == col)
                {
                    // Optim : Creer un RepositoryItemLookUpEdit au lieu d'un RepositoryItemGridLookUpEdit rend le scroll de la grille bien plus fluide
                    var repo = new RepositoryItemMemoEdit()
                    {
                        NullText = null,
                        AutoHeight = true,
                        WordWrap = true,
                        ReadOnly = true
                    };
                    repo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
                    repo.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
                    repo.Appearance.Options.UseTextOptions = true;
                    repo.ReadOnly = !userCanEdit();
                    e.RepositoryItem = repo;
                }
            };
            gridView.CustomRowCellEditForEditing += (sender, e) =>
            {
                if (!userCanEdit())
                    return;
                var gv = (GridView)sender;
                if (!gv.IsDataRow(e.RowHandle))
                    return;
                if (e.Column == col)
                { 
                    var item = (TRow)gv.GetRow(e.RowHandle);
                    Debug.Assert(item != null);
                    var repo = new RepositoryItemEnhancedGridLookUpEdit();
                    repo.NullText = null;
                    repo.View.OptionsView.RowAutoHeight = true;

                    var colLongName = repo.View.Columns.AddVisible("*Issue Count", "Issue Count");
                    colLongName.UnboundType = UnboundColumnType.String;
                    repo.View.CustomUnboundColumnData += (_, ee) =>
                    {
                        if (ee.IsGetData && ee.Column.FieldName == colLongName.FieldName)
                            ee.Value = ((Project)ee.Row).GetIssues(_dummyProgress).Count;
                    };
                    repo.View.Columns.AddVisible(nameof(Project.Name));
                    repo.View.Columns.AddVisible(nameof(Project.Id), "Id");
                    repo.DisplayMember = nameof(Project.Key);
                    //repo.PopupFilterMode = PopupFilterMode.Contains;
                    repo.View.OptionsView.ShowAutoFilterRow = true;
                    repo.View.OptionsFind.AlwaysVisible = true; // Ne fonctionne pas pour les RepositoryItemGridLookUpEdit d'apres devexpress
                                                                //repo.View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
                                                                // repo.View.OptionsBehavior.AllowIncrementalSearch = true; // inutile ?!
                    repo.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
                    foreach (GridColumn c in repo.View.Columns)
                        c.AppearanceHeader.Font = new Font(c.AppearanceHeader.Font, FontStyle.Bold);
                    //repo.ValueMember = nameof(User.Self);
                    //Debug.Assert(gv.FocusedRowHandle == e.RowHandle && gv.FocusedColumn == e.Column); // Il est deja arrivé que ca foire, incapable de reproduire..
                    repo.DataSource = BusinessSingletons.Instance.GetRepository().Projects;
                    repo.EditValueChanged += (_, __) =>
                    {
                        ExceptionManager.Instance.IgnoreExceptionAndBreak(() => gv.PostEditor());
                    };
                    e.RepositoryItem = repo;
                }
            };
            col.OptionsFilter.FilterPopupMode = FilterPopupMode.Excel;
            ((GridView)col.View).PopupMenuShowing += AddFeatureForColumnDisplayingProject_PopupMenuShowing;
        }
        static readonly IProgress<string> _dummyProgress = new Progress<string>();

        static void AddFeatureForColumnDisplayingProject_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null || !e.HitInfo.InDataRow || e.HitInfo.Column == null)
                return;
            var view = (GridView)sender;
            var cellValue = view.GetRowCellValue(e.HitInfo.RowHandle, e.HitInfo.Column);
            var project = cellValue as Project;
            if (project == null)
                return;

            var mnu = new DXMenuItem();
            mnu.Click += (_, __) => view.GridControl.ShowBusyWhileDoing("Opening link for " + project.Name + "(" + project.Key + ")", pr => Process.Start(project.IntranetUrl));
            mnu.Enabled = !string.IsNullOrWhiteSpace(project.IntranetUrl);
            mnu.Caption = "Open Atlassian Link";
            e.Menu.AddToGroup(ePopupMenuHeaderType.Connectivity, mnu);
        }

        public static void MakeColumnDisplayProjectComponent<TRow>(GridColumn col, GridView gridView)
        {
            // Default settings for business objects
            gridView.RowCellStyle += (sender, e) =>
            {
                if (e.Column == col && e.RowHandle != GridControl.AutoFilterRowHandle)
                {
                    e.Appearance.Options.UseForeColor = true;
                    e.Appearance.ForeColor = GraphicalChart.StealPurple;
                    e.Appearance.Options.UseBackColor = true;
                    e.Appearance.BackColor = GraphicalChart.WarmOrange;
                }
            };
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            col.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            col.AppearanceCell.Options.UseTextOptions = true;

            col.OptionsFilter.FilterPopupMode = FilterPopupMode.Excel;
            ((GridView)col.View).PopupMenuShowing += AddFeatureForColumnDisplayingProjectComponent_PopupMenuShowing;
        }
        static void AddFeatureForColumnDisplayingProjectComponent_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null || !e.HitInfo.InDataRow || e.HitInfo.Column == null)
                return;
            var view = (GridView)sender;
            var cellValue = view.GetRowCellValue(e.HitInfo.RowHandle, e.HitInfo.Column);
            var component = cellValue as ProjectComponent;
            if (component == null)
                return;

            var mnu = new DXMenuItem();
            mnu.Click += (_, __) => view.GridControl.ShowBusyWhileDoing("Opening issues of this components in Atlassian", pr => Process.Start(component.InternetUrl));
            mnu.Enabled = !string.IsNullOrWhiteSpace(component.InternetUrl);
            mnu.Caption = "Open Atlassian Link";
            e.Menu.AddToGroup(ePopupMenuHeaderType.Connectivity, mnu);
        }


        public static void MakeColumnDisplayIssue<TRow>(GridColumn col, GridView gridView)
        {
            // Default settings for business objects
            gridView.RowCellStyle += (sender, e) =>
            {
                if (e.Column == col && e.RowHandle != GridControl.AutoFilterRowHandle)
                {
                    e.Appearance.Options.UseForeColor = true;
                    e.Appearance.ForeColor = GraphicalChart.StealPurple;
                    e.Appearance.Options.UseBackColor = true;
                    e.Appearance.BackColor = GraphicalChart.WarmOrange;
                }
            };
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            col.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            col.AppearanceCell.Options.UseTextOptions = true;

            col.OptionsFilter.FilterPopupMode = FilterPopupMode.Excel;
            ((GridView)col.View).PopupMenuShowing += AddFeatureForColumnDisplayingIssue_PopupMenuShowing;
        }

        static void AddFeatureForColumnDisplayingIssue_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null || !e.HitInfo.InDataRow || e.HitInfo.Column == null)
                return;
            var view = (GridView)sender;
            var cellValue = view.GetRowCellValue(e.HitInfo.RowHandle, e.HitInfo.Column);
            var issue = cellValue as Issue;
            if (issue == null)
                return;

            var mnu = new DXMenuItem();
            mnu.Click += (_, __) => view.GridControl.ShowBusyWhileDoing("Opening link for issue " + issue.Key, pr => Process.Start(issue.InternetUrl));
            mnu.Enabled = !string.IsNullOrWhiteSpace(issue.InternetUrl);
            mnu.Caption = "Open Atlassian Link";
            e.Menu.AddToGroup(ePopupMenuHeaderType.Connectivity, mnu);

            mnu = new DXMenuItem();
            mnu.Click += (_, __) =>
            {
                view.GridControl.ShowBusyWhileDoing("Converting to JSON...",
                pr =>
                {
                    return issue.Storage.ToString(Newtonsoft.Json.Formatting.Indented);
                }).ContinueWithUIWorkOnSuccess(jsonIndented =>
                {
                    var mf = new MemoForm(jsonIndented, "Issue " + issue.Key + " - JSON");
                    mf.Memo.ReadOnly = true;
                    mf.ShowIcon = false;
                    mf.ShowInTaskbar = false;
                    mf.Show(view.GridControl.FindForm());
                });
            };
            mnu.Enabled = !string.IsNullOrWhiteSpace(issue.InternetUrl);
            mnu.Caption = "See Json";
            e.Menu.AddToGroup(ePopupMenuHeaderType.Connectivity, mnu);
        }

    }
}
