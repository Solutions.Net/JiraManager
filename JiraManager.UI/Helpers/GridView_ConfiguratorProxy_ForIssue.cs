﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

using TechnicalTools;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Helpers;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.UI.Helpers
{
    public class GridView_ConfiguratorProxy_ForIssue
    {
        Control _owner;
        GridView _view;
        GridBand _band;
        string _nestedProperty;
        // Get an issue when gridview row is not directly of type issue
        // This is useful for composition when in grid wewant to display a type "A", which is composed of a property of type Issue 
        // and we want to display in the same grid  the object Issue (relation 1-0 or 1-1 between A and Issue)
        Func<object, Issue> _getIssue = x => (Issue)x;

        public GridViewDataSourceFlattenizer Flattenizer { get; private set; }

        // Generate column on first band
        public void Install(Control owner, EnhancedGridView view, Func<object, Issue> getIssue = null)
        {
            _getIssue = getIssue ?? _getIssue;
            _Install(owner, view);
        }
        public void Install(Control owner, EnhancedBandedGridView view, string nestedProperty, GridBand band, Func<object, Issue> getIssue = null)
        {
            _nestedProperty = nestedProperty.IfNotBlankAddSuffix(".") ?? "";
            _band = band;
            _getIssue = getIssue ?? _getIssue;
            _Install(owner, view);
        }
        void _Install(Control owner, GridView view)
        {
            if (_view != null)
                Uninstall();
            _owner = owner;
            _view = view;
            _view.BeginUpdate();

            // we use Editable = false instead of "readonly = true" because user wants to :
            // select part of text in a cell and copy it 
            // with readonly we have to copy all cell or nothing which is annoying for user when he has to copy ticket number etc
            _view.OptionsBehavior.Editable = false;
            _view.OptionsView.ColumnAutoWidth = false;
            _view.OptionsView.ShowAutoFilterRow = true;
            _view.OptionsView.RowAutoHeight = true;
            _view.OptionsDetail.AllowExpandEmptyDetails = true; // Evite que MasterRowGetChildList soit appelé deux fois
            _view.LevelIndent = -1;

            ConfigureIssueColumns(_view);

            var parentView = _view.ParentView as GridView;
            if (parentView != null) // It means that _view is a detailed View
                InstallAsSubGridView(parentView);

            _view.EndUpdate();
        }

        void InstallAsSubGridView(GridView parentView)
        {
            _view.OptionsView.ShowIndicator = false;
            var t = parentView.DataSource?.GetType();
            var itemType = t.GetInterfaces()
                            .Select(i => i.GetOneImplementationOf(typeof(IEnumerable<>)))
                            .NotNull()
                            .FirstOrDefault()
                            ?.GenericTypeArguments[0];
            var parentViewDisplayIssueToo = itemType.Implements(typeof(Issue));
            if (!parentViewDisplayIssueToo)
            {
                var enumerator = (parentView.DataSource as System.Collections.IEnumerable)?.GetEnumerator();
                parentViewDisplayIssueToo = enumerator.MoveNext()
                                                && enumerator.Current is Issue;
            }
            _view.OptionsView.ShowGroupPanel = !parentViewDisplayIssueToo;

            if (!parentViewDisplayIssueToo)
                return;
            
            foreach (GridColumn pcol in parentView.Columns)
            {
                var col = _view.Columns[pcol.FieldName];
                if (col != null)
                    col.Width = pcol.Width;
            }
            if (parentView.Columns[nameof(Project)].Visible) // only for level 1 child view, for level 2, 3, etc view will be the current gvSubView
                _view.Columns[nameof(Issue.Id)].Width += _view.Columns[nameof(Project)].Width + _view.Columns[nameof(ProjectComponent)].Width;
            _view.Columns[nameof(Project)].Visible = false;
            _view.Columns[nameof(ProjectComponent)].Visible = false;
            _view.Columns[nameof(Issue.Id)].Width += -((GridViewInfo)parentView.GetViewInfo()).CalcLevelIndent();

            // TODO : https://www.devexpress.com/Support/Center/Example/Details/e2454/how-to-keep-master-and-detail-view-column-widths-synchronized
            // TODO : https://www.devexpress.com/Support/Center/Question/Details/T272620/is-there-something-like-synchronizeclones-or-synchronizescrolling-that-synchronize
        }

        public void Uninstall()
        {
            _view.RowCellStyle -= GridView_RowCellStyle;
            _view.DataSourceChanged -= View_DataSourceChanged;
            if (_view.GridControl is EnhancedGridControl egc)
                egc.DataSourceRefreshed -= Grid_DataSourceChanged;
            _view = null;
        }

        public object GetRow(int rowHandle)
        {
            if (rowHandle < 0)
                return null;
            var obj = Flattenizer != null
                    ? Flattenizer.GetRowBusinessObject(rowHandle)
                    : _view.GetRow(rowHandle);
            return _getIssue(obj);
        }
        public object GetRow(object row)
        {
            if (row == null)
                return row;
            return _getIssue(row);
        }
        public string FPrefix
        {
            get
            {
                return Flattenizer == null
                     ? ""
                     : GridViewDataSourceFlattenizer.LvlPropertyName(0) + ".";
            }
        }

        void ConfigureIssueColumns(GridView view)
        {
            var bview = view as BandedGridView;

            var newColumns = view.PopulateColumnsFromDataAnnotations(typeof(Issue), _band);

            view.Columns.Remove(view.Columns[nameof(Issue.TypedId)]);

            foreach (GridColumn col in newColumns)
                col.FieldName = _nestedProperty + col.FieldName;

            var colProject = view.Columns[nameof(Issue.Project)];
            colProject.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            colProject.InsertAfter(null);
            colProject.FilterMode = ColumnFilterMode.DisplayText;
            GridViewJiraHelper.MakeColumnDisplayProject<Issue>(colProject, view, () => false);

            var colComponent = view.Columns[nameof(Issue.Component)];
            colComponent.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            colComponent.InsertAfter(colProject);
            colComponent.FilterMode = ColumnFilterMode.DisplayText;
            GridViewJiraHelper.MakeColumnDisplayProjectComponent<Issue>(colComponent, view);

            var colParentTask = view.Columns[nameof(Issue.ParentTask)];
            colParentTask.FilterMode = ColumnFilterMode.DisplayText;
            colParentTask.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            colParentTask.InsertAfter(colComponent);

            var colSelf = view.Columns[nameof(Issue.Self)];
            colSelf.Caption = "Key";
            colSelf.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            colSelf.InsertAfter(colParentTask);
            colSelf.FilterMode = ColumnFilterMode.DisplayText;
            GridViewJiraHelper.MakeColumnDisplayIssue<Issue>(colSelf, view);

            view.Columns[nameof(Issue.Id)].Visible = false;
            view.Columns[nameof(Issue.IssueType)].FilterMode = ColumnFilterMode.DisplayText;
            view.Columns[nameof(Issue.IssueType)].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            view.Columns[nameof(Issue.Status)].FilterMode = ColumnFilterMode.DisplayText;
            view.Columns[nameof(Issue.Status)].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            view.Columns[nameof(Issue.StatusCategory)].FilterMode = ColumnFilterMode.DisplayText;
            view.Columns[nameof(Issue.StatusCategory)].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            view.Columns[nameof(Issue.Priority)].FilterMode = ColumnFilterMode.DisplayText;
            view.Columns[nameof(Issue.Priority)].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;

            view.Columns[nameof(Issue.Creator)].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            view.Columns[nameof(Issue.Creator)].FilterMode = ColumnFilterMode.DisplayText;
            view.Columns[nameof(Issue.Reporter)].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            view.Columns[nameof(Issue.Reporter)].FilterMode = ColumnFilterMode.DisplayText;
            view.Columns[nameof(Issue.Assignee)].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            view.Columns[nameof(Issue.Assignee)].FilterMode = ColumnFilterMode.DisplayText;

            view.Columns[nameof(Issue.Description)].VisibleIndex = 999;

            var repoMultiLineCentered = view.CreateMultilineRepositoryEdit();
            if (view is IEnhancedGridView ieview)
                ieview.ResizeRow.MaxRowHeight = 150;
            repoMultiLineCentered.Appearance.Options.UseTextOptions = true;
            repoMultiLineCentered.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            repoMultiLineCentered.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            var colDescription = view.Columns[nameof(Issue.Description)];
            view.CustomRowCellEdit += (_, e) =>
            {
                if (e.Column == colDescription && e.RowHandle != GridControl.AutoFilterRowHandle)
                    e.RepositoryItem = repoMultiLineCentered;
            };

            colProject.SortOrder = ColumnSortOrder.Ascending;
            colProject.SortIndex = 0;

            view.Columns[_nestedProperty + nameof(Issue.Updated)].SortOrder = ColumnSortOrder.Descending;
            view.Columns[_nestedProperty + nameof(Issue.Created)].SortOrder = ColumnSortOrder.Descending;
            view.Columns[_nestedProperty + nameof(Issue.Updated)].SortIndex = 1;
            view.Columns[_nestedProperty + nameof(Issue.Created)].SortIndex = 2;

            view.DataSourceChanged -= View_DataSourceChanged;
            view.DataSourceChanged += View_DataSourceChanged;
            if (view.GridControl is EnhancedGridControl egc)
            {
                egc.DataSourceRefreshed -= Grid_DataSourceChanged;
                egc.DataSourceRefreshed += Grid_DataSourceChanged;
            }
            if (view.IsDetailView && view.DataSource != null)
                View_DataSourceChanged(view, EventArgs.Empty);



            view.RowCellStyle -= GridView_RowCellStyle;
            view.RowCellStyle += GridView_RowCellStyle;

            if (view is IEnhancedGridView egv)
            {
                egv.RowItemDoubleClick -= gv_RowItemDoubleClick;
                egv.RowItemDoubleClick += gv_RowItemDoubleClick;
            }
        }


        void gv_RowItemDoubleClick(object sender, RowItemDoubleClickEventArgs e)
        {
            var view = (GridView)sender;
            var gv = (IEnhancedGridView)view;
            if (e.Column.FieldName != nameof(Issue.Description))
                return;
            if (e.RowHandle >= 0)
            {
                var issue = (Issue)GetRow(e.RowHandle);
                var parentForm = gv.GridControl.FindForm();
                var memoForm = new TechnicalTools.UI.DX.Forms.MemoForm(issue.Description)
                {
                    Text = "Issue's description",
                    Size = new Size(parentForm.Width * 80 / 100, parentForm.Height * 80 / 100)
                };
                memoForm.Show(parentForm);
            }
        }
        void Grid_DataSourceChanged(object sender, EventArgs e)
        {
            var view = (GridView)((GridControl)sender).MainView;
            View_DataSourceChanged(view, e);
        }

        void View_DataSourceChanged(object sender, EventArgs e)
        {
            var view = sender is GridView gv ? gv : (GridView)((GridControl)sender).MainView;
            GridViewHelper.SetDefaultColumnFormatting(view);
            _owner.BeginInvoke((Action)(() =>
            {
                var value = _view.BestFitMaxRowCount;
                _view.BestFitMaxRowCount = 10;
                _view.BestFitColumns();
                _view.BestFitMaxRowCount = value;
            }));

            if (!view.IsDetailView)
            {
                view.DataSourceChanged -= View_DataSourceChanged;
                if (view.GridControl is EnhancedGridControl egc)
                    egc.DataSourceRefreshed -= Grid_DataSourceChanged;
            }
        }

        void GridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            var view = sender as GridView;
            if (e.RowHandle == view.FocusedRowHandle)
            {
                if (e.CellValue != null)
                {
                    // Do not change background color of business object. 
                    // Usually some helpers take care of all display in gridview.
                    var name = e.CellValue.GetType().Assembly.FullName;
                    if (name.Contains(nameof(JiraManager)) && name.Contains(nameof(Business)))
                        e.Appearance.BackColor = e.Appearance.BackColor.InterpolateTo(Color.White, 0.5);
                }
            }
        }
    }
}
