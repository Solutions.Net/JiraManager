﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business.API
{
    // TODO ? https://stackoverrun.com/fr/q/9621575
    // http://james.newtonking.com/archive/2015/12/20/json-net-8-0-release-1-allocations-and-bug-fixes
    /// <summary>
    /// Helper that deserialize and respect the filling of <see cref="DtoObject.OriginalJSON"/>.
    /// </summary>
    public class JsonUtils
    {
        public static readonly JsonUtils Instance = new JsonUtils(false, BusinessValues.PriorityWordsForNormalization);
        protected internal JsonUtils(bool useStrictDeserialization = false, IReadOnlyDictionary<string, int> priorityWordsForNormalization = null)
        {
            _useStrictDeserialization = useStrictDeserialization;
            _priorityWordsForNormalization = priorityWordsForNormalization ?? new Dictionary<string, int>();
            _defaultPriority = _priorityWordsForNormalization.TryGetValueStruct(string.Empty) 
                            ?? _priorityWordsForNormalization.Values.DefaultIfEmpty(0).Max() + 1;
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(JsonUtils));
        readonly bool _useStrictDeserialization;
        // indicate word priority, when word is not present, the return value will be 100
        readonly IReadOnlyDictionary<string, int> _priorityWordsForNormalization;
        readonly int _defaultPriority;


        public Dictionary<string, JArray> FindArrayPropertiesFromJSON(string json)
        {
            var token = (JObject)JToken.Parse(json);
            var res = token.Properties().Where(p => p.Value is JArray).ToDictionary(p => p.Name, p => (JArray)p.Value);
            return res;
        }

        public string SerializeToJSON(object obj, bool indented = true)
        {
            return JsonConvert.SerializeObject(obj,
                                               indented ? Formatting.Indented : Formatting.None,
                                               new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

        /// <summary>
        ///  A safe way to deserialize json to a container (often a page result) 
        ///  where one property contains a list of item.
        ///  This method is useful to assign <see cref="DtoObject.OriginalJSON"/> for each items, 
        ///  and the cut json for container object
        /// </summary>
        internal Page<TItem> DeserializePageWithItemArray<TItem>(string json, string collectionProp)
            where TItem : IHasJsonStorage, new()
        {
            var page = new Page<TItem>();
            var storage = JToken.Parse(json);
            if (storage is JArray array) // For api v2
            {
                var wrapper = new JObject();
                wrapper.Add(collectionProp, storage);
                wrapper.Add("maxResults", int.MaxValue.ToStringInvariant());
                wrapper.Add("startAt", 0);
                wrapper.Add("total", array.Count);
                wrapper.Add("isLast", "true");
                page.Storage = wrapper;
            }
            else
            {
                page.Storage = JObject.Parse(json);
            }
            var jArray = (JArray)page.Storage[collectionProp];
            page.Items = DeserializeObjectArray<TItem>(jArray);
            return page;
        }

        internal List<TItem> DeserializeObjectArray<TItem>(string json, string itemCollectionProp = null)
            where TItem : IHasJsonStorage
        {
            var jArray = itemCollectionProp == null
                       ? JArray.Parse(json)
                       : (JArray)JObject.Parse(json)[itemCollectionProp];
            return DeserializeObjectArray<TItem>(jArray);
        }
        internal List<TItem> DeserializeObjectArray<TItem>(JArray jArray)
            where TItem : IHasJsonStorage
        {
            var itemType = typeof(TItem);
            var items = (List<TItem>)new object[] { }.ToTypedList(itemType);
            var constructor = DefaultObjectFactory.ConstructorFor(itemType);
            foreach (var jItem in jArray)
            {
                var item = (TItem)constructor();
                item.Storage = (JObject)jItem;
                items.Add(item);
            }

            return items;
        }


        /// <summary>
        ///  A safe way to deserialize json to object and log ALL warnings
        /// </summary>
        internal T DeserializeFromJSON<T>(string json)
            where T : IHasJsonStorage, new()
        {
            var item = new T();
            item.Storage = JObject.Parse(json);
            return item;
        }
        /// <summary>
        ///  A safe way to deserialize json to object and log ALL warnings
        /// </summary>
        internal object DeserializeFromJSON(Type type, string json)
        {
            var item = DefaultObjectFactory.ConstructorFor(type)();
            if (item is DtoObject dto)
                dto.Storage = JObject.Parse(json);
            return item;
        }

        // Equivalent of Newtonsoft SelectToken but with case insensitivity handling 
        internal JToken SelectToken(JToken storage, bool createPath, params object[] keys)
        {
            Debug.Assert(storage != null);
            if (keys.Length == 0)
                throw new ArgumentException("key path cannot be empty!", nameof(keys));
            JToken tok = storage;
            JToken newTok = null;
            int k = -1;
            while (++k < keys.Length)
            {
                if (tok is JObject obj)
                    newTok = obj.GetValue((string)keys[k], StringComparison.OrdinalIgnoreCase);
                else if (tok is JArray a)
                    if (keys[k] is int i)
                        newTok = i < a.Count
                               ? a[i]
                               : null;
                    else
                        throw new TechnicalException("It seems underlying data has changed, expecting an array at this point!", null);
                if (newTok == null)
                    if (!createPath)
                        return null;
                    else if (k + 1 == keys.Length) // if not more keys, we assume the next keys is a string
                        tok[keys[k]] = newTok = new JValue("");
                    else if (keys[k + 1] is string)
                        if (tok is JArray a)
                        {
                            newTok = new JObject();
                            var index = (int)keys[k];
                            while (a.Count < index)
                                a.Add(Null);
                            a.Add(newTok);
                        }
                        else
                            tok[keys[k]] = newTok = new JObject();
                    else
                        tok[keys[k]] = newTok = new JArray();
                tok = newTok;
            }
            return tok;
        }
        static readonly JValue Null = JValue.CreateNull();



        // Inspired from https://stackoverflow.com/a/40679286/294998
        internal JToken NormalizeToken(JToken token)
        {
            if (token is JObject o)
            {
                List<JProperty> orderedProperties = new List<JProperty>(o.Properties());
                orderedProperties.Sort(LexicalOrderWithPrioritizedWords);
                JObject normalized = new JObject();
                foreach (JProperty property in orderedProperties)
                    normalized.Add(property.Name, NormalizeToken(property.Value));
                return normalized;
            }
            if (token is JArray array)
            {
                for (int i = 0; i < array.Count; i++)
                    array[i] = NormalizeToken(array[i]);
                return array;
            }
            return token;
        }
        int LexicalOrderWithPrioritizedWords(JProperty x, JProperty y)
        {
            int xPriority = _priorityWordsForNormalization.TryGetValueStruct(x.Name) ?? _defaultPriority;
            int yPriority = _priorityWordsForNormalization.TryGetValueStruct(y.Name) ?? _defaultPriority;
            if (xPriority < yPriority)
                return -1;
            if (xPriority > yPriority)
                return 1;
            return string.Compare(x.Name, y.Name);
        }



        internal void CopyUninterestingDataTo(JObject from, JObject target)
        {
            Debug.Assert(GetType() == target.GetType());
            var keyPaths = BusinessValues.UninterestingPaths.TryGetValueClass(GetType()); // expected to work otherwise we should not be here 
            if (keyPaths == null)
                return;
            foreach (var keyPath in keyPaths)
            {
                var value = SelectToken(from, true, keyPath);
                var toAssign = SelectToken(target, false, keyPath.Take(keyPath.Length - 1).ToArray());
                var toBeAssigned = SelectToken(target, false, keyPath.Take(keyPath.Length - 1).ToArray());

                if (toBeAssigned == null)
                    if (toAssign == null)
                        continue;
                    else
                    {
                        toBeAssigned = SelectToken(target, true, keyPath.Take(keyPath.Length - 1).ToArray());
                        toBeAssigned.Replace(toAssign.DeepClone());
                    }
                else
                    if (toAssign == null)
                    toBeAssigned.Remove();
                else
                    toBeAssigned.Replace(toAssign.DeepClone());
            }
        }
    }
}
