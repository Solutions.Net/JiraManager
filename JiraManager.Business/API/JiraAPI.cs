﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

using RestSharp;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Model;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business.API
{
    public class JiraAPI : IDisposable
    {
        public JiraAPI(Config cfg)
        {
            _cfg = cfg;
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(JiraAPI));
        readonly Config _cfg;

        public int UId { get; } = System.Threading.Interlocked.Increment(ref UIdSeed);
        static int UIdSeed = 0;

        internal ServerInfo GetServerInfo()
        {
            return Get<ServerInfo>("/serverInfo");
        }


        public User GetUser(string accountIdOrKey)
        {
            var user = Get<User>("/user?" + User.UniqueIdJsonKey(_cfg.ApiVersion) + "=" + accountIdOrKey + "&expand=groups,applicationRoles");
            return user;
        }
        public List<User> GetUsers()
        {
            return GetArray<User>($"/user/bulk").ToList();
        }

        public Project GetProject(long id)
        {
            return null;
        }
        public Project GetProject(string id)
        {
            return null;
        }
        public List<Project> GetProjects()
        {
            var ps = GetPage<Project>("/project", "values");
            var projects = ps.Items.ToList();
            while (!(ps.isLast ?? false) && ps.Items.Count == ps.maxResults)
            {
                ps = GetPage<Project>(ps.nextPage, "values");
                projects.AddRange(ps.Items);
            }
            return projects;
        }



        internal ProjectComponent GetComponent(long id)
        {
            return Get<ProjectComponent>($"/component/{id}");
        }
        internal List<ProjectComponent> GetComponents(string projectIdOrKey = null)
        {
            if (!string.IsNullOrWhiteSpace(projectIdOrKey))
                return GetArray<ProjectComponent>($"/project/{projectIdOrKey}/components").ToList();
            var projects = GetProjects();
            return projects.SelectMany(p => GetComponents(p.Key))
                           .ToList();
        }

        public Issue GetIssue(long id)
        {
            var item = Get<Issue>("/issue/" + id.ToStringInvariant());
            return item;
        }
        public Issue GetIssue(string key)
        {
            var item = Get<Issue>("/issue/" + System.Web.HttpUtility.UrlEncode(key));
            return item;
        }
        internal List<Issue> GetIssues(string jqlFilter = null, bool sortById = false, IProgress<string> pr = null)
        {
            var lst = GetList<Issue>("/search", "issues", jqlFilter, null /*it seems buggy on atlassian side when we pass id here :/*/, pr);
            return lst;
        }

        public IssueType GetIssueType(long id)
        {
            var issueType = Get<IssueType>("/issuetype/" + id);
            return issueType;
        }
        internal List<IssueType> GetIssueTypes()
        {
            var lst = GetArray<IssueType>("/issuetype");
            return lst;
        }

        public Status GetStatus(long id)
        {
            var status = Get<Status>("/status/" + id);
            return status;
        }
        internal List<Status> GetStatuses()
        {
            var lst = GetArray<Status>("/status");
            return lst;
        }

        public StatusCategory GetStatusCategory(long id)
        {
            var status = Get<StatusCategory>("/statuscategory/" + id);
            return status;
        }
        internal List<StatusCategory> GetStatusCategories()
        {
            var lst = GetArray<StatusCategory>("/statuscategory");
            return lst;
        }

        public Resolution GetResolution(long id)
        {
            var status = Get<Resolution>("/resolution/" + id);
            return status;
        }
        internal List<Resolution> GetResolutions()
        {
            var lst = GetArray<Resolution>("/resolution");
            return lst;
        }

        public Priority GetPriority(long id)
        {
            var status = Get<Priority>("/priority/" + id);
            return status;
        }
        internal List<Priority> GetPriorities()
        {
            var lst = GetArray<Priority>("/priority");
            return lst;
        }

        public IssueLinkType GetIssueLink(long id)
        {
            var status = Get<IssueLinkType>("/issueLink/" + id);
            return status;
        }
        // Seems to not exist
        //internal List<IssueLinkType> GetIssueLinks()
        //{
        //    var lst = GetArray<IssueLinkType>("/issueLink");
        //    return lst;
        //}

        public IssueLinkType GetIssueLinkType(long id)
        {
            var status = Get<IssueLinkType>("/issueLinkType/" + id);
            return status;
        }
        internal List<IssueLinkType> GetIssueLinkTypes()
        {
            var lst = GetArray<IssueLinkType>("/issueLinkType", "issueLinkTypes");
            return lst;
        }
        class IssueLinkTypes
        {

        }


        #region Technical part

        // Abstract the multiple calls to get all pages
        // If idFields is filled, it must contains ids name that forms the IdTuple of TITem (in the same order)
        internal List<TItem> GetList<TItem>(string baseurl, string itemCollectionProp, string jqlFilter = null, string[] idFields = null, IProgress<string> pr = null, [CallerMemberName] string method = null)
             where TItem : JiraObject, new()
        {
            pr = pr ?? ProgressEx<string>.Default;
            var startAt = 0;
            
            var orderBy = idFields == null ? string.Empty : " order by " + idFields.Select(idField => idField + " asc").Join();
            var jql = jqlFilter + orderBy;
            int resultCountFetchedByRequest = 500;
            string getUrl()
            {
                return baseurl
                     + "?maxResults=" + resultCountFetchedByRequest .ToStringInvariant() + "&startAt="
                     + startAt.ToStringInvariant()
                     + "&jql=".AsPrefixForIfNotBlank(System.Web.HttpUtility.UrlEncode(jql));
            }

            var issues = new List<TItem>();
            Page<TItem> GetPageAndRetryIfNeeded()
            {
                while (true)
                    try
                    {
                        pr.Report($"Retrieving issues...  [{startAt}, {startAt + resultCountFetchedByRequest - 1}] / {(issues.Count == 0 ? "???" : issues.Capacity.ToString())}");
                        return GetPage<TItem>(getUrl(), itemCollectionProp, method);
                    }
                    catch (System.Net.WebException ex) when (ex.Status == System.Net.WebExceptionStatus.Timeout && resultCountFetchedByRequest > 1)
                    {
                        resultCountFetchedByRequest /= 2;
                    }
            }
            Page<TItem> ps = GetPageAndRetryIfNeeded();
            issues.Capacity = ps.total;
            issues.AddRange(ps.Items);
            // sometimes isLast is not in json result so it is false ! 
            while (!(ps.isLast ?? false) && ps.Items.Count == ps.maxResults)
            {
                if (orderBy.Length == 0)
                    startAt += ps.maxResults;

                if (idFields != null)
                {
                    var idTuple = ps.Items.Last().IdTuple;
                    string pkFilter = "";
                    var keys = idTuple.Keys;
                    for (int i = 0; i < keys.Count; ++i)
                    {
                        var idField = idFields[i];
                        var valueFormatted = keys[i] is string str ? "\"" + str + "\"" : keys[i].ToStringInvariant();
                        pkFilter += "(" + idField + " > " + valueFormatted;
                        if (i < keys.Count - 1)
                            pkFilter += " OR " + idField + " = " + valueFormatted + " AND ";
                        pkFilter += ")";
                    }
                    jql = "(".AsPrefixForIfNotBlank(jqlFilter).IfNotBlankAddSuffix(") and ")
                        + pkFilter;
                }
                ps = GetPageAndRetryIfNeeded();
                issues.AddRange(ps.Items);
            }
            pr.Report($"Retrieving issues...   {issues.Count} / {issues.Count}");
            return issues;
        }

        Page<TItem> GetPage<TItem>(string url, string itemCollectionProp, [CallerMemberName] string method = null)
            where TItem : JiraObject, new()
        {
            if (!url.StartsWith(_cfg.ApiUrl) && !url.StartsWith("http"))
                url = _cfg.ApiUrl + (url.StartsWith("/") ? "" : "/") + url;
            var client = new RestClient(url);
            client.Timeout = _cfg.Timeout ?? client.Timeout;
            var request = new RestRequest(Method.GET);
            request.AddHeader(ContentType, ApplicationJson);
            request.AddHeader("Authorization", "Basic " + GetEncodedCredentials(_cfg.EmailAccount, _cfg.ApiKey));
            // Valeurs sniffées dans chrome 
            if (_cfg.CompanyCookies.Count > 0)
                foreach (var cookie in _cfg.CompanyCookies)
                    request.AddCookie(cookie.Key, cookie.Value);
            return EnrichWithRequestOnException(method, "", () =>
            {
                IRestResponse response = CheckResponse(client.Execute(request), request);
                var apiResponse = JsonUtils.Instance.DeserializePageWithItemArray<TItem>(response.Content, itemCollectionProp);
                return apiResponse;
            });
        }
        List<TItem> GetArray<TItem>(string url, string itemCollectionProp = null, [CallerMemberName] string method = null)
            where TItem : JiraObject
        {
            if (!url.StartsWith(_cfg.ApiUrl) && !url.StartsWith("http"))
                url = _cfg.ApiUrl + (url.StartsWith("/") ? "" : "/") + url;
            var client = new RestClient(url);
            client.Timeout = _cfg.Timeout ?? client.Timeout;
            var request = new RestRequest(Method.GET);
            request.AddHeader(ContentType, ApplicationJson);
            request.AddHeader("Authorization", "Basic " + GetEncodedCredentials(_cfg.EmailAccount, _cfg.ApiKey));
            return EnrichWithRequestOnException(method, "", () =>
            {
                IRestResponse response = CheckResponse(client.Execute(request), request);
                var apiResponse = JsonUtils.Instance.DeserializeObjectArray<TItem>(response.Content, itemCollectionProp);
                return apiResponse;
            });
        }

        T Get<T>(string url, [CallerMemberName] string method = null)
            where T : JiraObject, new()
        {
            if (!url.StartsWith(_cfg.ApiUrl) && !url.StartsWith("http"))
                url = _cfg.ApiUrl + (url.StartsWith("/") ? "" : "/") + url;
            var proxy = System.Net.WebRequest.DefaultWebProxy;
            proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            var client = new RestClient(url);
            client.Proxy = proxy;
            client.Timeout = _cfg.Timeout ?? client.Timeout;
            var request = new RestRequest(Method.GET);
            request.AddHeader(ContentType, ApplicationJson);
            request.AddHeader("Authorization", "Basic " + GetEncodedCredentials(_cfg.EmailAccount, _cfg.ApiKey));
            return EnrichWithRequestOnException(method, "", () =>
            {
                IRestResponse response = CheckResponse(client.Execute(request), request);
                var apiResponse = JsonUtils.Instance.DeserializeFromJSON<T>(response.Content);
                return apiResponse;
            });
        }


        static string GetEncodedCredentials(string UserName, string Password)
        {
            string mergedCredentials = String.Format("{0}:{1}", UserName, Password);
            byte[] byteCredentials = Encoding.UTF8.GetBytes(mergedCredentials);
            return Convert.ToBase64String(byteCredentials);
        }


        const string ApplicationJson = "application/json";
        const string ContentType = "Content-Type";
        IRestResponse CheckResponse(IRestResponse response, IRestRequest request, [CallerMemberName] string methodName = "")
        {
            var ex = response.ErrorException;
            if (ex == null && !response.IsSuccessful)
                ex = new TechnicalException($"Error while doing {methodName} with a call to JIRA API." + Environment.NewLine +
                                            $"HTTP Code error is {(int)response.StatusCode} - {response.StatusCode}.", null);
            if (ex != null)
            {
                ex.EnrichDiagnosticWith("HResult code = " + ex.HResult, eExceptionEnrichmentType.Technical);
                var requestContent = request.Parameters.LastOrDefault(p => p.Name == ApplicationJson)?.Value;
                if (requestContent != null)
                    ex.EnrichDiagnosticWith("Request's Content:" + Environment.NewLine + requestContent, eExceptionEnrichmentType.Technical);
                if (string.IsNullOrWhiteSpace(response.Content))
                    ex.EnrichDiagnosticWith("Response's Content is empty", eExceptionEnrichmentType.Technical);
                else
                    ex.EnrichDiagnosticWith("Response's Content:" + Environment.NewLine + response.Content, eExceptionEnrichmentType.Technical);
                if (ex is TechnicalException)
                    throw ex;
                throw ex.Rethrow();
            }
            return response;
        }

        static TResult EnrichWithRequestOnException<TResult>(string apiMethodName, object requestObj, Func<TResult> func)
        {
            try
            {
                return func();
            }
            catch (Exception ex) when (BuildDiagnosticsOnExceptionAndReturnFalse(apiMethodName, requestObj, ex))
            {
                throw; // for compiler, because this line will never be reached
            }
        }

        // This method is duplicated in multiple project
        // TODO : Remplacer par Dump dans technicaltools ?
        static bool BuildDiagnosticsOnExceptionAndReturnFalse(string apiMethodName, object requestObj, Exception ex)
        {
            var objType = requestObj.GetType();
            string request = objType.Name
                           + Environment.NewLine
                                                      + "{";
            if (objType == typeof(string))
            {
                request += " ";
                request += Newtonsoft.Json.JsonConvert.ToString((string)requestObj);
                request += " ";
            }
            else
            {
                Func<string, object, string> convert = (keyName, value) =>
                {
                    if (value == null)
                        return "<NULL>";
                    else if (value is string)
                    {
                        if (keyName.ToLower().Contains("pass"))
                            return "\"[...]\"";
                        else
                            return "\""
                                 + ((string)value).Replace("\\", "\\\\")
                                                  .Replace("\r", @"\r")
                                                  .Replace("\n", @"\n")
                                                  .Replace("\"", "\\\"")
                                 + "\"";
                    }
                    return value.ToStringInvariant();
                };

                request += Environment.NewLine;
                var props = objType.GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
                foreach (var prop in props)
                    if (prop.CanRead &&  // obvious 
                        prop.GetIndexParameters().Length == 0) // Filter out property like Chars on string type 
                        request += "  " + prop.Name + " = " + convert(prop.Name, prop.GetValue(requestObj)) + Environment.NewLine;
                var fields = objType.GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
                foreach (var field in fields)
                    request += "  " + field.Name + " = " + convert(field.Name, field.GetValue(requestObj)) + Environment.NewLine;
            }
            request += "}";
            ex.EnrichDiagnosticWith("API Call to " + apiMethodName + " with " + request, eExceptionEnrichmentType.Technical);
            return false;
        }

        public void Dispose()
        {
            // Nothing, IDisposable was just to have a "using (...) { }" syntax 
        }
        
        #endregion
    }
}
