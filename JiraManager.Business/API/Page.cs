﻿using System;
using System.Collections.Generic;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business.API
{
    class Page<TItem> : DtoObject
        where TItem : IHasJsonStorage
    {
        public string nextPage   { get { return GetValue("nextPage"); } }
        public int    maxResults { get { return GetValue<int>("maxResults").Value; } }
        public int    startAt    { get { return GetValue<int>("startAt").Value; } }
        public int    total      { get { return GetValue<int>("total").Value; } }
        // isLast is sometimes to false when it should be true, and sometiems it is not present at all in json
        public bool?  isLast     { get { return GetValue<bool>("isLast"); } }

        public string self       { get { return GetValue("self"); } }
        public string expand     { get { return GetValue("expand"); } }

        public List<TItem> Items { get; set; }

        // Last array field is specific to API call
    }
}
