﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace JiraManager.Business.API
{
    [XmlType(AnonymousType = true)]
    public class Config
    {
        public string                     ApiUrl         { get { return _ApiUrl; } set { _ApiUrl = value?.TrimEnd('/'); } } string _ApiUrl; 
        public string                     EmailAccount   { get; set; } 
        public string                     ApiKey         { get; set; } 
        public Dictionary<string, string> CompanyCookies { get; set; } = new Dictionary<string, string>(); 
 
        public int?                       Timeout        { get; set; } 
 
        public int ApiVersion { get { return int.Parse(Path.GetFileName(ApiUrl)); } } 
    } 
}
