﻿using System;
using System.ComponentModel;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class Status : JiraObject<long>, IUserInteractiveObject
    {
        public long           Id                { get { return GetId<long>("id").Value;                      } }
        public string         Name              { get { return GetValue("name");                             } }
        public string         Description       { get { return GetValue("description");                      } }
        public string         IconUrl           { get { return GetValue("iconUrl");                          } }

        [IsTechnicalProperty][Browsable(false)]
        public long           StatusCategoryId  { get { return GetValue<long>("statusCategory", "id").Value; } }
        public StatusCategory StatusCategory    { get { return GetObject<StatusCategory>(StatusCategoryId);  } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new Status(); }

        public override string ToString() { return Name; }
    }
}
