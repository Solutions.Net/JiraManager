﻿using System;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class IssueLinkType : JiraObject<long>, IUserInteractiveObject
    {
        public long   Id        { get { return GetId<long>("id").Value; } }
        public string Name      { get { return GetValue("name");        } }
        public string Inward    { get { return GetValue("inward");      } }
        public string Outward   { get { return GetValue("outward");     } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new IssueLinkType(); }

        public override string ToString() { return Name; }
    }
}
