﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using TechnicalTools;
using TechnicalTools.Extensions.Data;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class Issue : JiraObject<long>
    {
        [Display(Order = 0)]
        public long             Id               { get { return GetId<int>("id").Value; } set { SetId(value, "id"); } }
        public string           Summary          { get { return GetValue("fields", "summary"); } }
        public DateTime?        Updated          { get { return GetValue<DateTime>("fields", "updated"); } }
        public DateTime         Created          { get { return GetValue<DateTime>("fields", "created") ?? DateTime.MinValue; } }

        public float?           StoryPoint         { get { return GetValue<float>(BusinessValues.StoryPointField);         } set { SetValue(value, BusinessValues.StoryPointField); } }
        public float?           StoryPointEstimate { get { return GetValue<float>(BusinessValues.StoryPointEstimateField); } set { SetValue(value, BusinessValues.StoryPointEstimateField); } }
        
        // ADF document (Atlassian Document Format) : https://developer.atlassian.com/cloud/jira/platform/apis/document/structure/
        [IsTechnicalProperty][Display(Order = -1)]
        public string           RawDescription   { get { return JsonUtils.Instance.SelectToken(Storage, false, "fields", "description")?.ToString(); } }
        public string           Description      { get { return _Description ?? (_Description = BuildDescription(JsonUtils.Instance.SelectToken(Storage, false, "fields", "description"))); } } string _Description;



        [Display(Order = -1)]
        public string           Key              { get { return GetValue("key") ?? string.Empty; /* Empty in case Issue is new*/ } set { SetValue(value, "key"); }  }
        public Issue            Self             { get { return this; } }
        public override string  ToString()       { return Key; }

        [IsTechnicalProperty][Display(Order = -1)]
        public string           ProjectKey       { get { return Key.Remove(Key.IndexOf('-')); } }
        public Project          Project          { get { return Repository.GetProject(ProjectKey); } }

        // This field can be null
        [IsTechnicalProperty][Display(Order = -1)]
        public string           ComponentName    { get { return GetValue("fields", "components", 0, "name"); } set { SetValue(value, "fields", "components", 0, "name"); } }
        public ProjectComponent Component        { get { return Repository.GetComponent(ProjectKey, ComponentName); } set { ComponentName = value.Name ?? ""; } }

        [IsTechnicalProperty][Display(Order = -1)]
        public string           ParentTaskKey    { get { return GetValue(BusinessValues.ParentTaskField); } set { SetValue(value, BusinessValues.ParentTaskField); } }
        public Issue            ParentTask       { get { return Repository.GetIssue(ParentTaskKey); } set { ParentTaskKey = value?.Key; } }

        [IsTechnicalProperty][Display(Order = -1)]
        public long             IssueTypeId      { get { return GetValue<long>("fields", "issuetype", "id").Value; } set { SetValue(value, "fields", "issuetype", "id"); } }
        public IssueType        IssueType        { get { return Repository.GetIssueType(IssueTypeId); } set { IssueTypeId = value.Id; } }

        [IsTechnicalProperty][Display(Order = -1)]
        public long             StatusId         { get { return GetValue<long>("fields", "status", "id").Value; } set { SetValue(value, "fields", "status", "id"); } }
        public Status           Status           { get { return Repository.GetStatus(StatusId); } set { StatusId = value.Id; } }
        public StatusCategory   StatusCategory   { get { return Status.StatusCategory; } }

        [IsTechnicalProperty][Display(Order = -1)]
        public long?            PriorityId       { get { return GetValue<long>("fields", "priority", "id"); } set { SetValue(value, "fields", "priority", "id"); } } 
        public Priority         Priority         { get { return PriorityId == null ? null : Repository.GetPriority(PriorityId.Value); } set { PriorityId = value.Id; } } 

        [IsTechnicalProperty][Display(Order = -1, Description = "The user who create the issue (see also Reporter who find/think about this issue)")] 
        public string           CreatorId        { get { return GetValue("fields", "creator", UserJsonIdField); } set { SetValue(value, "fields", "creator", "key"); } } 
        public User             Creator          { get { return GetObject<User>(CreatorId); } set { CreatorId = value.Id; } } 
        [IsTechnicalProperty][Display(Order = -1, Description = "The user who report the issue (by default this is the same than creator)")] 
        public string           ReporterId       { get { return GetValue("fields", "reporter", UserJsonIdField); } set { SetValue(value, "fields", "reporter", "key"); } } 
        public User             Reporter         { get { return GetObject<User>(ReporterId); } set { ReporterId = value.Id; } } 
        [IsTechnicalProperty][Display(Order = -1, Description = "The user who is assigned to work on this issue")] 
        public string           AssigneeId       { get { return GetValue("fields", "assignee", UserJsonIdField); } set { SetValue(value, "fields", "assignee", "key"); } } 
        public User             Assignee         { get { return GetObject<User>(AssigneeId); } set { AssigneeId = value.Id; } } 
        string UserJsonIdField { get { return User.UniqueIdJsonKey(); } } 
 

        internal IEnumerable<IssueLink>    LinksTo { get { return GetObjects<IssueLink, long>(new[] { "fields", "issueLinks" }, "id".WrapInArray()).Where(lnk => lnk.OutwardIssueId == Id); } }
        /// <summary> Links where "this" blocks others issues </summary>
        [IsTechnicalProperty] [Browsable(false)]
        public IEnumerable<IssueLink> BlockLinks     { get { return LinksTo.Where(lnk => lnk.Type == BusinessValues.Blocks); } }

        internal IEnumerable<IssueLink>  LinksFrom { get { return GetObjects<IssueLink, long>(new[] { "fields", "issueLinks" }, "id".WrapInArray()).Where(lnk => lnk.InwardIssueId == Id); } }
        public IEnumerable<IssueLink> BlockedByLinks { get { return LinksFrom.Where(lnk => lnk.Type == BusinessValues.Blocks); } }


        [IsTechnicalProperty][Browsable(false)]
        public string       Expand               { get { return GetValue("expand"); } set { SetValue(value, "expand"); } }
        [IsTechnicalProperty][Browsable(false)]
        public string       ApiSelfUrl           { get { return GetValue("self"); } }
        [IsTechnicalProperty][Browsable(false)]
        public string       InternetUrl          { get { return Repository.ServerInfos.Single().BaseUrl + "/browse/" + Key; }  }




        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new Issue(); }

        string BuildDescription(JToken jToken)
        {
            if (jToken == null || jToken.Type == JTokenType.Null)
                return string.Empty;
            if (jToken is JValue value) // for api v2
                return value.Value?.ToString() ?? string.Empty;
            var tok = jToken;
            var content = tok["content"] as JArray;
            var type = tok["type"].Value<string>();
            if (type == "text")
            {
                var text = tok["text"].Value<string>();
                var marks = (JArray)tok["marks"];
                var before = string.Empty;
                var after = " ";
                if (marks != null)
                    foreach (var mark in marks)
                    {
                        var markType = mark["type"].Value<string>();
                        if (markType == "strong")
                        {
                            before += "**";
                            after = "**" + after;
                        }
                        else if (markType == "underline")
                        {
                            before += "__";
                            after = "__" + after;
                        }
                        else if (markType == "link")
                        {
                            var link = mark["attrs"]["href"].Value<string>();
                            if (!text.Contains(link))
                                text += "(" + link + ")";
                        }
                        else if (markType == "code")
                        {
                            before += "<code>" + Environment.NewLine;
                            after = Environment.NewLine + "</code>" + after;
                        }
                        else if (markType == "em") // italic
                        {
                            before += "//";
                            after = "//" + after;
                        }
                        else if (markType == "textColor")
                        {
                            // Nothing
                            // recupere au format #RRGGBB
                            //var colorHex = mark["attrs"]["color"].Value<string>();
                        }
                        else if (markType == "strike")
                        {
                            before += "~~";
                            after = "~~" + after;
                        }
                        else
                        {
                            // No idea ! Not implemented
                            TechnicalTools.Diagnostics.DebugTools.Break();
                        }
                    }
                return before + text + after;
            }
            if (tok["marks"] != null)
            {
                // "marks" is not expected here
                TechnicalTools.Diagnostics.DebugTools.Break();
            }
            if (type == "paragraph")
                return content.Select(subTok => BuildDescription(subTok)).Join(" ") + Environment.NewLine + Environment.NewLine;
            if (type == "doc")
                return content.Select(subTok => BuildDescription(subTok)).Join("");
            if (type == "hardBreak")
                return Environment.NewLine;
            if (type == "bulletList")
                return content.Select(subTok => "• " + BuildDescription(subTok)).Join("");
            if (type == "listItem")
                return content.Select(subTok => BuildDescription(subTok)).Join(" ");
            if (type == "heading")
            {
                var level = tok["attrs"]["level"].Value<int>();
                return "<h" + level + ">" + content.Select(subTok => BuildDescription(subTok)).Join(" ") + "</h" + level + ">" + Environment.NewLine + Environment.NewLine;
            }
            if (type == "mediaSingle")
                return BuildDescription(content.Single());
            if (type == "media")
                return "<Media>" + Environment.NewLine + tok["attrs"].ToString() + Environment.NewLine + "</Media>" + Environment.NewLine + Environment.NewLine;
            return "";
        }
    }
}
