﻿using System;

using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class ServerInfo : JiraObject<string>
    {
        public string                   BaseUrl          { get { return GetId("baseUrl"); } }
        public string                   Version          { get { return GetValue("version"); }                set { SetValue(value, "version"); } }
        //public int[]                    VersionNumbers   { get { return GetValue<int[]>("versionNumbers"); } }
        public string                   DeploymentType   { get { return GetValue("deploymentType"); }         set { SetValue(value, "deploymentType"); } }
        public int                      BuildNumber      { get { return GetValue<int>("buildNumber").Value; } set { SetValue(value, "buildNumber"); } }
        public DateTime?                BuildDate        { get { return GetValue<DateTime>("buildDate"); }    set { SetValue(value, "buildDate"); } }
        public DateTime?                ServerTime       { get { return GetValue<DateTime>("serverTime"); }   set { SetValue(value, "serverTime"); } }
        public string                   ScmInfo          { get { return GetValue("scmInfo"); }                set { SetValue(value, "scmInfo"); } }
        public string                   BuildPartnerName { get { return GetValue("buildPartnerName"); }       set { SetValue(value, "buildPartnerName"); } }
        public string                   ServerTitle      { get { return GetValue("serverTitle"); }            set { SetValue(value, "serverTitle"); } }
        //public IEnumerable<HealthCheck> HealthChecks     { get; set; }

        protected internal override IdTuple<string> IdTuple { get { return new IdTuple<string>(BaseUrl.Replace("https://", "")); } }
        protected override BaseBusinessObject CreateNewInstance() { return new ServerInfo(); }

        public override string ToString() { return BaseUrl; }
    }
}
