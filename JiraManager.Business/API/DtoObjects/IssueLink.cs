﻿using System;
using System.ComponentModel;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class IssueLink : JiraObject<long>, IUserInteractiveObject
    {
        public long          Id      { get { return GetId<long>("id").Value;             } }

        [IsTechnicalProperty][Browsable(false)]
        public long          TypeId  { get { return GetValue<long>("type", "id").Value;        } }
        public IssueLinkType Type    { get { return GetObject<IssueLinkType>(TypeId); } }

        [IsTechnicalProperty][Browsable(false)]
        public long   InwardIssueId  { get { return GetValue<long>("inwardIssue", "id").Value;  } }
        public Issue  InwardIssue    { get { return GetObject<Issue>(InwardIssueId); } }

        [IsTechnicalProperty][Browsable(false)]
        public long   OutwardIssueId { get { return GetValue<long>("outwardIssue", "id").Value; } }
        public Issue  OutwardIssue   { get { return GetObject<Issue>(OutwardIssueId); } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new IssueLink(); }
    }
}
