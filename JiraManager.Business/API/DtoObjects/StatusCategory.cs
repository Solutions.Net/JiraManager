﻿using System;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class StatusCategory : JiraObject<long>, IUserInteractiveObject
    {
        public long   Id                { get { return GetId<long>("id").Value;           } }
        public string Key               { get { return GetValue("key");                   } }
        public string Name              { get { return GetValue("name");                  } }
        public string ColorName         { get { return GetValue("colorName");             } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new StatusCategory(); }

        public override string ToString() { return Name ?? Key; }

        public static StatusCategory NoCategory { get { return BusinessSingletons.Instance.GetRepository().GetStatusCategory(1); } }
        public static StatusCategory ToDo       { get { return BusinessSingletons.Instance.GetRepository().GetStatusCategory(2); } }
        public static StatusCategory InProgress { get { return BusinessSingletons.Instance.GetRepository().GetStatusCategory(4); } }
        public static StatusCategory Done       { get { return BusinessSingletons.Instance.GetRepository().GetStatusCategory(3); } }
    }
}
