﻿using System;
using System.ComponentModel;
using TechnicalTools.Extensions.Data;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class User : JiraObject<string>
    {
        public string     Id           { get { return GetId(UniqueIdJsonKey()); } }
        public string     EmailAddress { get { return GetValue("emailAddress"); }       set { SetValue(value, "emailAddress"); } }
        public string     DisplayName  { get { return GetValue("displayName"); }        set { SetValue(value, "displayName"); } }
        public bool       Active       { get { return GetValue<bool>("active").Value; } set { SetValue(value, "active"); } }
        public string     TimeZone     { get { return GetValue("timeZone"); }           set { SetValue(value, "timeZone"); } }
        public string     AccountType  { get { return GetValue("accountType"); }        set { SetValue(value, "accountType"); } }

        [Browsable(false)][IsTechnicalProperty]
        public string     self         { get { return GetValue("self"); }               set { SetValue(value, "self"); } }
        //public AvatarUrls avatarUrls   { get { return GetValue<AvatarUrls>("avatarUrls"); }  }

        protected internal override IdTuple<string> IdTuple { get { return new IdTuple<string>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new User(); }

        public static string UniqueIdJsonKey(int? apiVersion = null)
        {
            apiVersion = apiVersion ?? BusinessSingletons.Instance.GetConfigOfUser().JiraApiVersion;
            switch (apiVersion) { case 2: return "key"; case 3: return "accountId"; default: throw new NotImplementedException(); }
        }

        public override string ToString() { return DisplayName; }
    }
}
