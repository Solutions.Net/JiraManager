﻿using System;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class Resolution : JiraObject<long>, IUserInteractiveObject
    {
        public long   Id                { get { return GetId<long>("id").Value;           } }
        public string Name              { get { return GetValue("name");                  } }
        public string Description       { get { return GetValue("description");           } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new Resolution(); }

        public override string ToString() { return Name; }
    }
}
