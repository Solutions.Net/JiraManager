﻿using System;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class IssueType : JiraObject<long>, IUserInteractiveObject
    {
        public long   Id          { get { return GetId<long>("id").Value;           } }
        public string Name        { get { return GetValue("name");                  } }
        public long   AvatarId    { get { return GetValue<long>("avatarId").Value;  } }
        public string Description { get { return GetValue("description");           } }
        public string IconUrl     { get { return GetValue("iconUrl");               } }
        public bool   Subtask     { get { return GetValue<bool>("subtask").Value;   } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new IssueType(); }

        public override string ToString() { return Name; }
    }
}
