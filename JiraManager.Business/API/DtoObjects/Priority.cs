﻿using System;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class Priority : JiraObject<long>, IUserInteractiveObject
    {
        public long   Id                { get { return GetId<long>("id").Value;           } }
        public string Name              { get { return GetValue("name");                  } }
        public string Description       { get { return GetValue("description");           } }
        public string IconUrl           { get { return GetValue("iconUrl");               } }
        public string Color             { get { return GetValue("statusColor");               } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new Priority(); }

        public override string ToString() { return Name; }
    }
}
