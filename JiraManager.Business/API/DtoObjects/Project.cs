﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class Project : JiraObject<long>
    {
        public long   Id             { get { return GetId<long>("id").Value; } }
        public string Key            { get { return GetValue("key"); }                      set { SetValue(value, "key"); } }
        public string description    { get { return GetValue("description"); }              set { SetValue(value, "description"); } }
        public string assigneeType   { get { return GetValue("assigneeType"); }             set { SetValue(value, "assigneeType"); } }
        public string Name           { get { return GetValue("name"); }                     set { SetValue(value, "name"); } }
        public string projectTypeKey { get { return GetValue("projectTypeKey"); }           set { SetValue(value, "projectTypeKey"); } }
        public bool   simplified     { get { return GetValue<bool>("simplified").Value; }   set { SetValue(value, "simplified"); } }
        public string style          { get { return GetValue("style"); }                    set { SetValue(value, "style"); } }
        public bool   isPrivate      { get { return GetValue<bool>("isPrivate").Value; }    set { SetValue(value, "isPrivate"); } }

        [Browsable(false)][IsTechnicalProperty]
        public string expand         { get { return GetValue("expand"); }                   set { SetValue(value, "expand"); } }
        [Browsable(false)][IsTechnicalProperty]
        public string self           { get { return GetValue("self"); }                     set { SetValue(value, "self"); } }

        public string IntranetUrl    { get { return "https://lemonway.atlassian.net/browse/" + Key; } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new Project(); }

        public override string ToString() { return Key; }

        public List<ProjectComponent> GetComponents(IProgress<string> pr)
        {
            var cached = BusinessSingletons.Instance.GetRepository();
            return cached.GetListFromMemory<ProjectComponent>(pr)
                         .Where(c => c.ProjectKey == this.Key)
                         .ToList();
        }

        public List<Issue> GetIssues(IProgress<string> pr)
        {
            var cached = BusinessSingletons.Instance.GetRepository();
            return cached.GetListFromMemory<Issue>(pr)
                         .Where(i => i.ProjectKey == Key)
                         .ToList();
        }
    }
}
