﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API.DtoObjects
{
    public class ProjectComponent : JiraObject<long>, IUserInteractiveObject
    {
        public long   Id         { get { return GetId<long>("id").Value; }              set { SetId(value, "id"); } }
        public string Name       { get { return GetValue("name"); }                     set { SetValue(value, "name"); } }
        public long   ProjectId  { get { return GetValue<long>("projectId").Value; }    set { SetValue(value, "projectId"); } }
        public string ProjectKey { get { return GetValue("project"); }                  set { SetValue(value, "project"); } }

        protected internal override IdTuple<long> IdTuple { get { return new IdTuple<long>(Id); } }
        protected override BaseBusinessObject CreateNewInstance() { return new ProjectComponent(); }

        public override string ToString() { return Name; }

        public ProjectComponent Self    { get { return this; } }
        public Project          Project { get { return Repository.GetProject(ProjectKey); } }

        [IsTechnicalProperty]
        [Browsable(false)]
        public string InternetUrl { get { return Repository.ServerInfos.Single().BaseUrl + "/issues/?jql=" + System.Web.HttpUtility.UrlEncode("project = \"" + ProjectKey + "\" AND component = \"" + Name + "\""); } }

        public List<Issue> GetIssues(IProgress<string> pr)
        {
            return Project.GetIssues(pr)
                          .Where(i => i.Component?.Name == Name)
                          .ToList();
        }
    }
}
