using System;
using System.Runtime.CompilerServices;

using ApplicationBase.Common;
using ApplicationBase.Business;
using JiraManager.Common.Logging;


namespace JiraManager.Business
{
    public abstract class Program : ApplicationBase.Business.Program
    {
        protected override void Initialize()
        {
            RuntimeHelpers.RunClassConstructor(typeof(CommonSingletons).TypeHandle);
            RuntimeHelpers.RunClassConstructor(typeof(BusinessSingletons).TypeHandle);
            base.Initialize();

            TechnicalTools.Tools.Threading.Thread.ForceInitialization();

            // Action required to load native assemblies
            // To deploy an application that uses spatial data types to a machine that does not have 'System CLR Types for SQL Server' installed you also need to deploy the native assembly SqlServerSpatial110.dll. Both x86 (32 bit) and x64 (64 bit) versions of this assembly have been added to your project under the SqlServerTypes\x86 and SqlServerTypes\x64 subdirectories. The native assembly msvcr100.dll is also included in case the C++ runtime is not installed. 

            // You need to add code to load the correct one of these assemblies at runtime (depending on the current architecture). 

            // ASP.NET applications
            // For ASP.NET applications, add the following line of code to the Application_Start method in Global.asax.cs: 

            // SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));

            // Desktop applications
            // For desktop applications, add the following line of code to run before any spatial operations are performed: 

            //SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
        }

        protected override ApplicationBase.Business.Config CreateConfig()
        {
            return new Config();
        }
        protected override void CreateDefaultLogHandler()
        {
            StdLogHandler = new Log4NetLogHandler(TechnicalTools.Logs.LogManager.Default);
        }
        protected Log4NetLogHandler StdLogHandler;
        protected override void ConfigureLogHandler(string config)
        {
            StdLogHandler.ConfigureLog4netWith(config);
        }
        protected override AuthenticationManager GetAuthenticationManager()
        {
            return BusinessSingletons.Instance.GetAuthenticationManager();
        }

        protected override void OnConfigLoaded(ApplicationBase.Business.Config cfg, BootstrapConfig bcfg)
        {
            base.OnConfigLoaded(cfg, bcfg);
            Common.BootstrapConfig.Instance = (Common.BootstrapConfig)bcfg;
            //Any_DAL_Class.InjectExtractionMethodToDALClass();
            bool firstTime = true;
            GetAuthenticationManager().CurrentUserChanged += () =>
            {
                BootstrapConfig.Instance.DataAccessor = null;
                if (firstTime)
                {
                    //Common.DB.MyDatabase.CheckAllEnums();

                    // Optional loading
                    try
                    {
                        //Common.DB.AnotherDatabase.CheckAllEnums();
                    }
                    catch (Exception ex)
                    {
                        TechnicalTools.Tools.BusEvents.Instance.RaiseBusinessWarning($"Data of optional DB won't be available for your session of {bcfg.ApplicationName}!", _log, new[] { ex });
                    }
                }
                firstTime = false;
            };
            
        }
    }
}

