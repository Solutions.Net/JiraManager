﻿using System;

using JiraManager.Business.API.DtoObjects;

using TechnicalTools.Model.Cache;


namespace JiraManager.Business.ObjectExtensions
{
    public class ServerLocalData : ExtensionObject<string, ServerInfo>
    {
        int SeedId { get { return GetValue<int>("SeedId") ?? 0; } set { SetValue(value, "SeedId"); } }

        public ServerLocalData(IIdTuple<string, string> ownerIdTuple)
            : base(ownerIdTuple)
        {
        }
        public ServerLocalData(ServerInfo owner)
            : base(owner)
        {
        }

        protected override BaseBusinessObject CreateNewInstance()
        {
            return new ServerLocalData((ServerInfo)null);
        }

        public int NewId()
        {
            lock (_lck)
            {
                --SeedId;
                return SeedId;
            }
        }
        readonly object _lck = new object();
    }
}
