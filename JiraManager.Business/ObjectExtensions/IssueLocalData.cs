﻿using System;

using JiraManager.Business.API.DtoObjects;

using TechnicalTools.Model.Cache;


namespace JiraManager.Business.ObjectExtensions
{
    public class IssueLocalData : ExtensionObject<long, Issue>
    {
        public float  Left    { get { return GetValue<float>("Left") ?? 0; }   set { SetValue(value, "Left"); } }
        public float  Top     { get { return GetValue<float>("Top") ?? 0; }    set { SetValue(value, "Top"); } }
        public float  Width   { get { return GetValue<float>("Width") ?? 0; }  set { SetValue(value, "Width"); } }
        public float  Height  { get { return GetValue<float>("Height") ?? 0; } set { SetValue(value, "Height"); } }

        public long? ParentContainer { get { return GetValue<long>("ParentContainer"); } set { SetValue(value, "ParentContainer"); } }

        public string Summary { get { return GetValue("Summary") ?? ""; }      set { SetValue(value, "Summary"); } }

        public IssueLocalData(IIdTuple<long, string> ownerIdTuple)
            : base(ownerIdTuple)
        {
        }
        public IssueLocalData(Issue owner)
            : base(owner)
        {
        }

        protected override BaseBusinessObject CreateNewInstance()
        {
            return new IssueLocalData((Issue)null);
        }
    }
}
