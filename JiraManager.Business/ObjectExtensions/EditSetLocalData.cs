﻿using System;

using JiraManager.Business.API.DtoObjects;

using TechnicalTools.Model.Cache;


namespace JiraManager.Business.ObjectExtensions
{
    public class EditSetLocalData : ExtensionObject<long, JiraObject<long>>
    {
        public float  Top     { get { return GetValue<float>("Top").Value; } set { SetValue(value, "Top"); } }
        public float  Left    { get { return GetValue<float>("Left").Value; } set { SetValue(value, "Left"); } }
        public float  Width   { get { return GetValue<float>("Width").Value; } set { SetValue(value, "Width"); } }
        public float  Height  { get { return GetValue<float>("Height").Value; } set { SetValue(value, "Height"); } }

        public string Summary { get { return GetValue("Summary"); } set { SetValue(value, "Summary"); } }

        public EditSetLocalData(IIdTuple<long, string> ownerIdTuple)
            : base(ownerIdTuple)
        {
        }
        public EditSetLocalData(JiraObject<long> owner)
            : base(owner)
        {
        }

        protected override BaseBusinessObject CreateNewInstance()
        {
            return new EditSetLocalData((JiraObject<long>)null);
        }
    }
}
