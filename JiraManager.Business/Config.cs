﻿using System;
using System.Collections.Generic;
using System.Threading;

using TechnicalTools.Diagnostics;
using TechnicalTools.Tools;


using ApplicationBase.Business;


namespace JiraManager.Business
{
    public class Config : ApplicationBase.Business.Config
    {
        [ConfigType(FullTypeName = nameof(JiraManager) + "." + nameof(Common) + "." + nameof(Common.Config))]
        public override ApplicationBase.Common.Config Connections { get { return base.Connections; } protected set { base.Connections = value; } }

        [ConfigType(FullTypeName = nameof(JiraManager) + ".UI.ViewConfig")]
        public override ViewConfig View { get { return base.View; } protected set { base.View = value; } }

        public Automation.Config         Automation = new Automation.Config();

        public new static Config Instance
        {
            get { return (Config)ApplicationBase.Business.Config.Instance; }
            set { ApplicationBase.Business.Config.Instance = value; }
        }

        protected internal Config() { }

        public override void ShallowCopyFrom(ApplicationBase.Business.Config from)
        {
            base.ShallowCopyFrom(from);
            var source = from as Config;
            if (source == null)
                return;

            //GmailAccountAccessToken = source.GmailAccountAccessToken;
            Automation = source.Automation;
        }

        protected override void Apply()
        {
            base.Apply();
            
            // After that we can load DB assemblies in a normal way
            //DB.MyDatabase.CheckAllEnums();

            new Thread(DoRuntimeChecks) { Name = nameof(DoRuntimeChecks) }.Start();
        }

        protected override void AdaptSettings(Dictionary<string, string> values)
        {
            //string specialCase = nameof(GmailAccountAccessToken) + "."
            //                   + nameof(GmailClientConfig.installed) + "."
            //                   + nameof(GmailClientConfig.ClientConfig.redirect_uris);
            //if (values.ContainsKey(specialCase))
            //{
            //    GmailAccountAccessToken.installed.redirect_uris = values[specialCase].Split('|').ToArray();
            //    values.Remove(specialCase);
            //}
        }

        /// <summary>
        /// Verifie les choses qui peuvent l'etre au runtime et que les developpeurs oublieront avec le temps
        /// Cela evite les surprises en producton (tests oubliés)
        /// Ce code n'est en realité pas necessaire mais permet au passage des chargements optimisé car géré de maniere ensembliste
        /// </summary>
        protected internal virtual void DoRuntimeChecks()
        {
            try
            {
                if (DebugTools.IsForDevelopper)
                    ConfigOfUser.CheckAllValues();

                //DateTime dt = DateTime.UtcNow;


                //var time = DateTime.UtcNow - dt;
                //Console.WriteLine(nameof(DoRuntimeChecks) + " done in " + time.ToString() + ".");
            }
            catch (Exception ex)
            {
                if (DebugTools.IsForDevelopper)
                    throw;
                ExceptionManager.Instance.NotifyException(ex, UnexpectedExceptionManager.eExceptionKind.UnhandledButIgnored, Thread.CurrentThread.Name);
            }
        }
    }
}
