﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

using Newtonsoft.Json.Linq;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using JiraManager.Business.ObjectExtensions;
using JiraManager.Business.Repositories;


namespace JiraManager.Business.API.DtoObjects
{
    public abstract partial class JiraObject : BaseBusinessObject
    {
        [XmlIgnore]
        [IsTechnicalProperty][Browsable(false)]
        public virtual JiraAPI OwnerMapper { get; protected internal set; }

        #region Edit detection (generic)

        internal EditSetLocalData Edits { get { return Repository.GetOrCreateLocalObject<EditSetLocalData>(TypedId); } }

        internal bool AllowEdits      { get; set; }
        internal bool HasEdits        { get { return Repository.GetOrCreateLocalObject<EditSetLocalData>(TypedId, false) != null; } }
        internal bool HasUnsavedEdits { get { return Repository.GetOrCreateLocalObject<EditSetLocalData>(TypedId, false)?.DataHasChanged ?? false; } }

        protected internal T? GetId<T>(params object[] keys)
            where T : struct
        {
            // We do not use "Edits" here to prevent stackoverflow.
            // "Id" data are not editable anyway...
            return base.GetValue<T>(keys);
        }
        protected internal string GetId(params object[] keys)
        {            
            // We do not use "Edits" here to prevent stackoverflow.
            // "Id" data are not editable anyway...
            return base.GetValue(keys);
        }
        protected internal override T? GetValue<T>(params object[] keys)
            //where T : struct
        {
            var edits = Repository.GetOrCreateLocalObject<EditSetLocalData>(TypedId, false);
            var token = edits == null ? null : JsonUtils.Instance.SelectToken(edits.Storage, false, keys);
            if (token == null || token.Type == JTokenType.Null)
                return base.GetValue<T>(keys);
            return token.Value<T>();
        }
        protected internal override string GetValue(params object[] keys)
        {
            var edits = Repository.GetOrCreateLocalObject<EditSetLocalData>(TypedId, false);
            var token = edits == null ? null : JsonUtils.Instance.SelectToken(edits.Storage, false, keys);
            if (token == null || token.Type == JTokenType.Null)
                return base.GetValue(keys);
            return token.Value<string>();
        }


        protected internal virtual CachedRepository Repository { get { return BusinessSingletons.Instance.GetRepository(); } }

        protected internal virtual T GetObject<T>(string id)
            where T : JiraObject, IHasClosedIdReadable<string>
        {
            if (id == null)
                return null;
            var repo = Repository;
            // https://confluence.atlassian.com/display/JIRA044/Changing+the+Project+Key?_ga=2.161494012.1556065556.1592502964-39994059.1574641287
            if (typeof(T) == typeof(Project))
                return (T)(object)repo.GetProject(id);
            return repo.GetJiraObject<T>(new IdTuple<string>(id));
        }
        protected internal virtual T GetObject<T>(long? id)
            where T : JiraObject, IHasClosedIdReadable<long>
        {
            return id == null ? null : Repository.GetJiraObject<T>(new IdTuple<long>(id.Value));
        }

        protected internal virtual IEnumerable<T> GetObjects<T, TKey>(string[] arrayPath, string[] idPath)
            where T : JiraObject, IHasClosedIdReadable<long>
            where TKey : IEquatable<TKey>, IComparable<TKey>, IComparable
        {
            var array = (JArray)JsonUtils.Instance.SelectToken(Storage, false, arrayPath);
            if (array != null)
                foreach (var item in array)
                {
                    var token = JsonUtils.Instance.SelectToken(item, false, idPath);
                    var id = token.Value<TKey>();
                    yield return Repository.GetJiraObject<T>(new IdTuple<TKey>(id));
                }
        }

        protected internal void SetId<T>(T value, params object[] keys)
        {
            if (!AllowEdits)
                throw new TechnicalException("Edits forbidden!", null);
            // We do not use Edits because it would cause an infinite loop
            base.SetValue(value, keys);
        }

        protected internal override void SetValue<T>(T value, params object[] keys)
        {
            if (!AllowEdits)
                throw new TechnicalException("Edits forbidden!", null);
            Edits.SetValue(value, keys);
        }

        void CleanEdits()
        {
            var edits = Repository.GetOrCreateLocalObject<EditSetLocalData>(TypedId, false);
            if (edits == null)
                return;
            //HashSet<string> toRemove = null;
            //foreach (var edit in edits)
            //    if (new JValue(edit.Value) == Storage[edit.Key])
            //    {
            //        toRemove = toRemove ?? new HashSet<string>();
            //        toRemove.Add(edit.Key);
            //    }
            //if (toRemove != null)
            //    foreach (var key in toRemove)
            //        Edits.Remove(key);
        }

        #endregion


        public override int GetHashCode()
        {
            int hashCode = GetType().GetHashCode();
            hashCode = (hashCode * 397) ^ GetIdTuple().GetHashCode();
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is JiraObject asBaseJsonDto))
                return false;
            return GetType() == obj.GetType()
                && GetIdTuple().Equals(asBaseJsonDto.GetIdTuple()) // ok object match but ...
                && ReferenceEquals(OwnerMapper, asBaseJsonDto.OwnerMapper); // ... do they belongs to the same owner (ie: in general it means same database) ?
        }
    }

    public abstract class JiraObject<TId1> : JiraObject, IHasTypedIdReadable<TId1>, ICloneable, IComparable<JiraObject<TId1>>
        where TId1 : IEquatable<TId1>, IComparable<TId1>, IComparable
    {
        /// <summary>
        /// Représente l'id de l'objet. L'id est un IIdTuple (composition de plusieurs données de type simple : int string date etc)
        /// Cette proprieté est nommé IdTuple pour eviter un conflit de nom avec la propriete courrament nommée Id dans les classes filles
        /// </summary>
        protected internal new abstract IdTuple<TId1> IdTuple { get; }
        protected override IIdTuple GetIdTuple() { return IdTuple; }
        IdTuple<TId1> IHasClosedIdReadable<TId1>.IdTuple { get { return IdTuple; } }
        protected internal new TypedId<TId1> TypedId => IdTuple.ToTypedId(GetType()); TypedId<TId1> IHasTypedIdReadable<TId1>.TypedId => TypedId;
        TypedId<TId1> IHasTypedIdReadable<TId1>.MakeTypedId( IdTuple<TId1> id) { return id.ToTypedId(GetType()); }

        public int CompareTo(JiraObject<TId1> other) { return ObjectComparisonHelper.CompareIfOverridingToString(this, other); }
    }

    public abstract class JiraObject<TId1, TId2> : JiraObject, IHasTypedIdReadable<TId1, TId2>, ICloneable, IComparable<JiraObject<TId1, TId2>>
        where TId1 : IEquatable<TId1>, IComparable<TId1>, IComparable
        where TId2 : IEquatable<TId2>, IComparable<TId2>, IComparable
    {
        /// <summary>
        /// Représente l'id de l'objet. L'id est un IIdTuple (composition de plusieurs données de type simple : int string date etc)
        /// Cette proprieté est nommé IdTuple pour eviter un conflit de nom avec la propriete courrament nommée Id dans les classes filles
        /// </summary>
        protected internal new abstract IdTuple<TId1, TId2> IdTuple { get; }
        protected override IIdTuple GetIdTuple() { return IdTuple; }
        IdTuple<TId1, TId2> IHasClosedIdReadable<TId1, TId2>.IdTuple { get { return IdTuple; } }
        protected internal new TypedId<TId1, TId2> TypedId => IdTuple.ToTypedId(GetType()); TypedId<TId1, TId2> IHasTypedIdReadable<TId1, TId2>.TypedId => TypedId;
        TypedId<TId1, TId2> IHasTypedIdReadable<TId1, TId2>.MakeTypedId( IdTuple<TId1, TId2> id) { return id.ToTypedId(GetType()); }

        public int CompareTo(JiraObject<TId1, TId2> other) { return ObjectComparisonHelper.CompareIfOverridingToString(this, other); }
    }
}
