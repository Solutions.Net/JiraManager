﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model.Cache;

using JiraManager.Business.API;
using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.Repositories;


namespace JiraManager.Business.ObjectExtensions
{
    public static class JiraObject_Extensions
    {
        public static ServerLocalData GetLocalData(this ServerInfo si, bool create = true) { return BusinessSingletons.Instance.GetRepository().GetOrCreateLocalObject<ServerLocalData>(si.TypedId, create); }
        public static IssueLocalData  GetLocalData(this Issue issue, bool create = true)   { return BusinessSingletons.Instance.GetRepository().GetOrCreateLocalObject<IssueLocalData>(issue.TypedId, create); }
    }

    public abstract class ExtensionObject : BaseBusinessObject, ICloneable
    {
        public override int GetHashCode()
        {
            int hashCode = GetType().GetHashCode();
            hashCode = (hashCode * 397) ^ GetIdTuple().GetHashCode();
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ExtensionObject asBaseJsonDto))
                return false;
            return GetType() == obj.GetType()
                && GetIdTuple().Equals(asBaseJsonDto.GetIdTuple());
        }

        protected internal virtual CachedRepository Repository { get { return BusinessSingletons.Instance.GetRepository(); } }
    }

    public abstract class ExtensionObject<TId1, TOwner> : ExtensionObject, IHasCompositeTypedIdReadable<TId1>, /*IHasCompositeOwner<TOwner>,*/ IComparable<ExtensionObject<TId1, TOwner>>
        where TId1 : IEquatable<TId1>, IComparable<TId1>, IComparable
        where TOwner : JiraObject, IHasClosedIdReadable<TId1>
    {
        [IsTechnicalProperty][Browsable(false)]
        public new IdTuple<TId1, string> IdTuple        { get; private set; }
        [IsTechnicalProperty][Browsable(false)]
        public     IdTuple<TId1>         OwnerIdTuple   { get { return IdTuple.Shortenize(); } }
        [IsTechnicalProperty][Browsable(false)]
        public Type                      OwnerRealType  { get { return Type.GetType((string)IdTuple.Keys.Last()); } }
        [IsTechnicalProperty][Browsable(false)]
        public TOwner                    Owner          { get { return Repository.GetJiraObject<TOwner>(OwnerIdTuple); } }

        /// <summary>
        /// Représente l'id de l'objet extension. Il s'agit de OwnerIdTuple + string identifiant le type du proprietaire
        /// </summary>
        protected override IIdTuple GetIdTuple() { return IdTuple; }
        IdTuple<TId1, string> IHasClosedIdReadable<TId1, string>.IdTuple => IdTuple;
        [IsTechnicalProperty][Browsable(false)]
        public new TypedId<TId1, string> TypedId => IdTuple.ToTypedId(GetType());
        TypedId<TId1, string> IHasTypedIdReadable<TId1, string>.MakeTypedId( IdTuple<TId1, string> id) { return id.ToTypedId(GetType()); }
        

        public ExtensionObject(IIdTuple<TId1, string> ownerIdTuple)
        {
            IdTuple = new IdTuple<TId1, string>(ownerIdTuple.Id1, ownerIdTuple.Id2);
            Debug.Assert(typeof(TOwner).IsAssignableFrom(OwnerRealType));
        }
        public ExtensionObject(TOwner owner)
            : this((IIdTuple<TId1, string>)owner.TypedId)
        {
        }

        public int CompareTo(ExtensionObject<TId1, TOwner> other) { return ObjectComparisonHelper.CompareIfOverridingToString(this, other); }
    }
}
