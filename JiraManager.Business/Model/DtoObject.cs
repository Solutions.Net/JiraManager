﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using TechnicalTools;
using TechnicalTools.Extensions.Data;
using TechnicalTools.Model;


namespace JiraManager.Business.API.DtoObjects
{
    // https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-migrate-from-newtonsoft-how-to
    /// <summary>
    /// Base class for all objects return by API in general.
    /// And that wrap a json semi-parsed data
    /// </summary>
    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    public class DtoObject : IHasJsonStorage
    {
        [Browsable(false)]
        [IsTechnicalProperty]
        public JObject Storage { get { return _Storage; }
                                 set { if (ReferenceEquals(_Storage, value)) return; DetectChanges(value); _Storage = value; StorageChanged?.Invoke(this, EventArgs.Empty); } } JObject _Storage = new JObject();

        public event EventHandler StorageChanged;
        [IsTechnicalProperty]
        [Browsable(false)]
        public bool            DataHasChanged { get; internal set; }
        [IsTechnicalProperty]
        [Browsable(false)]
        public bool InterestingDataHasChanged { get; internal set; }

        internal T? GetValue<T>(params object[] keys)
            where T : struct
        {
            var token = JsonUtils.Instance.SelectToken(Storage, false, keys);
            return token == null || token.Type == JTokenType.Null ? (T?)null : token.Value<T>();
        }
        /// <summary> Same But for returning object (string or JToken) </summary>
        internal string GetValue(params object[] keys)
        {
            var token = JsonUtils.Instance.SelectToken(Storage, false, keys);
            if (token is JObject &&
                JsonUtils.Instance.SelectToken(token, false, "nonEditableReason", "message") is JValue jvalue &&
                jvalue.Value is string str && str == "The Parent Link is only available to Jira Premium users.")
                return null;
            return token == null || token.Type == JTokenType.Null ? null : token.Value<string>();
        }

        internal void    SetValue<T>(T     value, params object[] keys)
        {
            var tok = JsonUtils.Instance.SelectToken(Storage, true, keys);
            var tokValue = JToken.FromObject(value);
            var hasChanged = !JToken.DeepEquals(tokValue, tok);
            DataHasChanged |= hasChanged;
            var isInteresting = !BusinessValues.UninterestingPaths.TryGetValue(GetType(), out string[][] keyPaths)
                             || keyPaths.Any(keyPath => keyPath.Length == keys.Length &&
                                                        keyPath.SequenceEqual(keys));
            InterestingDataHasChanged |= isInteresting;
            var type = typeof(T);
            if ((typeof(string) == type ||
                type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>)) &&
                EqualityComparer<T>.Default.Equals(value, default(T)))
                tok.Remove();
            else
                tok.Replace(tokValue);
        }

        /// <summary>
        /// Update InterestingDataHasChanged & DataHasChanged when storage is replaced
        /// </summary>
        /// <param name="newStorage">Value that is about to replace Storage</param>
        void DetectChanges(JObject newStorage)
        {
            // If interesting changed have already been recorded there is nothing to do 
            // because we cannot compre with original data
            if (InterestingDataHasChanged)
                return;
            // If there is no uninteresing data, same though but with DataHasChanged
            if (!BusinessValues.UninterestingPaths.TryGetValue(GetType(), out string[][] keyPaths))
            {
                InterestingDataHasChanged = DataHasChanged;
                return;
            }

            // The simplest way is just to clone and remove uninteresting properties.
            // If it cause performance problem we could also remove uninteresting properties form origingal json object,
            // compare, and add them back by calling cancel actions.
            // This is possible because the order of properties does not matter in memory.
            // They are ordered at saving time anyway.
            // Actually, let's do this...
            //var storageClone = Storage.DeepClone();
            //var newStorageClone = newStorage.DeepClone();
            var cancels = new List<Action>(keyPaths.Length * 2);
            try
            {
                foreach (var keyPath in keyPaths)
                {
                    cancels.Add(RemovePropertyAndGetCancel(Storage, keyPath));
                    cancels.Add(RemovePropertyAndGetCancel(newStorage, keyPath));
                }
                InterestingDataHasChanged = !JToken.DeepEquals(_Storage, newStorage);
            }
            finally
            {
                foreach (var cancel in cancels)
                    cancel();
            }
        }
        Action RemovePropertyAndGetCancel(JToken storage, object[] keys)
        {
            if (keys.Length == 0)
                throw new ArgumentException("Key path cannot be empty!", nameof(keys));
            var token = JsonUtils.Instance.SelectToken(storage, false, keys);
            // if the value does not exist at all in json we have nothing to do
            // By "does not exist" I mean not even explicitely (which coudl be tested with value.Type == JTokenType.Null)
            if (token == null)
                return Action_Extensions.DoNothing;
            var link = token.Parent; // get the link, often a jproperty (... always ?)
            var container = link.Parent;
            var key = keys.Last(); // Fix the value, we do not know which code acces the array reference after all.
            link.Remove();
            return () => container[key] = token; // recreate "link"
        }

        public void CopyAllFieldsFrom(DtoObject source)
        {
            Storage = (JObject)source.Storage.DeepClone();
        }


        internal string ToAlphabeticalOrderedJsonString()
        {
            var json = JsonConvert.SerializeObject(JsonUtils.Instance.NormalizeToken(_Storage), Formatting.Indented);
            return json;
        }

        
        public void NormalizeStorage()
        {
            _Storage = (JObject)JsonUtils.Instance.NormalizeToken(_Storage);
        }

        //public class OrderedContractResolver : DefaultContractResolver
        //{
        //    protected override IList<JsonProperty> CreateProperties(System.Type type, MemberSerialization memberSerialization)
        //    {
        //        return base.CreateProperties(type, memberSerialization).OrderBy(p => p.PropertyName).ToList();
        //    }
        //}
        string AsDebugString { get { return ToAlphabeticalOrderedJsonString(); } }
    }
}
