﻿using System;

using Newtonsoft.Json.Linq;


namespace JiraManager.Business.API.DtoObjects
{
    public interface IHasJsonStorage
    {
        JObject Storage { get; set; }
        void NormalizeStorage();
    }
}
