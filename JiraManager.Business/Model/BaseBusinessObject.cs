﻿using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;


namespace JiraManager.Business.API.DtoObjects
{
    // Classe de base pour objets servant de DTO, afin de fournir des méthodes de check
    public abstract partial class BaseBusinessObject : IHasJsonStorage, IHasTypedIdReadable, ICloneable, ICopyable<BaseBusinessObject>
    {
        #region technical stuff 

        [IsTechnicalProperty]
        [Browsable(false)]
        public IIdTuple IdTuple => GetIdTuple(); protected abstract IIdTuple GetIdTuple();
        [IsTechnicalProperty]
        [Browsable(false)]
        public ITypedId TypedId => GetIdTuple().ToTypedId(GetType());
        ITypedId IHasTypedIdReadable.MakeTypedId(IIdTuple id) { return id.ToTypedId(GetType()); }

        protected BaseBusinessObject()
        {
            if (DataMapper.BaseDTO.TrackInstances)
                ObjectTracker.Instance.Track(this);
        }
        readonly DtoObject _dto = new DtoObject();

        JObject IHasJsonStorage.Storage { get { return Storage; } set { Storage = value; } }

        #region Cloneable (Implementation de ICloneable permettant de gérer l'heritage plus tard)

        object ICloneable.Clone()
        {
            return Clone();
        }
        public BaseBusinessObject Clone()
        {
            var clone = CreateNewInstance();
            clone.CopyAllFieldsFrom(this);
            return clone;
        }
        protected abstract BaseBusinessObject CreateNewInstance();
        public virtual void CopyAllFieldsFrom(BaseBusinessObject source)
        {
            _dto.CopyAllFieldsFrom(source._dto);
        }

        void ICopyable.CopyFrom(ICopyable source) { CopyAllFieldsFrom((BaseBusinessObject)source); }
        void ICopyable<BaseBusinessObject>.CopyFrom(BaseBusinessObject source) { CopyAllFieldsFrom(source); }

        #endregion

        #endregion technical stuff 


        [IsTechnicalProperty][Browsable(false)]
        public virtual JObject Storage        { get { return _dto.Storage; } internal set { _dto.Storage = value; } }

        [IsTechnicalProperty][Browsable(false)]
        public bool            DataHasChanged { get { return _dto.DataHasChanged; } internal set { _dto.DataHasChanged = value; } }
        [IsTechnicalProperty][Browsable(false)]
        public bool InterestingDataHasChanged { get { return _dto.InterestingDataHasChanged; } internal set { _dto.InterestingDataHasChanged = value; } }

        protected internal virtual T? GetValue<T>(params object[] keys)
            where T : struct
        {
            return _dto.GetValue<T>(keys);
        }
        /// <summary> Same But for returning object (string or JToken) </summary>
        protected internal virtual string GetValue(params object[] keys)
        {
            return _dto.GetValue(keys);
        }

        protected internal virtual void SetValue<T>(T value, params object[] keys)
        {
            _dto.SetValue<T>(value, keys);
        }

        internal string ToAlphabeticalOrderedJsonString()
        {
            return _dto.ToAlphabeticalOrderedJsonString();
        }
        public void NormalizeStorage()
        {
            _dto.NormalizeStorage();
        }

        public int CompareTo(BaseBusinessObject other) { return ObjectComparisonHelper.CompareIfOverridingToString(this, other); }
    }
}
