﻿using System;
using System.Collections.Generic;

using ApplicationBase.Deployment.Data;


namespace JiraManager.Business
{
    public class ReleaseNotifier : ApplicationBase.Business.ReleaseNotifier
    {
        protected internal ReleaseNotifier(ApplicationBase.Deployment.Config config, bool canSeeTechnicalRelease)
            : base(config, canSeeTechnicalRelease)
        {
        }
        protected override List<ReleaseNoteWithContent> Load(ApplicationBase.Business.ConfigOfUser userConfig)
        {
            var notes = base.Load(userConfig);
            notes.Clear();  // JiraManager does not store its own data in a database so no need to have a release note about Disconnected mode
            return notes;
        }
    }
}
