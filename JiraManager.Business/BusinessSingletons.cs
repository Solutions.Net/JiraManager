using System;
using System.Linq;

using TechnicalTools;

using Base = ApplicationBase.Business;

using JiraManager.Business.API;


namespace JiraManager.Business
{
    public class BusinessSingletons : Base.BusinessSingletons
    {
        // Please See "Note 01" in file "ApplicationBase.Common/Code Design Notes.txt"
        public new static BusinessSingletons Instance
        {
            get { return _Instance; }
            set
            {
                _Instance = value;
                if (Base.BusinessSingletons.Instance == null ||
                    value != null && Base.BusinessSingletons.Instance.GetType().IsAssignableFrom(value.GetType()))
                    Base.BusinessSingletons.Instance = value;
            }
        }
        static BusinessSingletons _Instance;
        static BusinessSingletons()
        {
            Instance = new BusinessSingletons();
        }
        protected BusinessSingletons()
        {
            OnObjectExistsDo<Repositories.CachedRepository>(repo => repo.RecommendedInit());
        }

        public    new                   BusinessFactory  GetBusinessFactory()    { return (BusinessFactory)base.GetBusinessFactory(); }
        protected override         Base.BusinessFactory  CreateBusinessFactory() { return new BusinessFactory(); }

        public    new                   ConfigOfUser     GetConfigOfUser()       { return (ConfigOfUser)base.GetConfigOfUser(); } 
 
        public    virtual  Repositories.CachedRepository GetRepository()                 { return GetOrAdd(() => GetBusinessFactory().CreateCachedRepository(GetDiskRepository())); } 
        public    virtual  Repositories.DiskRepository   GetDiskRepository()             { return GetOrAdd(() => GetBusinessFactory().CreateDiskRepository(GetRecommendedLocalDataFolder(), GetJiraRepository())); } 
        public    virtual  Repositories.JiraRepository   GetJiraRepository()             { return GetOrAdd(() => GetBusinessFactory().CreateJiraRepository()); } 
        public    virtual               JiraAPI          GetJiraRestClient()             { return GetOrAdd(() => GetBusinessFactory().CreateJiraRestClient(GetConfigOfUser().Yield().Cast<ConfigOfUser>().Select(cfg => new API.Config() { ApiUrl = cfg.JiraApiUrl, ApiKey = cfg.JiraApiKey, EmailAccount = cfg.JiraEmailAccount, CompanyCookies = cfg.GetCompanyCookies() }).Single())); } 
    }
}

