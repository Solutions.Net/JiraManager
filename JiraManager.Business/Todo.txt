﻿- Verifier que Selecttoken etc soit case insensitive dans DtObject
- faire en sorte que ne soit pas serializé les object nested (exemple : "parent")
- Afficher des containers pour les taches
- gerer les links
- Quand un utilisateur raffraichit et l'a fait il y a moins de 2 mois, proposer d'ouvrir une fenetre montrant tous les nouveaux objets

- https://stackoverflow.com/a/59106797/294998