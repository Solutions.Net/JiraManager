﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business.DataRelation
{
    public partial class HierarchyProvider : GenericHierarchyGraphProvider
    {
        public static HierarchyProvider Instance { get; } = new HierarchyProvider();

        class TypeSizeEstimatorIgnoringJsonField : TypeSizeEstimator
        {
            public TypeSizeEstimatorIgnoringJsonField()
            {
                AddRuleForEmptySize((t, _) => typeof(DtoObject).IsAssignableFrom(t)
                                            ? 0
                                            : (int?)null);
                AddRuleForEmptySize((t, _) => typeof(API.JiraAPI).IsAssignableFrom(t)
                                            ? 0
                                            : (int?)null);
            }
        }

        private HierarchyProvider()
            : base(new TypeSizeEstimatorIgnoringJsonField())
        {
            AddConstructor(_ => BuildCachedHierarchy("Components", (IEnumerable<Project> projects) =>
            {
                var components = projects.ToDictionary(p => p, p => p.GetComponents(_dummyProgress));
                return components;
            }, 50));

            AddConstructor(_ => BuildCachedHierarchy("Issues", (IEnumerable<ProjectComponent> projectComponents) =>
            {
                var issues = projectComponents.ToDictionary(p => p, p => p.GetIssues(_dummyProgress));
                return issues;
            }, 1000));

            AddConstructor(_ => BuildCachedHierarchy("Project Component", (IEnumerable<Issue> issues) =>
            {
                var repo = BusinessSingletons.Instance.GetRepository();
                var comp = repo.Components.GroupByToDictionary2(c => c.ProjectKey, c => c.Name);
                var empty = new List<ProjectComponent>();
                var dic = issues.Distinct().ToDictionary(issue => issue, issue => comp.TryGetValueClass(issue.ProjectKey)?.TryGetValueClass(issue.ComponentName) ?? empty);
                return dic;
            }, 1));
        }
        readonly IProgress<string> _dummyProgress = new Progress<string>();
    }

}
