using System;

using TechnicalTools.Model;


namespace JiraManager.Business.Automation
{
    public abstract class JMCommandLineTask<TConfig> : ApplicationBase.Business.Automation.CommandLineTask
        where TConfig : Business.Config
    {
        protected new TConfig Cfg { get { return (TConfig)base.Cfg; } }

        protected JMCommandLineTask(TConfig cfg, DateTime atDate)
            : base(cfg, atDate)
        {
            if (Cfg?.IsLoaded ?? false)
            {
                if (!string.IsNullOrWhiteSpace(cfg.Automation.Bot_Login) ||
                    !string.IsNullOrWhiteSpace(cfg.Automation.Bot_Password))
                    throw new TechnicalException("Automation account is not valid!", null);
            }
        }
    }
}

