﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business
{
    public class SetSnapshot<T> : List<T>, IUserInteractiveObject
        where T : BaseBusinessObject
    {
        public SetSnapshot(Repositories.CachedRepository repo, IEnumerable<object> objects = null)
            : base((objects ?? Enumerable.Empty<object>()).Cast<T>())
        {
            _repo = repo;
        }
        readonly Repositories.CachedRepository _repo;

        public void Refresh(IProgress<string> pr)
        {
            var issues = _repo.GetListFromMemory<T>(pr);
            this.EnsureCapacity(issues.Count);
            Clear();
            AddRange(issues);
        }
    }
}
