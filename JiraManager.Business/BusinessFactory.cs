﻿using System;
using System.IO;

using Base = ApplicationBase.Business;


namespace JiraManager.Business
{
    public class BusinessFactory : Base.BusinessFactory
    {
        public BusinessFactory()
        {
        }

        public override              string           CreateRecommendedLocalDataFolder(string wishedFolderName)                                 { return base.CreateRecommendedLocalDataFolder(wishedFolderName ?? "db"); }

        public override         Base.ConfigOfUser     CreateConfigOfUser(Base.AuthenticationManager m, Base.ConfigOfUser.StorageTraits traits)  { return new ConfigOfUser(m, traits ?? new Base.ConfigOfUser.StorageLocalFile(Path.Combine(BusinessSingletons.Instance.GetRecommendedLocalDataFolder(), Base.ConfigOfUser.StorageLocalFile.DefaultRepositoryFileName))); }

        public override         Base.ReleaseNotifier  CreateReleaseNotifier(ApplicationBase.Deployment.Config c, bool canSeeTechnicalRelease)   { return new ReleaseNotifier(c, canSeeTechnicalRelease); }

        public virtual  Repositories.CachedRepository CreateCachedRepository(Repositories.DiskRepository diskRepo)                              { return new Repositories.CachedRepository(diskRepo); }
        public virtual  Repositories.DiskRepository   CreateDiskRepository(string folder, Repositories.JiraRepository repo)                     { return new Repositories.DiskRepository(folder, repo); }
        public virtual  Repositories.JiraRepository   CreateJiraRepository()                                                                    { return new Repositories.JiraRepository(BusinessValues.UselessData, BusinessValues.GetRedondantData()); }

        public virtual           API.JiraAPI          CreateJiraRestClient(API.Config cfg)                                                      { return new API.JiraAPI(cfg); }
    }
}
