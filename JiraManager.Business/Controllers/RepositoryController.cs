﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools.Model;

using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.Repositories;


namespace JiraManager.Business.Controllers
{
    public class RepositoryController : IUserInteractiveObject
    {
        public RepositoryController(CachedRepository repository)
        {
            _repository = repository;
        }
        readonly CachedRepository _repository;

        public List<ProjectComponent> GetProjectComponents()
        {
            var components = _repository.Components.OrderBy(c => c.Project.Key).ThenBy(c => c.Name).ToList();

            // Complete with fictive projectcomponent instance when project have no one.
            var projectsAlone = _repository.Projects.Except(_repository.Components.Select(c => c.Project)).ToList();
            foreach (var projectAlone in projectsAlone)
            {
                var fpComp = _repository.CreateFictiveProjectComponent(projectAlone);
                components.Add(fpComp);
            }
            return components;
        }
    }
}
