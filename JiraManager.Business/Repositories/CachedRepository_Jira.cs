﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business.Repositories
{
    public partial class CachedRepository : IJiraRepository
    {
        public IReadOnlyCollection<ServerInfo>        ServerInfos      { get { return GetJiraObjects<ServerInfo>(); } }
        public IReadOnlyCollection<Project>           Projects         { get { return GetJiraObjects<Project>(); } }
        public IReadOnlyCollection<ProjectComponent>  Components       { get { return GetJiraObjects<ProjectComponent>(); } }
        public IReadOnlyCollection<User>              Users            { get { return GetJiraObjects<User>(); } }
        public IReadOnlyCollection<Issue>             Issues           { get { return GetJiraObjects<Issue>(); } }
        public IReadOnlyCollection<IssueType>         IssueTypes       { get { return GetJiraObjects<IssueType>(); } }
        public IReadOnlyCollection<Status>            Statuses         { get { return GetJiraObjects<Status>(); } }
        public IReadOnlyCollection<StatusCategory>    StatusCategories { get { return GetJiraObjects<StatusCategory>(); } }
        public IReadOnlyCollection<Resolution>        Resolutions      { get { return GetJiraObjects<Resolution>(); } }
        public IReadOnlyCollection<Priority>          Priorities       { get { return GetJiraObjects<Priority>(); } }
        public IReadOnlyCollection<IssueLink>         IssueLinks       { get { return GetJiraObjects<IssueLink>(); } }
        public IReadOnlyCollection<IssueLinkType>     IssueLinkTypes   { get { return GetJiraObjects<IssueLinkType>(); } }


        public Project          GetProject         (long   id) { return GetJiraObject<Project>(new IdTuple<long>(id));}
        public ProjectComponent GetProjectComponent(long   id) { return GetJiraObject<ProjectComponent>(new IdTuple<long>(id)); }
        public User             GetUser            (string id) { return GetJiraObject<User>(new IdTuple<string>(id)); }
        public Issue            GetIssue           (long   id) { return GetJiraObject<Issue>(new IdTuple<long>(id)); }
        public IssueType        GetIssueType       (long   id) { return GetJiraObject<IssueType>(new IdTuple<long>(id)); }
        public Status           GetStatus          (long   id) { return GetJiraObject<Status>(new IdTuple<long>(id)); }
        public StatusCategory   GetStatusCategory  (long   id) { return GetJiraObject<StatusCategory>(new IdTuple<long>(id)); }
        public Resolution       GetResolution      (long   id) { return GetJiraObject<Resolution>(new IdTuple<long>(id)); }
        public Priority         GetPriority        (long   id) { return GetJiraObject<Priority>(new IdTuple<long>(id)); }
        public IssueLink        GetIssueLink       (long   id) { return GetJiraObject<IssueLink>(new IdTuple<long>(id)); }
        public IssueLinkType    GetIssueLinkType   (long   id) { return GetJiraObject<IssueLinkType>(new IdTuple<long>(id)); }


        public IReadOnlyCollection<T> GetJiraObjects<T>(IProgress<string> pr = null)
            where T : JiraObject
        {
            return GetListFromMemory<T>(pr);
        }

        public T GetJiraObject<T>(IIdTuple id)
            where T : JiraObject
        {
            return GetObjectFromMemory(id, _disk.GetJiraObject<T>);
        }
        
        /// <see cref="IJiraRepository.Refresh(IReadOnlyCollection{JiraObject}, IProgress{string})"/>
        public IEnumerable<JiraObject> Refresh(IReadOnlyCollection<JiraObject> objects = null, IProgress<string> pr = null)
        {
            objects = objects ?? _objects.Values.Where(tl => typeof(JiraObject).IsAssignableFrom(tl.Type))
                                                .SelectMany(tl => tl.ByIds.Values)
                                                .Cast<JiraObject>()
                                                .ToList();
            foreach (var obj in _disk.Refresh(objects, pr))
            {
                RefreshOrAdd(obj, true);
                yield return obj;
            }
        }

        /// <see cref="IJiraRepository.RefreshAll(DateTime?, IProgress{string})"/>
        public IEnumerable<JiraObject> RefreshAll(DateTime? lastfullRefreshDate = null, IProgress<string> pr = null)
        {
            lastfullRefreshDate = lastfullRefreshDate ?? ServerInfos.SingleOrDefault()?.ServerTime;
            foreach (var obj in _disk.RefreshAll(lastfullRefreshDate, pr))
            {
                RefreshOrAdd(obj, true);
                yield return obj;
            }
        }

        public event EventHandler Refreshed
        {
            add    { _disk.Refreshed += value; }
            remove { _disk.Refreshed -= value; }
        }

        /// <see cref="IJiraRepository.PushEdits(IReadOnlyCollection{JiraObject}, eCacheType, IProgress{string})"/>
        public IReadOnlyCollection<JiraObject> PushEdits(IReadOnlyCollection<JiraObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null)
        {
            if (maxDepth < eCacheType.Memory)
                return null; // means "we don't know which objects needed to be changed"
            objects = objects ?? new JiraObject[0];
            if (maxDepth >= eCacheType.Source)
            {
                throw new NotImplementedException();
                //foreach (var obj in objects)
                //{
                //    // TODO : Put "Edits" in "Storage" 
                //}
            }
            objects = _disk.PushEdits(objects, maxDepth, pr)
                    ?? objects;
            // RefreshOrAdd does not filter for the object that need to be add to cache
            // But this is not mandatory
            var lst = objects.Select(obj => RefreshOrAdd(obj, false)).ToList();
            return lst;
        }

        #region Supplement

        public Project GetProject(string key)
        {
            if (key == null)
                return null;
            var wasEmpty = _projectByKeys == null;
            _projectByKeys = _projectByKeys ?? Projects.ToDictionary(p => p.Key, p => p.Id);
            var projectId = _projectByKeys.TryGetValueStruct(key);
            if (projectId != null)
                return GetProject(projectId.Value);
            if (wasEmpty)
            {
                BusEvents.Instance.RaiseBusinessWarning($"Key {key} does not reference any project!", _log);
                return null;
            }
            _projectByKeys = null; // force a refresh
            return GetProject(key); // recursive call
        }
        Dictionary<string, long> _projectByKeys;

        public ProjectComponent GetComponent(string projectKey, string componentKey)
        {
            if (string.IsNullOrWhiteSpace(componentKey))
                return null;
            var wasEmpty = _projectComponentByKeys == null;
            _projectComponentByKeys = _projectComponentByKeys ?? Components.ToDictionary(c => (c.ProjectKey, c.Name), c => c.Id);
            var componentId = _projectComponentByKeys.TryGetValueStruct((projectKey, componentKey));
            if (componentId != null)
                return GetProjectComponent(componentId.Value);
            if (wasEmpty)
            {
                if (!_projectComponentByKeysErrors.Contains((projectKey, componentKey)))
                {
                    _projectComponentByKeysErrors.Add((projectKey, componentKey));
                    BusEvents.Instance.RaiseBusinessWarning($"Key ({projectKey}, {componentKey}) does not reference any project's component!", _log);
                }
                return null;
            }
            _projectComponentByKeys = null; // Force a refresh
            return GetComponent(projectKey, componentKey); // recursive call
        }
        Dictionary<(string, string), long> _projectComponentByKeys; // keys are project name & component name
        readonly HashSet<(string, string)> _projectComponentByKeysErrors = new HashSet<(string, string)>(); // keys are project name & component name

        public Issue GetIssue(string key)
        {
            if (key == null)
                return null;
            var tl = _objects.TryGetValueClass(typeof(Issue));
            Issue issue;
            if (tl != null)
            {
                issue = tl.ByIds.Values.Cast<Issue>().FirstOrDefault(i => i.Key == key);
                if (issue != null)
                    return issue;
            }
            issue = _disk.GetIssue(key);
            if (issue == null)
                BusEvents.Instance.RaiseBusinessWarning($"Key {key} does not reference any issue!", _log);
            else
                RefreshOrAdd(issue, false);
            return issue;
        }

        #endregion Supplement
    }
}
