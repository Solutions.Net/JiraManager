﻿using System;
using System.Collections.Generic;

using TechnicalTools.Model.Cache;

using JiraManager.Business.ObjectExtensions;


namespace JiraManager.Business.Repositories
{
    public interface ILocalRepository
    {
        IReadOnlyCollection<IssueLocalData> IssueViews { get; }

        ServerLocalData GetServerData(string id, bool create = true);
        IssueLocalData  GetIssueData(long id, bool create = true);

                            T  GetOrCreateLocalObject<T>(ITypedId tid, bool create = true) where T : ExtensionObject;
        IReadOnlyCollection<T> GetLocalList  <T>(IProgress<string> pr = null) where T : ExtensionObject;


        //T AddLocalObject<T>(T obj) where T : ExtensionObject, IHasTypedIdReadable;

        /// <summary>
        /// Resync objects using the source repository (or service)
        /// If objects is null all objects currently in repository will be resync.
        /// This method does not make possible to get new objects.
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="pr"></param>
        /// <returns>The objects refreshed.</returns>
        IEnumerable<ExtensionObject> Refresh(IReadOnlyCollection<ExtensionObject> objects = null, IProgress<string> pr = null);

        /// <summary>
        /// Push all edits of all objects in "objects" to the underlying storage (or remote service).
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="pr"></param>
        /// <returns>objects that needed to be pushed</returns>
        IReadOnlyCollection<ExtensionObject> PushEdits(IReadOnlyCollection<ExtensionObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null);
    }
}
