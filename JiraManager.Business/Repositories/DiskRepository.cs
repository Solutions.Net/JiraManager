﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using LibGit2Sharp;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model.Cache;

using JiraManager.Common;
using JiraManager.Business.API;
using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.ObjectExtensions;


namespace JiraManager.Business.Repositories
{
    /// <summary>
    /// Long persistance cache(Disk : using Git)
    /// </summary>
    // https://stackoverflow.com/questions/122362/how-to-empty-flush-windows-read-disk-cache-in-c
    public partial class DiskRepository : IRepository
    {
        public string DbFolder { get; }

        internal DiskRepository(string dbFolder, JiraRepository repo)
        {
            if (string.IsNullOrWhiteSpace(dbFolder))
                throw new ArgumentException("Cannot be empty", nameof(dbFolder));
            dbFolder = dbFolder.Trim();
            if (dbFolder[dbFolder.Length - 1] != Path.DirectorySeparatorChar)
                dbFolder += Path.DirectorySeparatorChar;
            DbFolder = dbFolder;
            _repo = repo;
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(CachedRepository));
        readonly JiraRepository _repo;
        
        /// <summary>
        /// Save all modified objects on disk
        /// </summary>
        /// <returns>The number of business objects that have been finally written to disk (ie: were truly modified).
        /// ServerInfo does not count as business object because they changed everytime because of updated clock)</returns>
        int WriteListOnDisk(IEnumerable<BaseBusinessObject> objects, IProgress<string> pr)
        {
            var objectsByGroups = objects.GroupByToDictionary(obj => obj.TypedId.Type);
            if (objectsByGroups.Count == 0)
                return 0;
            int uninterestingFileCount = 0;
            int interestingFileCount = 0;
            int? serverInfosCount = null;
            using (var repo = GetRepository())
            {
                CommitUnexpectedUserEdits(repo, pr);
                string state = "";
                // make ServerInfo the last type of object to save because it contains the last update time
                foreach (var kvp in objectsByGroups.OrderBy(kvp => kvp.Key == typeof(ServerInfo))) 
                {
                    if (kvp.Key == typeof(ServerInfo))
                        serverInfosCount = kvp.Value.Count;
                    var objToWrite = kvp.Value.Where(obj => obj.DataHasChanged).ToList();
                    int objIndex = 0;
                    
                    foreach (var obj in objToWrite)
                    {
                        ++objIndex;
                        pr.Report("Saving " + kvp.Key.Name + obj.IdTuple.ToString() + $" ({objIndex} / {objToWrite.Count})");
                        var isInteresting = obj.InterestingDataHasChanged; // capture before WriteOnDisk set it to false
                        if (isInteresting)
                        {
                            var old = GetObjectFromDisk(obj.GetType(), obj.IdTuple);
                            // new object, 
                            // or maybe an old one but not visible before 
                            // (ex: zendesk => jira escalation...
                            // I do not know how to check this currently except that i see that the created date date is very old)
                            if (old != null) 
                            {
                                JsonUtils.Instance.CopyUninterestingDataTo(obj.Storage, old.Storage);
                                uninterestingFileCount += WriteObjectOnDisk(old) ? 1 : 0;
                                Commands.Stage(repo, ObjectToLocalFilePath(obj).Substring(DbFolder.Length));
                            }
                        }
                        interestingFileCount += WriteObjectOnDisk(obj) ? 1 : 0;
                    }
                    state += "Saved " + objToWrite.Count + " " + kvp.Key.Name + " objects." + Environment.NewLine;
                    pr.Report(state);
                }
                if (uninterestingFileCount > 0)
                {
                    pr.Report(state + "Committing uninteresting data...");
                    ExceptionManager.Instance.IgnoreException<EmptyCommitException>(() => repo.Commit("Uninteresting data changes", _signature, _signature));
                }
                if (interestingFileCount > 0)
                {
                    pr.Report(state + "Staging...");
                    Commands.Stage(repo, "*");
                    pr.Report(state + "Committing interesting data...");
                    ExceptionManager.Instance.IgnoreException<EmptyCommitException>(() => repo.Commit("Interesting data changes", _signature, _signature));
                }
            }
            // We do not count item "ServerInfo" because it is written each time a refresh happens
            // ServerInfo may be not loaded (serverInfosCount = 0)
            return Math.Max(0, interestingFileCount - (serverInfosCount ?? 0)); // max in case we already saved and nothing has changed
        }

        bool WriteObjectOnDisk(BaseBusinessObject obj)
        {
            var objectFolder = Path.Combine(DbFolder, obj.GetType().FullName);
            Directory.CreateDirectory(objectFolder);
            var newFilename = Path.Combine(objectFolder, ObjectToFilename(obj));
            var content = obj is ExtensionObject 
                        ? obj.ToAlphabeticalOrderedJsonString()
                        : obj.Storage.ToString(); // Already done by JiraRepository
            if (File.Exists(newFilename) && File.ReadAllText(newFilename) == content)
                return false;

            #region This code is only useful for type "Issue"
            // Issue can be "moved" on atlassian website so their key is modified but not their Id
            // We have to detect this on writing so the commit are clean (file is seens as renamed)
            // and all the files have distinct ids
            var prefix = IdToMinimalFilename(obj.IdTuple);
            foreach (var filename in Directory.EnumerateFiles(objectFolder, prefix + "*"))
                if (Path.GetFileName(filename) == prefix || // new object with negative id and no text (no EmDash)
                    Path.GetFileName(filename).StartsWith(prefix + EmDash))
                    if (filename != newFilename)
                        File.Delete(filename);
            #endregion

            File.WriteAllText(newFilename, content);
            obj.DataHasChanged = false;
            obj.InterestingDataHasChanged = false;
            _exitsOnDisk[obj.TypedId] = true;
            return true;
        }

        
        string ObjectToLocalFilePath(BaseBusinessObject obj)
        {
            return Path.Combine(DbFolder, obj.GetType().FullName, ObjectToFilename(obj));
        }

        string ObjectToFilename(BaseBusinessObject obj)
        {
            return IdToMinimalFilename(obj.IdTuple) 
                 + ("" + EmDash).AsPrefixForIfNotBlank(ObjectToFilenameSuffix(obj));
        }
        static char EmDash { get; } = '—'; // U+FF5C

        // @dev: This method must not return character (U+FF5C) neither
        string IdToMinimalFilename(IIdTuple iid)
        {
            var id = IdTuple.Serialize(iid, "" + FullWidthVerticalLine);
            id = id.Replace(":", ";"); // For User object because field name <evaluate User.UniqueIdJsonKey()> contains ':' (not a guid ? wtf !) 
            return id;
        }
        static char FullWidthVerticalLine { get; } = '｜'; // U+FF5C
        
        // @dev: let's just hope this method does not return invalid characters for path.
        // This method must not return character (U+FF5C) neither
        string ObjectToFilenameSuffix(BaseBusinessObject obj)
        {
            if (obj is Project p)
                return p.Key; // Project key pattern can be modified by jira admin but they are by default [A-Z][A-Za-z0-9]+ and it is recommended to not change it.
            else if (obj is ProjectComponent c)
                return c.Name.Replace(":", ";"); // Project key pattern can be modified by jira admin but they are by default [A-Z][A-Za-z0-9]+ and it is recommended to not change it. 
            else if (obj is Issue issue)
                return issue.Key; // Project key pattern can be modified by jira admin but they are by default [A-Z][A-Za-z0-9]+ and it is recommended to not change it.
            else if (obj is User user)
                return user.DisplayName; // Shoudl not contains invalid character, i am mean i guess :S
            return string.Empty;
        }

        internal void CommitUnexpectedUserEdits(IProgress<string> pr = null)
        {
            CommitUnexpectedUserEdits(GetRepository(), pr);
        }
        void CommitUnexpectedUserEdits(Repository repo, IProgress<string> pr = null)
        {
            pr = pr ?? ProgressEx<string>.Default;
            pr.Report("Checking db state");
            ////bool isDirty = repo.Diff.Compare<TreeChanges>().Count > 0;
            //var status = repo.RetrieveStatus();
            //if (status.IsDirty)
            //{
                pr.Report("Saving unexpected user edits");
                var msg = "User Settings edits";
                ExceptionManager.Instance.IgnoreException<EmptyCommitException>(() => repo.Commit(msg + " (was already staged)", _signature, _signature));
                //if (status.IsDirty)
                //{
                    Commands.Stage(repo, "*"); // Does not throw if there is nothing to stage
                    ExceptionManager.Instance.IgnoreException<EmptyCommitException>(() => repo.Commit(msg, _signature, _signature));
                //}
            //}
        }

        IReadOnlyCollection<T> ReadAllFromDisk<T>(IProgress<string> pr = null)
            where T : BaseBusinessObject
        {
            return (IReadOnlyCollection<T>)ReadAllFromDisk(typeof(T).WrapInArray(), pr).Single().objects;
        }
        IEnumerable<(Type type, IReadOnlyCollection<BaseBusinessObject> objects)> ReadAllFromDisk(Type[] types, IProgress<string> pr = null)
        {
            pr = pr ?? ProgressEx<string>.Default;
            var typeLoaded = new HashSet<Type>();
            foreach ((Type type, string objectFolder) it in BrowseTypeFolders())
            {
                if (types == null || types.Contains(it.type))
                {
                    var objects = GetListFromDisk(it.type, pr);
                    typeLoaded.Add(it.type);
                    yield return (it.type, objects);
                }
            }
            foreach (var type in types)
                if (!typeLoaded.Contains(type))
                    yield return (type, new BaseBusinessObject[0]);
        }

        IEnumerable<(Type type, string folder)> BrowseTypeFolders()
        {
            foreach (var objectFolder in Directory.EnumerateDirectories(DbFolder))
            {
                if (objectFolder.EndsWith("\\.git"))
                    continue;
                var type = Type.GetType(Path.GetFileName(objectFolder));
                yield return (type, objectFolder);
            }
        }

        Repository GetRepository()
        {
            var needToBeCreated = !Directory.Exists(Path.Combine(DbFolder, ".git"));
            if (needToBeCreated)
            {
                Directory.CreateDirectory(DbFolder);
                Repository.Init(DbFolder);
            }
            var repo = new Repository(DbFolder);
            if (needToBeCreated)
            {
                File.WriteAllText(Path.Combine(DbFolder, ".gitattributes"), "* -text");
                Commands.Stage(repo, "*");
                repo.Commit("Initial commit", _signature, _signature);
            }
            return repo;
        }
        static readonly Signature _signature = new Signature(BootstrapConfig.Instance.ApplicationName, Deployer.GetCurrentVersion().ToString() + "@" + BootstrapConfig.Instance.ApplicationName, DateTimeOffset.Now);

        internal IReadOnlyCollection<T> GetListFromDisk<T>(IProgress<string> pr = null) where T : BaseBusinessObject, IHasClosedIdReadable { return (IReadOnlyCollection<T>)GetListFromDisk(typeof(T), pr); }
        IReadOnlyCollection<BaseBusinessObject> GetListFromDisk(Type type, IProgress<string> pr)
        {
            pr = pr ?? ProgressEx<string>.Default;
            Debug.Assert(typeof(BaseBusinessObject).IsAssignableFrom(type));
            var objectFolder = Path.Combine(DbFolder, type.FullName);
            var typeName = type.FullName.Split('.').Last();
            if (!Directory.Exists(objectFolder))
                return (IReadOnlyCollection<BaseBusinessObject>)typeof(List<>).MakeGenericType(type).GetDefaultConstructor().Invoke(null);
            var filenames = Directory.EnumerateFiles(objectFolder).ToArray();
            int count = 0;
               
            var constructor = RepositoryHelper.GetConstructorFor(type);
            var impl = type.GetOneImplementationOf(typeof(IHasClosedIdReadable<>));
            var iidType = impl.GetProperty(nameof(IHasClosedIdReadable<int>.IdTuple)).PropertyType;

            var userCfg = (ConfigOfUser)BusinessSingletons.Instance.GetConfigOfUser();
            bool isFolderOnSddDrive = userCfg.IsFolderOnSddDrive(objectFolder);
            IReadOnlyCollection<BaseBusinessObject> lst;

            var dt = DateTime.MinValue;
            Func<string, BaseBusinessObject> readItemFromFile = filename =>
            {
                IIdTuple iid = null;
                if (typeof(ExtensionObject).IsAssignableFrom(type))
                {
                    var iidStr = Path.GetFileName(filename).Split(EmDash)[0];
                    iid = IdTuple.Deserialize(iidStr, iidType.GetGenericArguments(), "" + FullWidthVerticalLine);
                }
                var jsonContent = File.ReadAllText(filename);
                Interlocked.Increment(ref count);
                var item = constructor(iid);
                item.Storage = (JObject)JToken.Parse(jsonContent);
                item.DataHasChanged = false;
                item.InterestingDataHasChanged = false;
                var now = DateTime.UtcNow;
                if ((now - dt).TotalSeconds >= 1)
                {
                    dt = now;
                    pr.Report("Loading " + typeName + " " + count + " / " + filenames.Length);
                }
                return item;
            };

            DateTime startPerfDuration = DateTime.UtcNow;
            if (isFolderOnSddDrive)
                lst = (IReadOnlyCollection<BaseBusinessObject>)filenames.Select(filename => Task.Run(() => readItemFromFile(filename)))
                                                                        .ToArray() // Force running all taks before waiting for them
                                                                        .Select(t => t.Result)
                                                                        .ToTypedList(type);
            else
                lst = (IReadOnlyCollection<BaseBusinessObject>)filenames.Select(readItemFromFile).ToTypedList(type);
            var perfDuration = DateTime.UtcNow - startPerfDuration;
            pr.Report("Loading " + typeName + " " + count + " / " + filenames.Length);
            _log.Info($"Loading of {filenames.Length} object of type {typeName} took " + perfDuration);
            return lst;
        }

        T GetObjectFromDisk<T>(IIdTuple iid) where T : BaseBusinessObject { return (T)GetObjectFromDisk(typeof(T), iid); }
            BaseBusinessObject GetObjectFromDisk(Type type, IIdTuple iid)
        {
            var tid = iid.ToTypedId(type);
            if (_exitsOnDisk.TryGetValue(tid, out bool exitsOnDisk) && !exitsOnDisk)
                return null;
            var objectFolder = Path.Combine(DbFolder, type.FullName);
            if (!Directory.Exists(objectFolder))
                return null;
            var prefix = IdToMinimalFilename(iid);
            var filename = Directory.EnumerateFiles(objectFolder, prefix + "*")
                                    .Where(f => Path.GetFileName(f) == prefix  // new object with negative id and no text (no EmDash)
                                             || Path.GetFileName(f).StartsWith(prefix + EmDash))
                                    .SingleOrDefault();
            if (filename == null)
            {
                _exitsOnDisk[tid] = false;
                return null;
            }
            Debug.Assert(prefix == Path.GetFileName(filename).Split(EmDash)[0]); // Not "GetFileNameWithoutExtension", because of rare case when prefix contains "."
            var jsonContent = File.ReadAllText(filename);
            var cons = RepositoryHelper.GetConstructorFor(type);
            var item = cons(iid);
            item.Storage = (JObject)JToken.Parse(jsonContent);
            item.DataHasChanged = false;
            item.InterestingDataHasChanged = false;
            return item;
        }
        readonly ConcurrentDictionary<ITypedId, bool> _exitsOnDisk = new ConcurrentDictionary<ITypedId, bool>();
    }
}
