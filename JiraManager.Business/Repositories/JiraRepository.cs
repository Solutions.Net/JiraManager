﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Newtonsoft.Json.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.API;

namespace JiraManager.Business.Repositories
{
    public class JiraRepository : IJiraRepository
    {
        public JiraRepository(Dictionary<Type, string[]> uselessDataPaths = null, 
                              Dictionary<Type, Dictionary<string, string[]>> nestedObjectPathAndIdField = null)
        {
            _uselessDataPaths = uselessDataPaths;
            _nestedObjectPathAndIdField = nestedObjectPathAndIdField;
        }
        readonly Dictionary<Type, string[]> _uselessDataPaths;
        readonly Dictionary<Type, Dictionary<string, string[]>> _nestedObjectPathAndIdField;
        

        public IReadOnlyCollection<ServerInfo>        ServerInfos      { get { return GetJiraObjects<ServerInfo>(); } }
        public IReadOnlyCollection<Project>           Projects         { get { return GetJiraObjects<Project>(); } }
        public IReadOnlyCollection<ProjectComponent>  Components       { get { return GetJiraObjects<ProjectComponent>(); } }
        public IReadOnlyCollection<User>              Users            { get { return GetJiraObjects<User>(); } }
        public IReadOnlyCollection<Issue>             Issues           { get { return GetJiraObjects<Issue>(); } }
        public IReadOnlyCollection<IssueType>         IssueTypes       { get { return GetJiraObjects<IssueType>(); } }
        public IReadOnlyCollection<Status>            Statuses         { get { return GetJiraObjects<Status>(); } }
        public IReadOnlyCollection<StatusCategory>    StatusCategories { get { return GetJiraObjects<StatusCategory>(); } }
        public IReadOnlyCollection<Resolution>        Resolutions      { get { return GetJiraObjects<Resolution>(); } }
        public IReadOnlyCollection<Priority>          Priorities       { get { return GetJiraObjects<Priority>(); } }
        public IReadOnlyCollection<IssueLink>         IssueLinks       { get { return GetJiraObjects<IssueLink>(); } }
        public IReadOnlyCollection<IssueLinkType>     IssueLinkTypes   { get { return GetJiraObjects<IssueLinkType>(); } }


        public Project          GetProject         (long   id) { return GetJiraObject<Project>(new IdTuple<long>(id)); }
        public ProjectComponent GetProjectComponent(long   id) { return GetJiraObject<ProjectComponent>(new IdTuple<long>(id)); }
        public User             GetUser            (string id) { return GetJiraObject<User>(new IdTuple<string>(id)); }
        public Issue            GetIssue           (long   id) { return GetJiraObject<Issue>(new IdTuple<long>(id)); }
        public Issue            GetIssue           (string key){ return GetJiraObject<Issue>(new IdTuple<string>(key)); }
        public IssueType        GetIssueType       (long   id) { return GetJiraObject<IssueType>(new IdTuple<long>(id)); }
        public Status           GetStatus          (long   id) { return GetJiraObject<Status>(new IdTuple<long>(id)); }
        public StatusCategory   GetStatusCategory  (long   id) { return GetJiraObject<StatusCategory>(new IdTuple<long>(id)); }
        public Resolution       GetResolution      (long   id) { return GetJiraObject<Resolution>(new IdTuple<long>(id)); }
        public Priority         GetPriority        (long   id) { return GetJiraObject<Priority>(new IdTuple<long>(id)); }
        public IssueLink        GetIssueLink       (long   id) { return GetJiraObject<IssueLink>(new IdTuple<long>(id)); }
        public IssueLinkType    GetIssueLinkType   (long   id) { return GetJiraObject<IssueLinkType>(new IdTuple<long>(id)); }


        public IReadOnlyCollection<T> GetJiraObjects  <T>(IProgress<string> pr = null)
            where T : JiraObject
        {
            var client = BusinessSingletons.Instance.GetJiraRestClient();
            if (typeof(T) == typeof(Project))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetProjects());
            if (typeof(T) == typeof(ProjectComponent))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetComponents());
            if (typeof(T) == typeof(User))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetUsers());
            if (typeof(T) == typeof(Issue))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetIssues(pr: pr));
            if (typeof(T) == typeof(IssueType))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetIssueTypes());
            if (typeof(T) == typeof(Status))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetStatuses());
            if (typeof(T) == typeof(StatusCategory))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetStatusCategories());
            if (typeof(T) == typeof(Resolution))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetResolutions());
            if (typeof(T) == typeof(Priority))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetPriorities());
            if (typeof(T) == typeof(IssueLink))
                return (IReadOnlyCollection<T>)CleanItemList(BuildIssueLinks(client.GetIssues(pr: pr)));
            if (typeof(T) == typeof(IssueLinkType))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetIssueLinkTypes());
            if (typeof(T) == typeof(ServerInfo))
                return (IReadOnlyCollection<T>)CleanItemList(client.GetServerInfo().WrapInList());
            throw new NotImplementedException();
        }
        public T GetJiraObject<T>(IIdTuple id)
            where T : JiraObject
        {
            var client = BusinessSingletons.Instance.GetJiraRestClient();
            if (typeof(T) == typeof(Project))
                return CleanItem((T)(object)client.GetProject((long)id.Keys[0]));
            if (typeof(T) == typeof(ProjectComponent))
                return CleanItem((T)(object)client.GetComponent((long)id.Keys[0]));
            if (typeof(T) == typeof(User))
                return CleanItem((T)(object)client.GetUser((string)id.Keys[0]));
            if (typeof(T) == typeof(Issue))
                if (id.Keys[0] is long)
                    return CleanItem((T)(object)client.GetIssue((long)id.Keys[0]));
                else 
                    return CleanItem((T)(object)client.GetIssue((string)id.Keys[0]));
            if (typeof(T) == typeof(IssueType))
                return CleanItem((T)(object)client.GetIssueType((long)id.Keys[0]));
            if (typeof(T) == typeof(Status))
                return CleanItem((T)(object)client.GetStatus((long)id.Keys[0]));
            if (typeof(T) == typeof(StatusCategory))
                return CleanItem((T)(object)client.GetStatusCategory((long)id.Keys[0]));
            if (typeof(T) == typeof(Resolution))
                return CleanItem((T)(object)client.GetResolution((long)id.Keys[0]));
            if (typeof(T) == typeof(Priority))
                return CleanItem((T)(object)client.GetPriority((long)id.Keys[0]));
            if (typeof(T) == typeof(IssueLink))
                return CleanItem((T)(object)client.GetIssueLink((long)id.Keys[0]));
            if (typeof(T) == typeof(IssueLinkType))
                return CleanItem((T)(object)client.GetIssueLinkType((long)id.Keys[0]));
            if (typeof(T) == typeof(ServerInfo))
            {
                var si = client.GetServerInfo();
                if (!si.IdTuple.Equals(id))
                    return null;
                return CleanItem((T)(object)si);
            }
            throw new NotImplementedException();
        }
        T CleanItem<T>(T item)
            where T : JiraObject
        {
            return CleanItemList(item.WrapInArray()).First();
        }
        TCollection CleanItemList<TCollection>(TCollection items)
            where TCollection : IReadOnlyCollection<JiraObject>
        {
            RemoveUselessData(items);
            CleanJsonRedondantData(items);
            foreach (var item in items)
                item.NormalizeStorage();
            return items;
        }
        void RemoveUselessData(IReadOnlyCollection<JiraObject> items)
        {
            if (items.Count == 0 || _uselessDataPaths == null)
                return;
            var type = items.First().TypedId.Type; // Assuming all objects are of the same type
            var pathsAndIds = _uselessDataPaths.TryGetValueClass(type);
            if (pathsAndIds == null)
                return;
            foreach (var item in items)
                foreach (var path in pathsAndIds)
                {
                    var tok = item.Storage.Property(path);
                    if (tok == null)
                        continue;
                    tok.Remove();
                }
        }

        void CleanJsonRedondantData(IReadOnlyCollection<JiraObject> items)
        {
            if (items.Count == 0 || _nestedObjectPathAndIdField == null)
                return;
            var type = items.First().TypedId.Type; // Assuming all objects are of the same type
            var pathsAndIds = _nestedObjectPathAndIdField.TryGetValueClass(type);
            if (pathsAndIds == null)
                return;
            foreach (var item in items)
                foreach (var pathsAndId in pathsAndIds)
                {
                    var path = pathsAndId.Key;
                    var ids = pathsAndId.Value;
                    var tok = item.Storage.SelectToken(path)?.SubstituteByIfEquals(null, Null);
                    if (tok == null)
                        continue;
                    var nestedObjToks = tok is JObject obj ? obj.Yield() 
                                        : (tok as JArray).Cast<JObject>();
                    foreach (var nestedObjTok in nestedObjToks)
                    {
                        if (ids.Length == 1) // Optimized version
                        {
                            var idTok = nestedObjTok.Property(ids[0]);
                            if (idTok == null)
                                throw new TechnicalException($"Expecting to see id \"{ids[0]}\" in nested object!", null);
                            foreach (var prop in nestedObjTok.Properties().ToArray())
                                if (prop != idTok)
                                    prop.Remove();
                        }
                        else
                        {
                            var idToks = ids.Select(id => nestedObjTok.Property(id)).ToArray();
                            if (idToks.Any(idTok => idTok == null))
                                throw new TechnicalException($"Expecting to see all ids {ids.Select(id => "\"" + id + "\"").Join()} in nested object!", null);
                            foreach (var prop in nestedObjTok.Properties().ToArray())
                                if (!idToks.Contains(prop))
                                    prop.Remove();
                        }
                    }
                }
        }
        static readonly JValue Null = JValue.CreateNull();

        /// <see cref="IJiraRepository.Refresh(IReadOnlyCollection{JiraObject}, IProgress{string})"/>
        public IEnumerable<JiraObject> Refresh(IReadOnlyCollection<JiraObject> objects = null, IProgress<string> pr = null)
        {
            throw new NotImplementedException("Too long");
        }

        /// <see cref="IJiraRepository.RefreshAll(DateTime?, IProgress{string})"/>
        public IEnumerable<JiraObject> RefreshAll(DateTime? lastfullRefreshDate = null, IProgress<string> pr = null)
        {
            pr = pr ?? ProgressEx<string>.Default;
            if (lastfullRefreshDate == null)
            {
                pr = pr.WrapReportWith("(This is the first time you refresh, it will take some minutes)");
                lastfullRefreshDate = DateTime.MinValue;
            }
            pr.Report("Refreshing from remote");
            using (var client = BusinessSingletons.Instance.GetJiraRestClient())
            {
                // Always must be the first because contains the date
                var si = CleanItem(client.GetServerInfo());
                foreach (var obj in UpdateInterestingObjects(client, lastfullRefreshDate.Value, pr))
                    yield return obj;
                pr.Report("Finalizing");
                yield return si;
            }
            Refreshed?.Invoke(this, EventArgs.Empty);
        }
        public event EventHandler Refreshed;


        IEnumerable<JiraObject> UpdateInterestingObjects(API.JiraAPI client, DateTime lastRetrieveDate, IProgress<string> pr)
        {
            pr.Report("Getting all projects");
            foreach (var p in Projects)
                yield return p;

            pr.Report("Getting all components");
            foreach (var pc in Components)
                yield return pc;

            var issueTypes = client.GetIssueTypes();
            foreach (var it in CleanItemList(issueTypes))
                yield return it;

            var statuses = client.GetStatuses();
            foreach (var status in CleanItemList(statuses))
                yield return status;

            var statusCategories = client.GetStatusCategories();
            foreach (var statusCategory in CleanItemList(statusCategories))
                yield return statusCategory;

            var resolutions = client.GetResolutions();
            foreach (var resolution in CleanItemList(resolutions))
                yield return resolution;

            var priorities = client.GetPriorities();
            foreach (var priority in CleanItemList(priorities))
                yield return priority;

            var issueLinkTypes = client.GetIssueLinkTypes();
            foreach (var issueLinkType in CleanItemList(issueLinkTypes))
                yield return issueLinkType;

            pr.Report("Getting all issues modified");
            var jqlIssueFilter = "Created > '" + lastRetrieveDate.ToString("yyyy/MM/dd HH:mm") + "'" +
                                 " OR " +
                                 "Updated > '" + lastRetrieveDate.ToString("yyyy/MM/dd HH:mm") + "'";
            if (!BusinessSingletons.Instance.GetConfigOfUser().IssueFilter.IsNullOrWhiteSpace())
                jqlIssueFilter = BusinessSingletons.Instance.GetConfigOfUser().IssueFilter + " AND ( " + jqlIssueFilter + " )";
            var issues = client.GetIssues(jqlIssueFilter, pr: pr)
                               .OrderBy(i => i.Updated ?? i.Created)
                               .ToList();
            var issueLinks = BuildIssueLinks(issues);
            foreach (var issue in CleanItemList(issues))
                yield return issue;
            foreach (var link in CleanItemList(issueLinks))
                yield return link;

        }

        IReadOnlyCollection<IssueLink> BuildIssueLinks(IReadOnlyCollection<Issue> uncleanedIssuesButInACompleteGraph)
        {
            var issueLinksByIds = new Dictionary<long, IssueLink>();
            foreach (var i in uncleanedIssuesButInACompleteGraph)
            {
                var tokLinks = (JArray)JsonUtils.Instance.SelectToken(i.Storage, false, new[] { "fields", "issueLinks" });
                if (tokLinks != null)
                    foreach (JObject tokLink in tokLinks)
                    {
                        long linkId = tokLink["id"].Value<long>();
                        var link = issueLinksByIds.TryGetValueClass(linkId);
                        if (link == null)
                        {
                            // Clone tokLink so the current method can be run on issue on disk without messing up with json tree
                            link = new IssueLink() { Storage = (JObject)tokLink.DeepClone() };
                            // We have to use link.Storage now, not tokLink
                            Debug.Assert(link.Id == linkId);
                            issueLinksByIds.Add(linkId, link);
                            // complete the link
                            var missing = link.Storage["inwardIssue"] == null ? "inwardIssue"
                                        : link.Storage["outwardIssue"] == null ? "outwardIssue"
                                        : throw new InvalidOperationException();
                            link.Storage[missing] = new JObject();
                            link.Storage[missing]["id"] = i.Id;
                            link.Storage[missing]["key"] = i.Key;
                        }
                    }
            }
            var unexpectedIncompleteIssues = issueLinksByIds.Values.Where(link => link.Storage["inwardIssue"] == null
                                                                               || link.Storage["outwardIssue"] == null)
                                                                   .ToList();
            Debug.Assert(unexpectedIncompleteIssues.Count == 0);
            return issueLinksByIds.Values;
        }

        

        /// <see cref="IJiraRepository.PushEdits(IReadOnlyCollection{JiraObject}, IProgress{string})"/>
        public IReadOnlyCollection<JiraObject> PushEdits(IReadOnlyCollection<JiraObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null)
        {
            if (maxDepth < eCacheType.Source)
                return null; // means "we don't know which objects needed to be changed"
            objects = objects ?? new JiraObject[0];
            var lst = new List<JiraObject>();
            foreach (var obj in objects)
                throw new NotImplementedException();
                //if Save / create / delete is needed
                //{
                //   do it
                //   refresh obj internals
                //   add obj to lst
                //}
            return lst;
        }
    }
}
