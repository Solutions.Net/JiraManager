﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.ObjectExtensions;
using System.Threading;

namespace JiraManager.Business.Repositories
{
    public partial class CachedRepository : IRepository, IUserInteractiveObject
    {
        internal CachedRepository(DiskRepository diskRepo)
        {
            _disk = diskRepo;
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(CachedRepository));
        readonly DiskRepository _disk;
        readonly ConcurrentDictionary<Type, TypeList> _objects = new ConcurrentDictionary<Type, TypeList>();

        class TypeList
        {
            public Type Type;
            public bool IsFull;
            public ConcurrentDictionary<IIdTuple, BaseBusinessObject> ByIds;
        }

        internal void RecommendedInit()
        {
            // Preload some basic data
            var pr = new Progress<string>();
            GetListFromMemory<Project>(pr);
            GetListFromMemory<ProjectComponent>(pr);
            GetListFromMemory<User>(pr);
        }

        #region Public features

        public Issue CreateIssue(ProjectComponent component)
        {
            var se = GetOrCreateLocalObject<ServerLocalData>(ServerInfos.Single().TypedId);
            var issue = new Issue() { AllowEdits = true };
            issue.Id = se.NewId();
            issue.Key = component.Project.Key + "-" + issue.Id;
            issue.Component = component;
            issue.IssueType = BusinessValues.Task;
            issue.Status = BusinessValues.ToDo;
            issue.Priority = BusinessValues.Medium;
            issue.AllowEdits = false;
            var issue2 = RefreshOrAdd(issue, false);
            Debug.Assert(ReferenceEquals(issue, issue2));
            var data = GetOrCreateLocalObject<IssueLocalData>(issue.TypedId);
            data.Summary = "New " + (-issue.Id).ToStringInvariant();
            return issue;
        }

        public ProjectComponent CreateFictiveProjectComponent(Project project)
        {
            var pComp = new ProjectComponent() { AllowEdits = true };
            pComp.Id = Interlocked.Increment(ref _fictiveProjectComponentIdSeed);
            pComp.ProjectKey = project.Key;
            pComp.AllowEdits = false;

            //var issue2 = RefreshOrAdd(issue, false);
            //Debug.Assert(ReferenceEquals(issue, issue2));
            //var data = GetOrCreateLocalObject<IssueLocalData>(issue.TypedId);
            //data.Summary = "New " + (-issue.Id).ToStringInvariant();
            return pComp;
        }
        static int _fictiveProjectComponentIdSeed;

        public SetSnapshot<T> GetSnapShotOf<T>()
            where T : BaseBusinessObject
        {
            return new SetSnapshot<T>(this, null);
        }

        public IEnumerable<BaseBusinessObject> Save(IReadOnlyCollection<BaseBusinessObject> objects = null, IProgress<string> pr = null)
        {
            var jiraObjects = objects?.OfType<JiraObject>().ToList()
                           ?? _objects.Values.Where(tl => typeof(JiraObject).IsAssignableFrom(tl.Type))
                                             .SelectMany(tl => tl.ByIds.Values)
                                             .Cast<JiraObject>()
                                             .Where(jiraObj => jiraObj.DataHasChanged)
                                             .ToList();
            IJiraRepository repo = this;
            foreach (var obj in repo.PushEdits(jiraObjects, eCacheType.Disk, pr))
                yield return obj;

            _disk.CommitUnexpectedUserEdits(pr);
            var extObjects = objects?.OfType<ExtensionObject>().ToList()
               ?? _objects.Values.Where(tl => typeof(ExtensionObject).IsAssignableFrom(tl.Type))
                                 .SelectMany(tl => tl.ByIds.Values)
                                 .Cast<ExtensionObject>()
                                 .Where(extObj => extObj.DataHasChanged)
                                 .ToList();
            ILocalRepository repo2 = this;
            foreach (var obj in repo2.PushEdits(extObjects, eCacheType.Disk, pr))
                yield return obj;
        }

        #endregion Public features

        #region Core Implementation

        internal IReadOnlyCollection<T> GetListFromMemory<T>(IProgress<string> pr = null)
            where T : BaseBusinessObject
        {
            var type = typeof(T);
            var tl = _objects.TryGetValueClass(type);
            if (tl != null && tl.IsFull)
                return (IReadOnlyCollection<T>)tl.ByIds.Values.ToTypedList(type);
            var lck = _readingLockByTypes.GetValueOrCreateDefault(type);
            lock (lck) // Asking a full list to nested cache is considered slow
            {
                tl = _objects.TryGetValueClass(type);
                if (tl != null && tl.IsFull) // so we handle concurrency access by a double checking
                    return (IReadOnlyCollection<T>)tl.ByIds.Values.ToTypedList(type);
                var lst = _disk.GetJiraObjects<T>(pr);
                tl = new TypeList()
                {
                    Type = type,
                    IsFull = true,
                    ByIds = lst.ToConcurrentDictionary(obj => obj.IdTuple, obj => (BaseBusinessObject)obj)
                };
                _objects[type] = tl;
                return lst;
            }
        }
        readonly ConcurrentDictionary<Type, object> _readingLockByTypes = new ConcurrentDictionary<Type, object>();

        internal T GetObjectFromMemory<T>(IIdTuple id, Func<IIdTuple, T> defaultImpl)
            where T : BaseBusinessObject
        {
            // When loaded from disk, we do not store object in _objects (memory)
            // because we don't handle partial loading yet
            var type = typeof(T);
            var tl = _objects.TryGetValueClass(type);
            BaseBusinessObject obj;
            if (tl == null)
            {
                tl = _objects.GetOrAdd(type, t => new TypeList() { Type = t, ByIds = new ConcurrentDictionary<IIdTuple, BaseBusinessObject>() });
                obj = null;
            }
            else
                obj = tl.ByIds.TryGetValueClass(id);
            if (obj == null)
            {
                obj = defaultImpl(id);
                if (obj != null)
                    RefreshOrAdd(obj, false);
            }
            return (T)obj;
        }


        /// <summary>
        /// Replace object by the one already existing in memory / cache
        /// If skipIfPossible is set to true and another version of same object is not already present in memory, null is returned
        /// This allow to avoid loading object not recently used.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="newBbo">A copy of object that may already exist in memory</param>
        /// <param name="skipIfPossible">true to return null if object does not already exist in memory</param>
        T RefreshOrAdd<T>(T newBbo, bool skipIfPossible)
            where T : BaseBusinessObject, IHasTypedIdReadable
        {
            if (newBbo == null)
                return null;
            TypeList tl;
            BaseBusinessObject curBbo;
            if (skipIfPossible)
            {
                if (!_objects.TryGetValue(newBbo.TypedId.Type, out tl)) // typeof(T) can be abstract...
                    return null;
                if (!tl.ByIds.TryGetValue(newBbo.IdTuple, out curBbo))
                {
                    // a priori, there is no need to lock here because once the value is false it w'ont become true again except if we flush everything
                    // So because the workflow of possible values is acyclic there is no need for a lock
                    tl.IsFull = false; 
                    return null;
                }
            }
            else
            {
                // TODO : There is a rare race condition here (for example if TypeList is rebuild at the same time in LoadListFromMemory)
                tl = _objects.GetOrCreate(newBbo.TypedId.Type, () => new TypeList() { Type = newBbo.TypedId.Type, ByIds = new ConcurrentDictionary<IIdTuple, BaseBusinessObject>() });
                curBbo = tl.ByIds.GetOrAdd(newBbo.IdTuple, newBbo);
            }
            if (ReferenceEquals(newBbo, curBbo))
                return (T)curBbo;
            curBbo.CopyAllFieldsFrom(newBbo);
            return (T)curBbo;
        }

        #endregion Core Implementation
    }
}
