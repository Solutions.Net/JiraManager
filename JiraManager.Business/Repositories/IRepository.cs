﻿using System;


namespace JiraManager.Business.Repositories
{
    public interface IRepository : IJiraRepository, ILocalRepository
    {
    }
}
