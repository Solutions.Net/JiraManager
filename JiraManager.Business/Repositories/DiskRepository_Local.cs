﻿using System;
using System.Collections.Generic;
using System.Linq;

using LibGit2Sharp;

using TechnicalTools.Model.Cache;
using TechnicalTools.Diagnostics;

using JiraManager.Business.ObjectExtensions;


namespace JiraManager.Business.Repositories
{
    public partial class DiskRepository : ILocalRepository
    {
        IReadOnlyCollection<IssueLocalData> ILocalRepository.IssueViews { get; }

        IssueLocalData ILocalRepository.GetIssueData(long id, bool create) { return null; }
        ServerLocalData ILocalRepository.GetServerData(string id, bool create) { return null; }

        IReadOnlyCollection<T> ILocalRepository.GetLocalList<T>(IProgress<string> pr) { return new List<T>(); }
        public T GetOrCreateLocalObject<T>(ITypedId tid, bool create)
            where T : ExtensionObject
        {
            var data = GetObjectFromDisk<T>(tid);
            if (data != null || !create)
                return data;
            var cons = RepositoryHelper.GetConstructorFor(typeof(T));
            data = (T)cons(tid);
            WriteObjectOnDisk(data);
            return data;
        }
        
        /// <see cref="ILocalRepository.Refresh(IReadOnlyCollection{ExtensionObject}, IProgress{string})"/>
        public IEnumerable<ExtensionObject> Refresh(IReadOnlyCollection<ExtensionObject> objects = null, IProgress<string> pr = null)
        {
            if (objects == null)
            {
                foreach (var it in BrowseTypeFolders())
                    if (typeof(ExtensionObject).IsAssignableFrom(it.type))
                        foreach (var obj in GetListFromDisk(it.type, pr))
                            yield return (ExtensionObject)obj;
            }
            else
            {
                foreach (var obj in objects)
                {
                    var freshObj = (ExtensionObject)GetObjectFromDisk(obj.GetType(), obj.IdTuple);
                    if (!freshObj.Storage.Equals(obj.Storage))
                        yield return freshObj;
                }
            }
        }

        /// <see cref="ILocalRepository.PushEdits(IReadOnlyCollection{ExtensionObject}, eCacheType, IProgress{string})"/>
        public IReadOnlyCollection<ExtensionObject> PushEdits(IReadOnlyCollection<ExtensionObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null)
        {
            if (maxDepth < eCacheType.Disk)
                return null; // means "we don't know which objects needed to be changed"
            objects = objects ?? new ExtensionObject[0];
            // ExtensionObject are meant to be saved on disk so there is nothing more to do
            // The current repository is the "source", so there is no other repos to call 
            // PushEdits with jira objects

            using (var repo = GetRepository())
            {
                CommitUnexpectedUserEdits(repo, pr);
                var lst = objects.Where(obj => WriteObjectOnDisk(obj)).ToList();
                pr.Report("Saving on drive");
                Commands.Stage(repo, "*");
                ExceptionManager.Instance.IgnoreException<EmptyCommitException>(() => repo.Commit("Push Local edits", _signature, _signature));
                return lst;
            }
        }
    }
}
