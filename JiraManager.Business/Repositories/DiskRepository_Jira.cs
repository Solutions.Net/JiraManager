﻿using System;
using System.Collections.Generic;
using System.Linq;

using LibGit2Sharp;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model.Cache;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business.Repositories
{
    public partial class DiskRepository : IJiraRepository
    {
        public IReadOnlyCollection<ServerInfo>        ServerInfos      { get { return GetJiraObjects<ServerInfo>(); } }
        public IReadOnlyCollection<Project>           Projects         { get { return GetJiraObjects<Project>(); } }
        public IReadOnlyCollection<ProjectComponent>  Components       { get { return GetJiraObjects<ProjectComponent>(); } }
        public IReadOnlyCollection<User>              Users            { get { return GetJiraObjects<User>(); } }
        public IReadOnlyCollection<Issue>             Issues           { get { return GetJiraObjects<Issue>(); } }
        public IReadOnlyCollection<IssueType>         IssueTypes       { get { return GetJiraObjects<IssueType>(); } }
        public IReadOnlyCollection<Status>            Statuses         { get { return GetJiraObjects<Status>(); } }
        public IReadOnlyCollection<StatusCategory>    StatusCategories { get { return GetJiraObjects<StatusCategory>(); } }
        public IReadOnlyCollection<Resolution>        Resolutions      { get { return GetJiraObjects<Resolution>(); } }
        public IReadOnlyCollection<Priority>          Priorities       { get { return GetJiraObjects<Priority>(); } }
        public IReadOnlyCollection<IssueLink>         IssueLinks       { get { return GetJiraObjects<IssueLink>(); } }
        public IReadOnlyCollection<IssueLinkType>     IssueLinkTypes   { get { return GetJiraObjects<IssueLinkType>(); } }


        public Project          GetProject         (long   id) { return GetJiraObject<Project>(new IdTuple<long>(id)); }
        public ProjectComponent GetProjectComponent(long   id) { return GetJiraObject<ProjectComponent>(new IdTuple<long>(id)); }
        public User             GetUser            (string id) { return GetJiraObject<User>(new IdTuple<string>(id)); }
        public Issue            GetIssue           (long   id) { return GetJiraObject<Issue>(new IdTuple<long>(id)); }
        public IssueType        GetIssueType       (long   id) { return GetJiraObject<IssueType>(new IdTuple<long>(id)); }
        public Status           GetStatus          (long   id) { return GetJiraObject<Status>(new IdTuple<long>(id)); }
        public StatusCategory   GetStatusCategory  (long   id) { return GetJiraObject<StatusCategory>(new IdTuple<long>(id)); }
        public Resolution       GetResolution      (long   id) { return GetJiraObject<Resolution>(new IdTuple<long>(id)); }
        public Priority         GetPriority        (long   id) { return GetJiraObject<Priority>(new IdTuple<long>(id)); }
        public IssueLink        GetIssueLink       (long   id) { return GetJiraObject<IssueLink>(new IdTuple<long>(id)); }
        public IssueLinkType    GetIssueLinkType   (long   id) { return GetJiraObject<IssueLinkType>(new IdTuple<long>(id)); }

        public Issue GetIssue(string key)
        {
            var issue = _repo.GetIssue(key);
            WriteObjectOnDisk(issue);
            return issue;
        }

        public IReadOnlyCollection<T> GetJiraObjects<T>(IProgress<string> pr = null)
            where T : BaseBusinessObject
        {
            return GetListFromDisk<T>(pr);
        }
        IReadOnlyCollection<T> IJiraRepository.GetJiraObjects<T>(IProgress<string> pr) { return GetJiraObjects<T>(pr); }

        public T GetJiraObject<T>(IIdTuple id)
             where T : JiraObject
        {
            var res = GetObjectFromDisk<T>(id);
            if (res == null)
            {
                res = _repo.GetJiraObject<T>(id);
                WriteObjectOnDisk(res);
            }
            return res;
        }

        /// <see cref="IJiraRepository.Refresh(IReadOnlyCollection{JiraObject}, IProgress{string})"/>
        public IEnumerable<JiraObject> Refresh(IReadOnlyCollection<JiraObject> objects = null, IProgress<string> pr = null)
        {
            if (objects == null)
                objects = BrowseTypeFolders().Where(it => typeof(JiraObject).IsAssignableFrom(it.type))
                                             .SelectMany(it => GetListFromDisk(it.type, pr).Cast<JiraObject>())
                                             .ToList();
            using (var repo = GetRepository())
            {
                CommitUnexpectedUserEdits(repo, pr);
                foreach (var obj in _repo.Refresh(objects, pr))
                {
                    if (WriteObjectOnDisk(obj))
                        yield return obj;
                }
                pr.Report("Saving on drive");
                Commands.Stage(repo, "*");
                ExceptionManager.Instance.IgnoreException<EmptyCommitException>(() => repo.Commit("Refresh All", _signature, _signature));
            }
        }

        /// <see cref="IJiraRepository.RefreshAll(DateTime, IProgress{string})"/>
        public IEnumerable<JiraObject> RefreshAll(DateTime? lastfullRefreshDate = null, IProgress<string> pr = null)
        {
            lastfullRefreshDate = lastfullRefreshDate ?? ServerInfos.SingleOrDefault()?.ServerTime;
            pr = pr ?? ProgressEx<string>.Default;
            using (var repo = GetRepository())
            {
                CommitUnexpectedUserEdits(repo, pr);
                foreach (var obj in _repo.RefreshAll(lastfullRefreshDate, pr))
                {
                    if (WriteObjectOnDisk(obj))
                        yield return obj;
                }
                pr.Report("Saving on drive");
                Commands.Stage(repo, "*");
                ExceptionManager.Instance.IgnoreException<EmptyCommitException>(() => repo.Commit("Refresh All", _signature, _signature));
            }
        }

        public event EventHandler Refreshed
        {
            add    { _repo.Refreshed += value; }
            remove { _repo.Refreshed -= value; }
        }

        /// <see cref="IJiraRepository.PushEdits(IReadOnlyCollection{JiraObject}, eCacheType, IProgress{string})"/>
        public IReadOnlyCollection<JiraObject> PushEdits(IReadOnlyCollection<JiraObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null)
        {
            if (maxDepth < eCacheType.Disk)
                return null; // means "we don't know which objects needed to be changed"
            objects = objects ?? new JiraObject[0];
            objects = _repo.PushEdits(objects, maxDepth, pr)
                    ?? objects;

            using (var repo = GetRepository())
            {
                CommitUnexpectedUserEdits(repo, pr);
                var lst = objects.Where(obj => WriteObjectOnDisk(obj)).ToList();
                pr.Report("Saving on drive");
                Commands.Stage(repo, "*");
                ExceptionManager.Instance.IgnoreException<EmptyCommitException>(() => repo.Commit("Push Jira edits", _signature, _signature));
                return lst;
            }
        }
    }
}
