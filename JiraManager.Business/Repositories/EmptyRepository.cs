﻿using System;
using System.Collections.Generic;

using TechnicalTools.Model.Cache;

using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.ObjectExtensions;


namespace JiraManager.Business.Repositories
{
    public class EmptyRepository : IJiraRepository, ILocalRepository
    {
        public EmptyRepository(object anydependency = null)
        {
            _anydependency = anydependency;
        }
        object _anydependency = null;

        #region IJiraRepository

        IReadOnlyCollection<ServerInfo>        IJiraRepository.ServerInfos      { get { return null; } }
        IReadOnlyCollection<Project>           IJiraRepository.Projects         { get { return null; } }
        IReadOnlyCollection<ProjectComponent>  IJiraRepository.Components       { get { return null; } }
        IReadOnlyCollection<User>              IJiraRepository.Users            { get { return null; } }
        IReadOnlyCollection<Issue>             IJiraRepository.Issues           { get { return null; } }
        IReadOnlyCollection<IssueType>         IJiraRepository.IssueTypes       { get { return null; } }
        IReadOnlyCollection<Status>            IJiraRepository.Statuses         { get { return null; } }
        IReadOnlyCollection<StatusCategory>    IJiraRepository.StatusCategories { get { return null; } }
        IReadOnlyCollection<Resolution>        IJiraRepository.Resolutions      { get { return null; } }
        IReadOnlyCollection<Priority>          IJiraRepository.Priorities       { get { return null; } }
        IReadOnlyCollection<IssueLink>         IJiraRepository.IssueLinks       { get { return null; } }
        IReadOnlyCollection<IssueLinkType>     IJiraRepository.IssueLinkTypes   { get { return null; } }


        Project          IJiraRepository.GetProject         (long   id) { return null; }
        ProjectComponent IJiraRepository.GetProjectComponent(long   id) { return null; }
        User             IJiraRepository.GetUser            (string id) { return null; }
        Issue            IJiraRepository.GetIssue           (long   id) { return null; }
        Issue            IJiraRepository.GetIssue           (string key){ return null; }
        IssueType        IJiraRepository.GetIssueType       (long   id) { return null; }
        Status           IJiraRepository.GetStatus          (long   id) { return null; }
        StatusCategory   IJiraRepository.GetStatusCategory  (long   id) { return null; }
        Resolution       IJiraRepository.GetResolution      (long   id) { return null; }
        Priority         IJiraRepository.GetPriority        (long   id) { return null; }
        IssueLink        IJiraRepository.GetIssueLink       (long   id) { return null; }
        IssueLinkType    IJiraRepository.GetIssueLinkType   (long   id) { return null; }

        IReadOnlyCollection<T> IJiraRepository.GetJiraObjects  <T>(IProgress<string> pr)
        {
            return new List<T>();
        }
        T IJiraRepository.GetJiraObject<T>(IIdTuple id)
        {
            return null;
        }
        public T AddLocalObject<T>(T obj)
            where T : ExtensionObject, IHasTypedIdReadable
        {
            return obj;
        }

        /// <see cref="IJiraRepository.Refresh(IReadOnlyCollection{JiraObject}, IProgress{string})"/>
        public IEnumerable<JiraObject> Refresh(IReadOnlyCollection<JiraObject> objects = null, IProgress<string> pr = null)
        {
            yield break;
        }

        /// <see cref="IJiraRepository.RefreshAll(DateTime?, IProgress{string})"/>
        public IEnumerable<JiraObject> RefreshAll(DateTime? lastfullRefrshDate = null, IProgress<string> pr = null)
        {
            yield break;
        }

        event EventHandler IJiraRepository.Refreshed { add { } remove { } }

        /// <see cref="IJiraRepository.PushEdits(IReadOnlyCollection{JiraObject}, eCacheType, IProgress{string})"/>
        public IReadOnlyCollection<JiraObject> PushEdits(IReadOnlyCollection<JiraObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null)
        {
            return objects;
        }

        #endregion IJiraRepository

        #region ILocalRepository

        IReadOnlyCollection<IssueLocalData> ILocalRepository.IssueViews { get; }

        IssueLocalData ILocalRepository.GetIssueData(long id, bool create) { return null; }
        ServerLocalData ILocalRepository.GetServerData(string id, bool create) { return null; }

        IReadOnlyCollection<T> ILocalRepository.GetLocalList<T>(IProgress<string> pr) { return new List<T>(); }
        T ILocalRepository.GetOrCreateLocalObject<T>(ITypedId tid, bool create) { return null; }

        /// <see cref="ILocalRepository.Refresh(IReadOnlyCollection{ExtensionObject}, IProgress{string})"/>
        public IEnumerable<ExtensionObject> Refresh(IReadOnlyCollection<ExtensionObject> objects = null, IProgress<string> pr = null)
        {
            yield break;
        }

        /// <see cref="ILocalRepository.PushEdits(IReadOnlyCollection{ExtensionObject}, eCacheType, IProgress{string})"/>
        public IReadOnlyCollection<ExtensionObject> PushEdits(IReadOnlyCollection<ExtensionObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null)
        {
            return objects;
        }

        #endregion
    }
}
