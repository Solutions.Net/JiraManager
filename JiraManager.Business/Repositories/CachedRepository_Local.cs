﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model.Cache;

using JiraManager.Business.API.DtoObjects;
using JiraManager.Business.ObjectExtensions;


namespace JiraManager.Business.Repositories
{
    public partial class CachedRepository : ILocalRepository
    {
        public IReadOnlyCollection<IssueLocalData> IssueViews { get { return GetLocalList<IssueLocalData>(new Progress<string>()); } }

        public IssueLocalData         GetIssueData(long id, bool create = true)   { return GetOrCreateLocalObject<IssueLocalData>(new IdTuple<long>(id).ToTypedId(typeof(Issue)), create); }
        public ServerLocalData        GetServerData(string id, bool create = true) { return GetOrCreateLocalObject<ServerLocalData>(new IdTuple<string>(id).ToTypedId(typeof(ServerInfo)), create); }

        public IReadOnlyCollection<T> GetLocalList<T>(IProgress<string> pr = null) 
            where T : ExtensionObject
        {
            return GetListFromMemory<T>(pr);
        }

        public T GetOrCreateLocalObject<T>(ITypedId tid, bool create = true) 
            where T : ExtensionObject
        {
            Func<IIdTuple, T> func = iid =>
            {
                var diskData = _disk.GetOrCreateLocalObject<T>((ITypedId)iid, false);
                if (diskData != null || !create)
                    return diskData;
                var cons = RepositoryHelper.GetConstructorFor(typeof(T));
                var newData = (T)cons((ITypedId)iid);
                return RefreshOrAdd(newData, false);
            };
            var data = GetObjectFromMemory<T>(tid, func);
            return data;
        }


        /// <see cref="ILocalRepository.Refresh(IReadOnlyCollection{ExtensionObject}, IProgress{string})"/>
        public IEnumerable<ExtensionObject> Refresh(IReadOnlyCollection<ExtensionObject> objects = null, IProgress<string> pr = null)
        {
            objects = objects ?? _objects.Values.Where(tl => typeof(ExtensionObject).IsAssignableFrom(tl.Type))
                                                .SelectMany(tl => tl.ByIds.Values)
                                                .Cast<ExtensionObject>()
                                                .ToList();
            foreach (var obj in _disk.Refresh(objects, pr))
            {
                RefreshOrAdd(obj, true);
                yield return obj;
            }
        }

        /// <see cref="ILocalRepository.PushEdits(IReadOnlyCollection{ExtensionObject}, eCacheType, IProgress{string})"/>
        public IReadOnlyCollection<ExtensionObject> PushEdits(IReadOnlyCollection<ExtensionObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null)
        {
            if (maxDepth < eCacheType.Memory)
                return null; // means "we don't know which objects needed to be changed"
            objects = objects ?? new ExtensionObject[0];
            objects = _disk.PushEdits(objects, maxDepth, pr)
                    ?? objects;
            // RefreshOrAdd does not filter for the object that need to be add to cache
            // But this is not mandatory
            var lst = objects.Select(obj => RefreshOrAdd(obj, false)).ToList();
            return lst;
        }

    }
}
