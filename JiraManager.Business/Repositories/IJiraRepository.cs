﻿using System;
using System.Collections.Generic;

using TechnicalTools.Model.Cache;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business.Repositories
{
    public interface IJiraRepository
    {
        IReadOnlyCollection<ServerInfo>        ServerInfos      { get; }
        IReadOnlyCollection<Project>           Projects         { get; }
        IReadOnlyCollection<ProjectComponent>  Components       { get; }
        IReadOnlyCollection<User>              Users            { get; }
        IReadOnlyCollection<Issue>             Issues           { get; }
        IReadOnlyCollection<IssueType>         IssueTypes       { get; }
        IReadOnlyCollection<Status>            Statuses         { get; }
        IReadOnlyCollection<StatusCategory>    StatusCategories { get; }
        IReadOnlyCollection<Resolution>        Resolutions      { get; }
        IReadOnlyCollection<Priority>          Priorities       { get; }
        IReadOnlyCollection<IssueLink>         IssueLinks       { get; }
        IReadOnlyCollection<IssueLinkType>     IssueLinkTypes   { get; }

        Project          GetProject         (long id);
        ProjectComponent GetProjectComponent(long id);
        User             GetUser            (string id);
        Issue            GetIssue           (long id);
        Issue            GetIssue           (string key);
        IssueType        GetIssueType       (long id);
        Status           GetStatus          (long id);
        StatusCategory   GetStatusCategory  (long id);
        Resolution       GetResolution      (long id);
        Priority         GetPriority        (long id);
        IssueLink        GetIssueLink       (long id);
        IssueLinkType    GetIssueLinkType   (long id);

        IReadOnlyCollection<T> GetJiraObjects<T>(IProgress<string> pr = null) where T : JiraObject;
                            T  GetJiraObject <T>(IIdTuple id)                 where T : JiraObject;

        /// <summary>
        /// Resync objects using the source repository (or service)
        /// If objects is null all objects currently in repository will be resync.
        /// This method does not make possible to get new objects.
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="pr"></param>
        /// <returns>The objects refreshed.</returns>
        IEnumerable<JiraObject> Refresh(IReadOnlyCollection<JiraObject> objects = null, IProgress<string> pr = null);

        /// <summary>Get all objects from source repository (or service), with new objects</summary>
        /// <returns>The objects refreshed or new.</returns>
        IEnumerable<JiraObject> RefreshAll(DateTime? lastfullRefrshDate = null, IProgress<string> pr = null);

        /// <summary>
        /// Push all edits of all objects in "objects" to the underlying storage (or remote service).
        /// </summary>
        /// <param name="objects"></param>
        /// <param name="pr"></param>
        /// <returns>objects that needed to be pushed</returns>
        IReadOnlyCollection<JiraObject> PushEdits(IReadOnlyCollection<JiraObject> objects = null, eCacheType maxDepth = eCacheType.Source, IProgress<string> pr = null);

        event EventHandler Refreshed;
    }

    public enum eCacheType
    {
        None,
        Ephemeral = 10, // memory (but can disappear if not referenced)
        Memory = 20, // Just memory (even if no referenced)
        Disk = 30, // hard drive / SDD etc
        Remote = 40, // a remote cache
        Source = 50, // The true source of data
    }
}
