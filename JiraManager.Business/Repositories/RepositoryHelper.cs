﻿using System;
using System.Collections.Concurrent;
using System.Linq;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business.Repositories
{
    public static class RepositoryHelper
    {
        internal static Func<IIdTuple, BaseBusinessObject> GetConstructorFor(Type type)
        {
            return _constructors.GetOrAdd(type, t => BuildConstructorFor(t));
        }
        static Func<IIdTuple, BaseBusinessObject> BuildConstructorFor(Type type)
        {
            if (typeof(JiraObject).IsAssignableFrom(type))
                return (IIdTuple _) => (BaseBusinessObject)DefaultObjectFactory.ConstructorFor(type)();

            var allConstructors = type.GetConstructors();
            var constructors = allConstructors.Where(c => c.GetParameters().Length == 1
                                                       && typeof(IIdTuple).IsAssignableFrom(c.GetParameters()
                                                                                             .First()
                                                                                             .ParameterType))
                                              .ToArray();
            if (constructors.Length == 0)
                throw new TechnicalException($"Type {type} must have a constructor taking a {typeof(IIdTuple)} (or an implementing type)", null);
            return (IIdTuple iid) => (BaseBusinessObject)constructors.Single(c => c.GetParameters().First().ParameterType.IsAssignableFrom(iid.GetType()))
                                                                     .Invoke(new object[] { iid });
        }
        static readonly ConcurrentDictionary<Type, Func<IIdTuple, BaseBusinessObject>> _constructors = new ConcurrentDictionary<Type, Func<IIdTuple, BaseBusinessObject>>();
    }
}
