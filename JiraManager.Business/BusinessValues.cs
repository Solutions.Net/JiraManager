﻿using System;
using System.Collections.Generic;

using TechnicalTools;

using JiraManager.Business.API.DtoObjects;


namespace JiraManager.Business
{
    public static partial class BusinessValues
    {
        public static object[] ParentTaskField         { get; } = new[] { "fields", "customfield_10007" };
        public static object[] StoryPointField         { get; } = new[] { "fields", "customfield_10017" };
        public static object[] StoryPointEstimateField { get; } = new[] { "fields", "customfield_10021" };
        public static object[] EpicColour              { get; } = new[] { "fields", "customfield_10005" };


        public static IssueLinkType Blocks     { get { return BusinessSingletons.Instance.GetRepository().GetIssueLinkType(10000); } }
        public static IssueLinkType Clones     { get { return BusinessSingletons.Instance.GetRepository().GetIssueLinkType(10001); } }
        public static IssueLinkType Duplicates { get { return BusinessSingletons.Instance.GetRepository().GetIssueLinkType(10002); } }
        public static IssueLinkType RelatesTo  { get { return BusinessSingletons.Instance.GetRepository().GetIssueLinkType(10003); } }

        // Exist by default on all jira project
        public static IssueType Epic    { get { return BusinessSingletons.Instance.GetRepository().GetIssueType(10000); } }
        public static IssueType Story   { get { return BusinessSingletons.Instance.GetRepository().GetIssueType(10001); } }
        public static IssueType Task    { get { return BusinessSingletons.Instance.GetRepository().GetIssueType(10002); } }
        public static IssueType SubTask { get { return BusinessSingletons.Instance.GetRepository().GetIssueType(10003); } }
        public static IssueType Bug     { get { return BusinessSingletons.Instance.GetRepository().GetIssueType(10004); } }

        // Exist by default on all jira project
        public static Status ToDo       { get { return BusinessSingletons.Instance.GetRepository().GetStatus(10000); } }
        public static Status Done       { get { return BusinessSingletons.Instance.GetRepository().GetStatus(10001); } }
        public static Status InProgress { get { return BusinessSingletons.Instance.GetRepository().GetStatus(3    ); } }

        // Exist by default on all jira project
        public static Priority Highest { get { return BusinessSingletons.Instance.GetRepository().GetPriority(1); } }
        public static Priority High    { get { return BusinessSingletons.Instance.GetRepository().GetPriority(2); } }
        public static Priority Medium  { get { return BusinessSingletons.Instance.GetRepository().GetPriority(3); } }
        public static Priority Low     { get { return BusinessSingletons.Instance.GetRepository().GetPriority(4); } }
        public static Priority Lowest  { get { return BusinessSingletons.Instance.GetRepository().GetPriority(5); } }


        public static Dictionary<string, int> PriorityWordsForNormalization { get; } = new Dictionary<string, int>()
        {
            { "id", 0 },
            { User.UniqueIdJsonKey(3), 1},
            { "key", 2}, // "key" is also equals to User.UniqueIdJsonKey(2)
            { "name", 3},
            { "", 4}, // All others
            { "fields", 5},
        };


        // These data are removed (and lost) from json when requesting API
        public static Dictionary<Type, string[]> UselessData { get; } =
                new Dictionary<Type, string[]>()
                {
                    // Because it change every time we request
                    // contains for example com.atlassian.greenhopper.service.sprint.Sprint@2767d563[completeDate=<null>,endDate=2020-07-03T09:40:00.000Z,goal=,id=25,name=Manage Sprint 2,rapidViewId=57,sequence=25,startDate=2020-06-19T09:40:58.835Z,state=ACTIVE]
                    { typeof(Issue), new []{ "fields.customfield_10115" } },
                };

        // Tell that for each Object (described by json Path on the left)
        // we only keep the properties defined on the right
        // The other properties, inside objects, are redondant data to make developper life easier with navigation
        // But we don't care we just want a relational data (easier to maintain and take less storage)
        public static Dictionary<Type, Dictionary<string, string[]>> GetRedondantData()
        {
            return new Dictionary<Type, Dictionary<string, string[]>>()
            {
                { typeof(Issue), new Dictionary<string, string[]>() {
                    { "fields.assignee", User.UniqueIdJsonKey().WrapInArray() },
                    { "fields.creator",  User.UniqueIdJsonKey().WrapInArray() },
                    { "fields.reporter", User.UniqueIdJsonKey().WrapInArray() },
                    { "fields.issuetype", "id".WrapInArray() },
                    { "fields.priority", "id".WrapInArray() },
                    { "fields.project", new []{ "id", "key" } },
                    { "fields.resolution", "id".WrapInArray() },
                    { "fields.security", "id".WrapInArray() },
                    { "fields.status",  "id".WrapInArray() },
                    { "fields.subtasks", new []{ "id", "key" } },
                    { "fields.customfield_10500.requestType", "id".WrapInArray() },
                    { "fields.customfield_10837", new [] { User.UniqueIdJsonKey(3), User.UniqueIdJsonKey(2) } },
                    { "fields.issuelinks", new []{ "id" } },
                } },
                { typeof(Project), new Dictionary<string, string[]>() {
                    { "projectCategory", new []{ "id", "name" } },
                } },
                { typeof(ProjectComponent), new Dictionary<string, string[]>() {
                    { "assignee", User.UniqueIdJsonKey().WrapInArray() },
                    { "lead", User.UniqueIdJsonKey().WrapInArray() },
                    { "realAssignee", User.UniqueIdJsonKey().WrapInArray() },
                } },
                { typeof(Status), new Dictionary<string, string[]>() {
                    { "statusCategory", new []{ "id", "key" } },
                } },
                { typeof(IssueLink), new Dictionary<string, string[]>() {
                    { "inwardIssue", new []{ "id", "key" } },
                    { "outwardIssue", new []{ "id", "key" } },
                } },
            };
        }


        // These data are uninteresting and so are saved on their own commit in db to make audit easier
        public static Dictionary<Type, string[][]> UninterestingPaths { get; } =
            new Dictionary<Type, string[][]>()
            {
                { typeof(User), new []{ new [] { "avatarUrls" } } }
            };
    }
}
