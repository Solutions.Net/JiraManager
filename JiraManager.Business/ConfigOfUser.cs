﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Tools;

using ApplicationBase.Business;
using System.IO;

namespace JiraManager.Business
{
    // To add a new user setting, just :  
    // Add a new property like other, add a line in CopyFrom, and add attributes Category &DisplayName
    public class ConfigOfUser : ApplicationBase.Business.ConfigOfUser 
    { 
        [Category("Credentials")][DisplayName("Jira API Url")] 
        [Description("Create it from https://id.atlassian.com/manage/api-tokens\n" + 
                     "Should be an url like \"https://yourdomain.atlassian.net/rest/api/3\" (or \"api/2\" at the end)")] 
        public string JiraApiUrl        { get { return _JiraApiUrl;         } set { SetUserSetting(ref _JiraApiUrl,         value?.TrimEnd('/')); } } string _JiraApiUrl; 
        [Category("Credentials")][DisplayName("Jira Email Account (login)")] 
        [Description("Create it from https://id.atlassian.com/manage/api-tokens")] 
        public string JiraEmailAccount  { get { return _JiraEmailAccount;   } set { SetUserSetting(ref _JiraEmailAccount,   value); } } string _JiraEmailAccount; 
        [Category("Credentials")][DisplayName("Jira API Key")] 
        [Description("Create it from https://id.atlassian.com/manage/api-tokens")] 
        public string JiraApiKey        { get { return _JiraApiKey;         } set { SetUserSetting(ref _JiraApiKey,         value); } } string _JiraApiKey; 
        [Category("Credentials")][DisplayName("Cookies from your browser")] 
        [Description("Get this value by sniffing sent cookie using your browser in http request.\n" +  
                     "Must be in form \"a=b; c=d; e=f\" (spaces are trimmed, ';' is not handled in values yet, nor '=' in key)")] 
        public string CompanyCookies    { get { return _CompanyCookies;     } set { SetUserSetting(ref _CompanyCookies,     value); } } string _CompanyCookies;
        [Category("Business")][DisplayName("Issue Filter")]
        [Description("Allow you to filter issue retrieved from your compay database. Because everything is probably not relevant for your.\n" +
                     "This is JQL format. For example you can type \"Project = MyProject AND component in (Foo, Bar)\"")] 
        public string IssueFilter       { get { return _IssueFilter;        } set { SetUserSetting(ref _IssueFilter,        value); } } string _IssueFilter;

        [Category("Automatic")][DisplayName("SSD/HDD hard drives detection")]
        [Description("This value is automatically configured. Indicate for each hard drive, if it is a SDD or not.")]
        public string HardDriveSddStates { get { return _HardDriveSddStates; } set { SetUserSetting(ref _HardDriveSddStates, value); _hardDriveSddStates = null; } } string _HardDriveSddStates;

        [Category("Application Style")][DisplayName("Style of property grid")]
        public bool ShowPropertiesAsOffice { get { return _ShowPropertiesAsOffice; } set { SetUserSetting(ref _ShowPropertiesAsOffice, value); } } bool _ShowPropertiesAsOffice;

        protected internal ConfigOfUser(AuthenticationManager manager, StorageTraits traits = null)
            : base(manager, traits)
        {
        }
        protected ConfigOfUser(StorageTraits traits = null)
            : base(traits)
        { }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(ConfigOfUser));

        protected override ApplicationBase.Business.ConfigOfUser CreateNewInstance(StorageTraits traits)
        {
            return new ConfigOfUser(traits);
        }
        protected override void CopyFrom(ApplicationBase.Business.ConfigOfUser from)
        {
            base.CopyFrom(from);
            var source = (ConfigOfUser)from;
            JiraApiUrl = source.JiraApiUrl;
            JiraEmailAccount = source.JiraEmailAccount;
            JiraApiKey = source.JiraApiKey;
            HardDriveSddStates = source.HardDriveSddStates;
            ShowPropertiesAsOffice = source.ShowPropertiesAsOffice;
            CompanyCookies = source.CompanyCookies; 
        } 
 
        public int JiraApiVersion { get { return int.Parse(Path.GetFileName(JiraApiUrl)); } } 
 
        public Dictionary<string, string> GetCompanyCookies() 
        { 
            if (string.IsNullOrWhiteSpace(CompanyCookies)) 
                return new Dictionary<string, string>(); ; 
            // TODO : improve parsing, how to handle ';', or '=' in name or value ? 
            return CompanyCookies.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries) 
                                 .Select(pair => pair.Split('=')) 
                                 .ToDictionary(pair => pair[0].Trim(), pair => pair.Skip(1).Join("=").Trim()); 
        } 

        public bool IsFolderOnSddDrive(string path)
        {
            var driveLetter = Path.GetPathRoot(Path.GetFullPath(path));
            if (driveLetter.Length > 1)
                if (driveLetter[0] == '\\')
                    return false;
                else
                    driveLetter = driveLetter.Truncate(1);

            if (_hardDriveSddStates == null)
            {
                if (!string.IsNullOrWhiteSpace(HardDriveSddStates))
                    ExceptionManager.Instance.IgnoreException(() =>
                        _hardDriveSddStates = HardDriveSddStates.Split('|').NotBlank()
                                                                .ToDictionary(str => str.Split('=')[0],
                                                                              str => bool.Parse(str.Split('=')
                                                                                                  ?.ElementAtOrDefault(1)
                                                                                                  ?.ToLowerInvariant())));
                // never done before or user screwed up value edition
                _hardDriveSddStates = _hardDriveSddStates ?? new Dictionary<string, bool>();
            }
            if (_hardDriveSddStates.TryGetValue(driveLetter, out bool res))
                return res;
            // There is some algorithm (see for example https://stackoverflow.com/a/13727686/294998)
            // to detect sdd, but they are too complex. We let user change this value himself
            res = false;
            BusEvents.Instance.RaiseBusinessWarning($"Local database is stored on a new drive letter ({driveLetter}:)." + Environment.NewLine +
                                                    BusinessSingletons.Instance.GetDeployer().Config.ApplicationName + " is now assuming (arbitrarily) this drive is not SSD." + Environment.NewLine +
                                                    "If this drive is a SDD, tell it by editing your personnal settings." + Environment.NewLine +
                                                    "Very interesting speed loading optimizations exist when drives are recognised/configured as SDD.",
                                                    _log);
            _hardDriveSddStates.Add(driveLetter, res);
            // Update settings storage
            HardDriveSddStates = _hardDriveSddStates.OrderBy(kvp => kvp.Key)
                                                    .Select(kvp => kvp.Key + "=" + kvp.Value.ToString())
                                                    .Join("|");
            return res;
        }
        Dictionary<string, bool> _hardDriveSddStates;

    }
}
