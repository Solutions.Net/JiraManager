using System;

using Base = ApplicationBase.Common;


namespace JiraManager.Common
{
    public class CommonSingletons : Base.CommonSingletons
    {
        // Please See "Note 01" in file "ApplicationBase.Common/Code Design Notes.txt"
        public new static CommonSingletons Instance
        {
            get { return _Instance/* ?? (BootstrapConfig)ApplicationBase.Common.BootstrapConfig.Instance*/; }
            set
            {
                _Instance = value;
                if (Base.CommonSingletons.Instance == null ||
                    Base.CommonSingletons.Instance.GetType().IsInstanceOfType(value))
                    Base.CommonSingletons.Instance = value;
            }
        }
        static CommonSingletons _Instance;
        static CommonSingletons()
        {
            Instance = new CommonSingletons();
        }

        protected CommonSingletons()
        {
        }

        // Minimal method to override 
        protected override Base.CommonFactory  CreateCommonFactory()  { return new CommonFactory(); }
        public new              CommonFactory  GetCommonFactory()     { return (CommonFactory)base.GetCommonFactory(); }
        public new              Config         GetConnectionsConfig() { return (Config)base.GetConnectionsConfig(); }
    }
}

