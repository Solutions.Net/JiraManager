using System;
using System.Data.SqlClient;
using System.Xml.Serialization;


namespace JiraManager.Common
{
    [XmlType(AnonymousType = true)]
    public class Config : ApplicationBase.Common.Config
    {
        public override string ConnectionString_Base
        {
            get { return ConnectionStringMain; }
            set { ConnectionStringMain = value; }
        }

        public string ConnectionStringMain
        {
            get { return _connectionStringMain; }
            set
            {
                _connectionStringMain = value;
                ConnectionMain = new SqlConnectionStringBuilder(_connectionStringMain);
                base.ConnectionString_Base = value;
            }
        }
        string _connectionStringMain;
        [XmlIgnore] public SqlConnectionStringBuilder ConnectionMain { get; private set; }

        protected override void CopyFrom(ApplicationBase.Common.Config source)
        {
            base.CopyFrom(source);
            var from = source as Config;
            if (from == null)
                return;

            ConnectionStringMain = from.ConnectionStringMain;
        }

    }
}

