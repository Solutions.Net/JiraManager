using System;
using System.IO;

using TechnicalTools.Logs;


namespace JiraManager.Common.Logging
{
    public class Log4NetLogHandler
    {
        public Log4NetLogHandler(LogManager manager)
        {
            _manager = manager;
            //This works for Winforms apps.  
            string instanceName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            Log4NetQuickSetup.SetUpFile($@"Logs\{instanceName}.log");
            manager.Log += OnAnyLog;
        }
        readonly LogManager _manager;

        public virtual void ConfigureLog4netWith(string configFile)
        {
            if (File.Exists(configFile))
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(configFile));
        }

        protected virtual void OnAnyLog(ILogger logger, ILog log)
        {
            var stdLogger = log4net.LogManager.GetLogger(logger.Name);
            if (log.Level == Level.Info)
                stdLogger.Info(log.ToFormatedMessage(false));
            else if (log.Level == Level.Notice)
                stdLogger.Logger.Log(null, log4net.Core.Level.Notice, log.ToFormatedMessage(false), null);
            else if (log.Level == Level.Debug)
                stdLogger.Debug(log.ToFormatedMessage(false));
            else if (log.Level == Level.Warn)
                stdLogger.Warn(log.ToFormatedMessage(false));
            else if (log.Level == Level.Error)
                stdLogger.Error(log.ToFormatedMessage(false));
            else if (log.Level == Level.Fatal)
                stdLogger.Fatal(log.ToFormatedMessage(false));
        }
    }
}
