using System;
using System.Drawing;


namespace JiraManager.Common
{
    // This class / File is shared (by link) within project TMT Launcher, TMT Deployer and TMT
    // Each project using it must have a ico file named logoTMT.ico
    public class BootstrapConfig : ApplicationBase.Common.BootstrapConfig
    {
        // Please See "Note 01" in file "ApplicationBase.Common/Code Design Notes.txt"
        public new static BootstrapConfig Instance
        {
            get { return _Instance; }
            set
            {
                _Instance = value;
                if (ApplicationBase.Common.BootstrapConfig.Instance == null ||
                    value != null && ApplicationBase.Common.BootstrapConfig.Instance.GetType().IsAssignableFrom(value.GetType()))
                    ApplicationBase.Common.BootstrapConfig.Instance = value;
            }
        }
        static BootstrapConfig _Instance;

        public override string DefaultInitConnectionString { get { return "No Connection StringData Source=????;Initial Catalog=???;User ID=?????;Password='???';Application Name=JiraManager;"; } }

        public BootstrapConfig(Type programType, string logoFileName = "logo.ico")
            : this(GetIconFromCallingAssembly(programType, logoFileName))
        {
        }
        protected BootstrapConfig(Icon icon = null)
            : base(icon)
        {
            InitialConnectionString = DefaultInitConnectionString;
            CliExecutableName = "";
            GuiExecutableName = "JiraManager.exe";
            LauncherExecutableName = "JiraManager Launcher.exe";
            DeployerExecutableName = "JiraManager Deployer.exe";
            CompanyName = "MyCompany";
            ApplicationName = "Jira Manager";
            ApplicationIcon = icon;
            LauncherRelativePathToBinFromMainProjectOutputFolder = @"..\..\..\Tools\JiraManager.Launcher.Gui\bin";
        }
    }
}

