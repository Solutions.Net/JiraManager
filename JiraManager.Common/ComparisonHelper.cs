﻿using System;
using System.Collections.Concurrent;

using TechnicalTools.Model.Cache;


namespace JiraManager.Business.API
{
    public static class ObjectComparisonHelper
    {
        public static int CompareIfOverridingToString<T>(T @this, T other)
            where T : IHasClosedIdReadable
        {
            if (other == null)
                return 1;
            var otherType = other.GetType();
            var a = _IsTypeOVerridingToString.GetOrAdd(@this.GetType(), t => t.GetMethod(nameof(ToString)).DeclaringType != typeof(object));
            var b = _IsTypeOVerridingToString.GetOrAdd(otherType, t => t.GetMethod(nameof(ToString)).DeclaringType != typeof(object));
            if (a)
                if (b)
                    return @this.ToString().CompareTo(other.ToString());
                else
                    return -1;  // make a first because it is readable
            else if (b)
                return 1; // make b first because it is readable
            else
            {
                var iid = @this.IdTuple;
                var iid2 = other.IdTuple;
                if (iid.KeyTypes.Count < iid2.KeyTypes.Count)
                    return -1;
                if (iid.KeyTypes.Count > iid2.KeyTypes.Count)
                    return 1;
                for (int i = 0; i < iid.KeyTypes.Count; ++i)
                {
                    var compare = iid.Keys[i].CompareTo(iid2.Keys[i]);
                    if (compare != 0)
                        return compare;
                }
                return 0;
            }
        }
        static readonly ConcurrentDictionary<Type, bool> _IsTypeOVerridingToString = new ConcurrentDictionary<Type, bool>();
    }
}
