@echo off

setlocal

set nopause=0
set skipGit=

:parsing_arg_loop
      :: Thanks to https://stackoverflow.com/a/34552964
      ::-------------------------- has argument ?
	  :: in %~1 - the ~ removes any wrapping " or '.
      if ["%~1"]==[""] (
        goto parsing_arg_end
      )
      ::-------------------------- argument exist ?

	  if ["%~1"]==["--nopause"] set nopause=1
	  if ["%~1"]==["--skipGit"] set "skipGit=--skipGit"

      ::--------------------------
      shift
      goto parsing_arg_loop


:parsing_arg_end

echo.
if ["%skipGit%"] == [""] (
  echo  ** Loading git submodule 
  REM remove empty folder "ApplicationBase" so commandline "git submodule update ..." works 
  REM (Why git create a empty folder on cloning ?! I thought git only handles files not folders...
  REM  So how it is possible ?)
  ROBOCOPY ApplicationBase ApplicationBase /S /MOVE >NUL
  echo git submodule update --init --recursive
  git submodule update --init --recursive
)


cd ApplicationBase
call bootstrap.bat --nopause %skipGit%
cd ..

echo ** Generating "No DX" version of solution/project files **
"ApplicationBase\scripts\AutonomousCSharpScripter.exe" --run "ApplicationBase\scripts\Generate No DX solution files (Gui).c#s"

if ["%skipGit%"] == [""] (
  echo ** Make all git repositories "standalone" **
  "ApplicationBase\scripts\AutonomousCSharpScripter.exe" --run "ApplicationBase\scripts\Git Submodule undo absorbgitdirs (Gui).c#s"
)


IF EXIST "ApplicationBase\packages" (
  echo ** Recreating symlink folder "ApplicationBase\packages"
  rmdir /S /Q "ApplicationBase\packages"
)
echo ** Create symlink needed by nuget in some submodules so solution can compile
echo mklink /j "ApplicationBase\packages" "packages"
     mklink /j "ApplicationBase\packages" "packages"

if ["%nopause%"] == ["0"] (
  echo DONE! Press any key to leave this script
  pause
)
endlocal
