using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;
using TechnicalTools.Tools;

using ApplicationBase.Deployment;

using JiraManager.Common;


namespace JiraManager.Launcher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (!DebugTools.IsForDevelopper) // To debug as if you are in prod, set this to false in VS' Immediate Window!
                Debug.Listeners.Clear(); // To not show assertion to user

            RuntimeHelpers.RunClassConstructor(typeof(ExceptionManager).TypeHandle);
            TechnicalTools.Logs.LogManager.Default = new TechnicalTools.Logs.LogManager();

            var parser = new CommandLineParser();
            var bcfg = new BootstrapConfig(typeof(Program));
            parser.ParseAndHydrate(args, bcfg);
            BootstrapConfig.Instance = bcfg;
            Debug.Assert(bcfg.LauncherExecutableName.ToLowerInvariant() == Path.GetFileName(Application.ExecutablePath.ToLowerInvariant()));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmLauncher(new Deployer(bcfg),
                                            new Filter { Domain = bcfg.Domain, EnvName = bcfg.EnvName }));
        }
    }
    class Deployer : Common.Deployer
    {
        protected internal Deployer(ApplicationBase.Deployment.Config cfg)
            : base(cfg)
        {
        }
    }
}

