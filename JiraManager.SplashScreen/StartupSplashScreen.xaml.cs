﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows;

using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;

using TechnicalTools.UI;

using ApplicationBase.UI;


namespace JiraManager.SplashScreen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class StartupSplashScreen : Window, ISplashScreen
    {
        public StartupSplashScreen()
            : this(null)
        { }
        public StartupSplashScreen(Icon icon)
        {
            InitializeComponent();
            btnConnection.Visibility = Visibility.Hidden;
            btnQuit.Visibility = Visibility.Hidden;
            lblVersion.Content = "";
            //lblBuild.Content = "";
            lblVersion.Visibility = Visibility.Hidden;
            //lblBuild.Visibility = Visibility.Hidden;
            FadeInFadeOut_Init(hideAtStart: true);
            MouseDown += SplashScreen_MouseDown;
            var brush = txtPassword.FindResource("CueBannerBrush") as System.Windows.Media.Brush;
            txtPassword.Background = brush;
            ///lblTitle.FontFamily = new System.Windows.Media.FontFamily(new Uri("pack://application:,,,/"), "./Fonts/Source Sans Pro (Google)/SourceSansPro-SemiBold.ttf#Source Sans Pro SemiBold");

            if (icon != null)
                Icon = icon.ToImageSource();

            //using (var stream = this.GetType().Assembly.GetManifestResourceStream(this.GetType().Namespace + "." + "logo.ico"))
            //{
            //    var imageSource = new System.Windows.Media.Imaging.BitmapImage();
            //    imageSource.BeginInit();
            //    imageSource.StreamSource = stream;
            //    imageSource.EndInit();
            //    Icon = imageSource;
            //}
        }

        void SplashScreen_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPassword.Password))
            {
                var brush = txtPassword.FindResource("CueBannerBrush") as System.Windows.Media.Brush;
                txtPassword.Background = brush;
            }
            else
                txtPassword.Background = System.Windows.Media.Brushes.White;
        }

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            FadeInThen(null);
            RaiseShown();
            RunProgressBarAnimation();
        }

        #region ProgressBar

        public void RunProgressBarAnimation()
        {
            var worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //for(int i = 0; i < 100; i++)
            //{
            //    (sender as BackgroundWorker).ReportProgress(i);
            //    Thread.Sleep(100);
            //}
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }
        #endregion

        public void ShowMessageBox(string message, string title)
        {
            bool txtPasswordFocused = txtPassword.IsFocused || btnConnection.IsFocused;

            MessageBox.Show(this, message, title);

            Dispatcher.BeginInvoke(DispatcherPriority.Input,
                new Action(delegate ()
                {
                    if (txtPasswordFocused)
                    {

                        txtPassword.Focus();
                        Keyboard.Focus(txtPassword);
                    }
                    else
                    {
                        txtLogin.Focus();
                        Keyboard.Focus(txtPassword);
                    }
                }));
        }

        private void BtnConnection_Click(object sender, RoutedEventArgs e)
        {
            if (UserConnect != null)
                UserConnect();
        }

        private void BtnQuit_Click(object sender, RoutedEventArgs e)
        {
            Quit();
        }
        void Quit()
        {
            if (UserQuit != null)
                UserQuit();
        }

        #region Fadein FadeOut

        public new double Opacity
        {
            get { return base.Opacity; }
            set
            {
                _timerBlurEffect.IsEnabled = false;
                base.Opacity = value;
            }
        }
    

        void FadeInFadeOut_Init(bool hideAtStart)
        {
            _timerBlurEffect = new System.Windows.Threading.DispatcherTimer();
            _timerBlurEffect.Interval = new TimeSpan(0, 0, 0, 0, 25);
            if (hideAtStart)
                base.Opacity = 0;
        }
        System.Windows.Threading.DispatcherTimer _timerBlurEffect;
        EventHandler _currentTimerHandler;
        const double OpacityShift = 0.05;

        void FadeInThen(Action action)
        {
            _timerBlurEffect.Tick -= _currentTimerHandler;
            _currentTimerHandler = (_, __) =>
            {
                base.Opacity = Math.Min(1, base.Opacity + OpacityShift);
                if (base.Opacity >= 1)
                {
                    base.Opacity = 1;
                    _timerBlurEffect.Stop();
                    _timerBlurEffect.Tick -= _currentTimerHandler;
                    if (action != null)
                        action();
                }
            };
            _timerBlurEffect.Tick += _currentTimerHandler;
            _timerBlurEffect.Start();
        }

        void FadeOutThen(Action action)
        {
            _timerBlurEffect.Tick -= _currentTimerHandler; // important si FadeIn n'a pas encore fini
            _currentTimerHandler = (_, __) =>
            {
                base.Opacity = Math.Max(0, base.Opacity - OpacityShift);
                if (base.Opacity <= 0)
                {
                    base.Opacity = 0;
                    _timerBlurEffect.Stop();
                    _timerBlurEffect.Tick -= _currentTimerHandler;
                    if (action != null)
                        action();
                }
            };
            _timerBlurEffect.Tick += _currentTimerHandler;
            _timerBlurEffect.Start();
        }
        #endregion

        // Gestion des messagebox (parenté des message box pour qu'elles apparaissent au dessus)
        IntPtr System.Windows.Forms.IWin32Window.Handle
        {
            get
            {
                if (!Dispatcher.CheckAccess())
                    return Dispatcher.Invoke(() => ((System.Windows.Forms.IWin32Window)this).Handle);
                return Dispatcher.Invoke(() => new WindowInteropHelper(this).Handle);
            }
        }

        #region SplashScreenBase
        
        public event EventHandler Shown;
        void RaiseShown()
        {
            if (Shown != null)
                Shown(this, EventArgs.Empty);
        }

        public System.Windows.Forms.Screen CurrentScreen
        {
            get
            {
                if (!Dispatcher.CheckAccess())
                    return Dispatcher.Invoke(() => CurrentScreen);
                if (WindowState != WindowState.Minimized)
                    WpfScreen.GetWinFormScreenFrom(this);
                var b = this.RestoreBounds;
                return WpfScreen.GetWinFormScreenFrom(new System.Windows.Point() { X = b.Left + b.Width / 2, Y = b.Top + b.Height / 2 });
            }
        }

        public new void Close()
        {
            FadeOutThen(() =>
            {
                explicitClose = true;
                base.Close();
            });
        }
        bool explicitClose; // Explicit close from application / user
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (!explicitClose)
            {
                e.Cancel = true; // Events are too complex to handle when user is closing via right click on taskbar
                Post(Quit); // So we simulate the same thing as if he clicked on Quit / Cancel
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            // A cause du mix Winform / WPF, le dispatcher qui propulse la window WPF ne s'arrete pas au moment du close, on l'aide un peu.
            System.Windows.Threading.Dispatcher.CurrentDispatcher.InvokeShutdown();
        }

        public void RunInNewGuiThread()
        {
            Show();
            // On souhaite que que la splashscreen soit par dessus tout quand elle s'affiche, mais apres on peut la repasser en dessous d'autre appli
            // Quand on lance depuis visual studio, Quand on lance depuis 
            Topmost = true;
            Activate(); // Bizarre il faut que Topmost soit a true pour que ca fonctionne reelement quand on lance depuis visual studio
            Topmost = false;
            System.Windows.Threading.Dispatcher.Run();
        }


        public void Post(Action action)
        {
            Dispatcher.Invoke(action);
        }

        #endregion SplashScreenBase



        #region ISplashScreenBase (spécificité DataExplorer)

        public string Version       { get { return (string)lblVersion.Content;        } set { lblVersion.Content = value; } }
        public string BuildInfo     { get { return "" /*(string)lblBuild.Content*/;   } set { /*lblBuild.Content = value;*/ } }
        public string LicensingInfo { get { return "" /*(string)lblLicensedClient.Content*/; } set { /*lblLicensedClient.Content = value;*/ } }
        public string Info          { get { return lblInfo.Text;                      } set { lblInfo.Text = value; } }
        
        public string       LoginValue         { get { return txtLogin.Text;        } set { txtLogin.Text = value; }}
        public string       PasswordValue      { get { return txtPassword.Password; } }
        
        public List<object> EnvAvailableValues { get { return _EnvAvailableValues; } set { _EnvAvailableValues = value.ToList(); RebuildEnvComboDatasource(); } }

        private void RebuildEnvComboDatasource()
        {
            object sValue = cmbEnv.SelectedValue;
            cmbEnv.Items.Clear();
            foreach (var value in _EnvAvailableValues)
                cmbEnv.Items.Add(value);
            if (_EnvAvailableValues.Contains(sValue))
                cmbEnv.SelectedValue = sValue;
        }

        List<object> _EnvAvailableValues;
        public object       EnvSelectedValue   { get; set; }

        public bool VersionVisible             { get { return lblVersion   .Visibility == Visibility.Visible; } set { lblVersion   .Visibility = value ? Visibility.Visible : Visibility.Hidden; } }
        public bool BuildInfoVisible           { get { return false /*lblBuild     .Visibility == Visibility.Visible*/; } set { /*lblBuild     .Visibility = value ? Visibility.Visible : Visibility.Hidden;*/ } }
        public bool BtnConnectionVisible       { get { return btnConnection.Visibility == Visibility.Visible; } set { btnConnection.Visibility = value ? Visibility.Visible : Visibility.Hidden; } }
        public bool BtnQuitVisible             { get { return btnQuit      .Visibility == Visibility.Visible; } set { btnQuit      .Visibility = value ? Visibility.Visible : Visibility.Hidden; } }
        public bool AnimatedProgressBarVisible { get { return progressBar  .Visibility == Visibility.Visible; } set { progressBar  .Visibility = value ? Visibility.Visible : Visibility.Hidden; } }
        public bool EnvVisible                 { get { return lblEnv       .Visibility == Visibility.Visible; } set { lblEnv.Visibility = cmbEnv.Visibility = value ? Visibility.Visible : Visibility.Hidden; } }
        
        public bool LoginVisible               { get { return lblLogin     .Visibility == Visibility.Visible; } set { lblLogin     .Visibility = Visibility.Hidden; txtLogin   .Visibility = value ? Visibility.Visible : Visibility.Hidden; } }
        public bool PasswordVisible            { get { return lblPassword  .Visibility == Visibility.Visible; } set { lblPassword  .Visibility = Visibility.Hidden; txtPassword.Visibility = value ? Visibility.Visible : Visibility.Hidden; } }

        public event Action UserConnect;
        public event Action UserQuit;

        public void FocusOnLogin()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Input,
               new Action(delegate ()
               {
                   txtLogin.Focus();
                   FocusManager.SetFocusedElement(this, txtLogin);
                   Keyboard.Focus(txtLogin);
               }));
        }

        public void FocusOnPassword()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Input,
               new Action(delegate ()
               {
                   txtPassword.Focus();
                   FocusManager.SetFocusedElement(this, txtPassword);
                   Keyboard.Focus(txtPassword);
               }));
        }

        #endregion ISplashScreenBase (spécificité DataExplorer)

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Quit();
        }

        private void btnConnection_MouseEnter(object sender, MouseEventArgs e)
        {
            //btnConnection.Background = System.Windows.Media.Brushes.Red;
            //btnConnection.BorderBrush = System.Windows.Media.Brushes.Red;
        }

        private void btnConnection_MouseLeave(object sender, MouseEventArgs e)
        {
          //  btnConnection.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb((byte)87, (byte)237, (byte)125));
        }


    }
}
